<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\MasterJenisBarang;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\BarangOpname */

$this->title = 'Stok Opname';
$this->params['breadcrumbs'][] = ['label' => 'Stok Opname', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barang-opname-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php


$listJenis=MasterJenisBarang::getList();

/* @var $this yii\web\View */
/* @var $model app\models\BarangOpname */
/* @var $form yii\widgets\ActiveForm */
$listDepartment = \yii\helpers\ArrayHelper::map(\app\models\Departemen::find()->where(['id'=>Yii::$app->user->identity->departemen])->all(),'id','nama');

$tanggal = !empty($_GET['tanggal']) ? $_GET['tanggal'] : date('Y-m-d');
$bulans = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        ];

/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanSubStok */

?>
  <div class="row">
    <div class="col-xs-12">
    <?php $form = ActiveForm::begin([
        'action' => ['barang-opname/create'],
        'method' => 'GET',
        'options' => [
            'id' => 'form-opname',
        'class' => 'form-horizontal',
        'role' => 'form'
        ]
    ]); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Unit</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('dept_id',$_GET['dept_id'] ?: '',$listDepartment, ['id'=>'dept_id']);?>

        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Jenis Barang</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('jenis_barang_id',$_GET['jenis_barang_id'] ?: '', $listJenis, ['prompt'=>'..Pilih Jenis Barang..','id'=>'jenis_barang_id']);?>

        </div>
    </div>
   
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> </label>
        <div class="col-sm-2">
         
    <input type="hidden" name="cari" value="1"/>
    <button type="submit" class="btn btn-success" name="btn-cari"><i class="fa fa-search"></i> Cari</button>
        

        </div>
    </div>
    

      
      <?php ActiveForm::end(); ?>
</div>
    </div>
   
    <div class="row">
    <div class="col-xs-12">
        
        <button type="submit" class="btn btn-info" name="simpan" value="1"><i class="fa fa-save"></i> Simpan</button>
        <button type="submit" class="btn btn-danger" name="validate" value="1" id="btn-validate"><i class="fa fa-save"></i> Validate</button>
    </div>
</div>

<?php
$script = "


$(document).on('keydown','input', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
         if($(this).val()=='')
            $(this).val(0);
        var stok = isNaN($(this).attr('data-item')) ? 0 : $(this).attr('data-item');
        var stok_riil = isNaN($(this).val()) ? 0 : $(this).val();
        var selisih = eval(stok) - eval(stok_riil);
        $(this).parent().next().find('.selisih').html(selisih);
        
        var offset = $(this).attr('data-id');
        var count = $('.stok_riil').length;

        
        if(offset % 20 == 0){
            $('#show_more_main').trigger('click');
            glob = $(this);
        }



        var inputs = $(this).closest('#tabel-opname').find(':input:visible');
              
        inputs.eq( inputs.index(this)+ 1 ).focus().select();
        $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        }, 10);


    }
});

$(document).on('keydown','input', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
        
        
    }
});

$(document).ready(function(){

  

    $('#dept_id').change(function(){
        // page = 0;

        // var tanggal = $('#tanggal').val() != '' ? $('#tanggal').val() : '';
        // var dept_id = $('#dept_id').val() != '' ? $('#dept_id').val() : '';
        // loadContent(tanggal, dept_id);
        // $('#show_more_main').trigger('click');
    });
    $('#btn-simpan').click(function(){
        var conf = confirm('Simpan Stok Opname?');
        if(conf)
            $('#form-opname').submit();
    });

    $('#btn-cari').click(function(){
        // var conf = confirm('Simpan Stok Opname?');
        // if(conf)
            $('#form-opname').submit();
    });
});
";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>
</div>


</div>
