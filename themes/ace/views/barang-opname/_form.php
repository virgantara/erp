<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\MasterJenisBarang;
use yii\helpers\Url;

$listJenis=MasterJenisBarang::getList();

/* @var $this yii\web\View */
/* @var $model app\models\BarangOpname */
/* @var $form yii\widgets\ActiveForm */
$listDepartment = \yii\helpers\ArrayHelper::map(\app\models\Departemen::find()->where(['id'=>Yii::$app->user->identity->departemen])->all(),'id','nama');

$tanggal = !empty($_GET['tanggal']) ? $_GET['tanggal'] : date('Y-m-d');
$bulans = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        ];

/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanSubStok */

?>
  <div class="row">
    <div class="col-xs-12">
    <?php $form = ActiveForm::begin([
        'action' => ['barang-opname/create'],
        'method' => 'GET',
        'options' => [
            'id' => 'form-opname',
        'class' => 'form-horizontal',
        'role' => 'form'
        ]
    ]); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Unit</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('dept_id',$_GET['dept_id'] ?: '',$listDepartment, ['id'=>'dept_id']);?>

        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Jenis Barang</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('jenis_barang_id',$_GET['jenis_barang_id'] ?: '', $listJenis, ['prompt'=>'..Pilih Jenis Barang..','id'=>'jenis_barang_id']);?>

        </div>
    </div>
     <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Tanggal Opname</label>
        <div class="col-sm-2">
           <?= DatePicker::widget([
            'name' => 'tanggal',
            'value' => !empty($_GET['tanggal']) ? $_GET['tanggal'] : date('d-m-Y'),
            'options' => ['placeholder' => 'Pilih tanggal opname ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true
            ]
        ]
    ) ?>

        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> </label>
        <div class="col-sm-2">
         
    <input type="hidden" name="cari" value="1"/>
    <button type="submit" class="btn btn-success" name="btn-cari"><i class="fa fa-search"></i> Cari</button>
        

        </div>
    </div>
    

      
      <?php ActiveForm::end(); ?>
</div>
    </div>
    <?php $form = ActiveForm::begin([
        'action' => ['barang-opname/create'],

        'options' => [
            'id' => 'form-opname',
        'class' => 'form-horizontal',
        'role' => 'form'
        ]
    ]); ?>
   

   <div class="form-group">

    <input type="hidden" name="tanggal_pilih" value="<?=$_GET['tanggal'] ?: date('Y-m-d');?>"/>
    <input type="hidden" name="dept_id_pilih" value="<?=$_GET['dept_id'] ?: 0;?>"/>
    <input type="hidden" name="jenis_barang_id" value="<?=$_GET['jenis_barang_id'] ?: 0;?>"/>
     <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
         
    </div>
<?php endif; ?>
    <table class="table table-bordered" id="tabel-opname">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Satuan</th>
                <!-- <th>Batch/ED</th>
                <th>Update Batch/ED</th>
                 --><th>Stok</th>
                <th>Stok<br>Nyata</th>
                 <th>Selisih</th>
            </tr>
        </thead>
        <body>
            <?php 
            foreach($list as $q=>$m)
            {

                $date = !empty($_GET['tanggal']) ? $_GET['tanggal'] : date('Y-m-d');
                $dept_id = !empty($_GET['dept_id']) ? $_GET['dept_id'] : 0;
                $tanggal = date('d',strtotime($date));
                $bulan = date('m',strtotime($date));
                $tahun = date('Y',strtotime($date));

                $stokOp = \app\models\BarangOpname::find()->where([
                    'departemen_stok_id' => $m->id,
                    'tahun' => $tahun.$bulan
                ])->one();


                 
                
                $selisih = 0;
                 $stok_riil = '';
                 if(!empty($stokOp))
                 {
                    $stok_riil = !empty($stokOp) ? $stokOp->stok_riil : $m->stok;
                    $selisih = $m->stok - $stok_riil;   
                 }
                 

                 $tmp_riil = $stok_riil;// != 0 ? $stok_riil : $m->stok;
            ?>
             <tr>
                <td><?=($q+1);?></td>
                <td><?=($m->barang->kode_barang);?></td>
                <td><?=($m->barang->nama_barang);?></td>
                <td><?=($m->barang->id_satuan);?></td>
                <!-- <td><?=$m->batch_no.'/'.$m->exp_date;?></td> -->
                <!-- <td>
                    <input type="text" value="<?=$m->batch_no;?>" name="batch_no_<?=$m->id;?>"/>
                     <?= \yii\jui\DatePicker::widget([
                            'value' => $m->exp_date ?: date('Y-m-d'),
                            'name' => 'exp_date_'.$m->id,
                            'options' => ['placeholder' => 'Pilih EXP Date ...'],
                            // 'formatter' => [
                                'dateFormat' => 'php:Y-m-d',
                                // 'todayHighlight' => true
                            // ]
                        ]) 
                        ?>
                </td> -->
                <td><?=($m->stok);?></td>
                <td><input value="<?=$tmp_riil;?>" type="number" style="width: 80px" data-barang_id="<?=$m->barang_id;?>" data-item="<?=$m->stok;?>" data-stok_id="<?=$m->id;?>" data-id="<?=($q+1);?>" class="stok_riil" name="stok_riil_<?=$m->id;?>"/></td>
                <td><span class="selisih"><?=$selisih;?></span>

                </td>
            </tr>
                <?php 
            }
                ?>
                 <!-- <tr id="show_more">
                <td colspan="7" style="text-align: center;" >

                    <a class="show_more_main btn btn-info" style="width: 100%" id="show_more_main" href="javascript:void(0)">
                        <span class="show_more" title="Load more posts">Show more</span>
                        <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
                    </a>

                </td>
            </tr> -->
            
        </body>
    </table>
    <div class="row">
    <div class="col-xs-12">
        
        <button type="submit" class="btn btn-info" name="simpan" value="1"><i class="fa fa-save"></i> Simpan</button>
        <button type="submit" class="btn btn-danger" name="validate" value="1" id="btn-validate"><i class="fa fa-save"></i> Validate</button>
    </div>
</div>
    </div>
    
    <?php ActiveForm::end(); ?>

<?php
$script = "


$(document).on('keydown','input', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
         if($(this).val()=='')
            $(this).val(0);
        var stok = isNaN($(this).attr('data-item')) ? 0 : $(this).attr('data-item');
        var stok_riil = isNaN($(this).val()) ? 0 : $(this).val();
        var selisih = eval(stok_riil) - eval(stok);
        $(this).parent().next().find('.selisih').html(selisih);
        
        var offset = $(this).attr('data-id');
        var count = $('.stok_riil').length;

        
        if(offset % 20 == 0){
            $('#show_more_main').trigger('click');
            glob = $(this);
        }



        var inputs = $(this).closest('#tabel-opname').find(':input:visible');
              
        inputs.eq( inputs.index(this)+ 1 ).focus().select();
        $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        }, 10);


    }
});

$(document).on('keydown','input', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
        
        
    }
});

$(document).ready(function(){

  

    $('#dept_id').change(function(){
        // page = 0;

        // var tanggal = $('#tanggal').val() != '' ? $('#tanggal').val() : '';
        // var dept_id = $('#dept_id').val() != '' ? $('#dept_id').val() : '';
        // loadContent(tanggal, dept_id);
        // $('#show_more_main').trigger('click');
    });
    $('#btn-simpan').click(function(){
        var conf = confirm('Simpan Stok Opname?');
        if(conf)
            $('#form-opname').submit();
    });

    $('#btn-cari').click(function(){
        // var conf = confirm('Simpan Stok Opname?');
        // if(conf)
            $('#form-opname').submit();
    });
});
";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>
</div>
