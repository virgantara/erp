<?php

use yii\helpers\Html;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SalesFakturSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sales Fakturs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-faktur-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sales Faktur', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax'=>true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_faktur',
            'namaSuplier',
            'no_faktur',
            'tanggal_faktur',
            // 'created_at',
            // 'updated_at',
             [
                'attribute' => 'is_approved',
                'label' => 'Status Faktur',
                'format' => 'raw',
                'filter'=>["0"=>"Belum","1"=>"Disetujui"],
                'value'=>function($model,$url){

                    $st = '';
                    $label = '';

                    switch ($model->is_approved) {
                        case 1:
                            $label = 'Disetujui';
                            $st = 'success';
                            break;
                        default:
                            $label = 'Belum';
                            $st = 'danger';
                            break;
                    }
                    
                    return '<button type="button" class="btn btn-'.$st.' btn-sm" >
                               <span>'.$label.'</span>
                            </button>';
                    
                },
            ],
            //'id_perusahaan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    
</div>
