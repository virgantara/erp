<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReturBarangCustomer */

$this->title = 'Update Retur Barang Customer: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Retur Barang Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="retur-barang-customer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'penjualan' => $penjualan,
         'penjualanResep'=>$penjualanResep
    ]) ?>

</div>
