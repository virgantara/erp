<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ReturBarangCustomer */

$this->title = $model->kodePenjualan;
$this->params['breadcrumbs'][] = ['label' => 'Retur Barang Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="retur-barang-customer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'kodePenjualan',
            // 'departemen_id',
            'unitPenerima',
            'namaPasien',
            'customer_id',
            'tanggal',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'retur_id',
            'namaBarang',
            'qty',
            // 'hb',
            // 'hj',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Option',
                'template' => '{delete}'
            ],
        ],
    ]); ?>
</div>
