<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

use yii\helpers\Url;
use yii\web\JsExpression;
use keygenqt\autocompleteAjax\AutocompleteAjax;
use yii\jui\AutoComplete;

use app\models\JenisResep;
$listJenisResep = \app\models\JenisResep::getListJenisReseps();
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Penjualan */
?>

<div class="penjualan-form">
<h3>Data Penjualan <?=$rawat[$jenis_rawat];?></h3>
<div class="row">
    <div class="col-sm-4">
         <?php $form = ActiveForm::begin([
            'action' => ['retur-barang-customer/create-tanpa-resep'],
            'method' => 'GET',
         ]); ?>
    <div class="form-group col-xs-12 col-lg-12">
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name"> No Px </div>

                <div class="profile-info-value pull-left">
                    <div class="col-sm-8 ">
                    <?= $form->field($model, 'customer_id')->label(false); ?>
                    </div>
                    <div class="col-sm-4">
                     &nbsp;
                     <?= Html::submitButton('Cari Px', ['class' => 'btn btn-success btn-sm']) ?>
            </div>
                </div>
            </div>

            
        </div>

      
     <?php ActiveForm::end(); ?>
   
</div>

</div>

<?php

     if(!empty($dataProvider))
     {?>

    <div class="col-sm-12">
   <?php $form = ActiveForm::begin([
         'action' => ['retur-barang-customer/simpan-retur-by-rm'],
        'method' => 'POST',
   ]); ?>        
    <?= $form->errorSummary($model) ?>
       <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
         
    </div>
<?php endif; ?>
 <?php if (Yii::$app->session->hasFlash('danger')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('danger') ?>
         
    </div>
<?php endif; ?>
<?= $form->field($model, 'penjualan_id')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'customer_id')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'departemen_id')->hiddenInput()->label(false); ?>

 <div class="form-group col-xs-12 col-lg-12">
        <div class="profile-user-info profile-user-info-striped">


            <div class="profile-info-row">
                <div class="profile-info-name"> Tanggal Retur </div>

                <div class="profile-info-value">
                    
                     <input name="tanggal"  type="text" id="tanggal" value="<?=date('Y-m-d');?>"/>
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Nama Px </div>

                <div class="profile-info-value">
                    <span class="editable" id="nama_px"><?=$pasien['namapx'] ?: '';?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Alamat</div>

                <div class="profile-info-value">
                    <span class="editable" ><?=$pasien['alamat'] ?: '';?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> JK</div>

                <div class="profile-info-value">
                    <span class="editable" ><?=$pasien['jk'] ?: '';?></span>
                </div>
            </div>
        </div>

      
   
</div>

     

<?php
     echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'kode_penjualan',
                    'RMPasien',
                    // 'barang_id',
                    // 'satuan',
                    'tanggal',
                    
                    // 'qty',
                    // //'harga_satuan',
                    // 'harga_total',
                    'namaUnit',
                    'created_at',
                     [
                        'attribute' => 'status_penjualan',
                        'label' => 'Status',
                        'format' => 'raw',
                        'filter'=>["1"=>"SUDAH BAYAR","0"=>"BELUM BAYAR","2"=>"BON"],
                        'value'=>function($model,$url){

                            $st = '';
                            $label = '';

                            switch ($model->status_penjualan) {
                                case 1:
                                    $label = 'SUDAH BAYAR';
                                    $st = 'success';
                                    break;
                                case 2:
                                    $label = 'BON';
                                    $st = 'warning';
                                    break;
                                default:
                                    $label = 'BELUM BAYAR';
                                    $st = 'danger';
                                    break;
                            }
                            
                            return '<button type="button" class="btn btn-'.$st.' btn-sm" >
                                       <span>'.$label.'</span>
                                    </button>';
                            
                        },
                    ],
                    

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} ',
                        'buttons' => [
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                           'title'        => 'delete',
                                            'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$url', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                    $('#alert-message').html('<div class=\"alert alert-success\">Data berhasil dihapus</div>');
                                                    $('#alert-message').show();    
                                                    $('#alert-message').fadeOut(2500);
                                                });
                                            }
                                            return false;
                                        ",
                                            // 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                            // 'data-method'  => 'post',
                                ]);
                            },
                            

                            'view' => function ($url, $model) {
                               return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                           'title'        => 'view',
                                           'data-item' => $model->id,
                                           'class' => 'view-barang',
                                            // 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                            // 'data-method'  => 'post',
                                ]);
                            },
                            'update' => function ($url, $model) {
                               return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                           'title'        => 'Update',
                                           'data-item' => $model->id,
                                            // 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                            // 'data-method'  => 'post',
                                ]);
                            },
                        ],
                        'visibleButtons' => [
                            'bayar' => function ($model) {
                                return !empty($model->penjualanResep) ? $model->penjualanResep->jenis_rawat == 2 && $model->penjualanResep->pasien_jenis == 'UMUM' : false;
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            
                            if ($action === 'delete') {
                                $url =\yii\helpers\Url::to(['penjualan/delete','id'=>$model->id]);
                                return $url;
                            }

                            else if ($action === 'update') {
                                $url =\yii\helpers\Url::to(['retur-barang-customer/update-resep','id'=>$model->id]);
                                return $url;
                            }

                            else if ($action === 'view') {
                                $url ='javascript:void(0)';
                                return $url;
                            }

                        }
                       
                    ],
                ],
            ]); 
    
            ?>
    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
      <?php ActiveForm::end(); ?>   
    </div><!-- /.col -->
<?php
}
?>

</div>

<?php 

\yii\bootstrap\Modal::begin([
    'header' => '<h2>Komposisi</h2>',
    'size' => 'modal-lg',
    'toggleButton' => ['label' => '','id'=>'modal-view','style'=>'display:none'],
    
]);

?>
<table class="table table-striped table-bordered" id="tabel-komposisi">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Signa 1</th>
            <th>Signa 2</th>
            <!-- <th>Kekuatan</th> -->
            <th>Dosis<br>Minta</th>
            <th>Qty</th>
            <th>Subtotal</th>
            <th>Option</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>
<?php

\yii\bootstrap\Modal::end();
?>

<?php
$script = "

function popup(url,label) {
    var w = screen.width * 0.6;
    var h = screen.height * 0.6;
    var left = (screen.width  - w) / 2;
    var top = (screen.height - h) / 2;
    
    newwindow=window.open(url,label,'height='+h+',width='+w+',top='+top+',left='+left);
    if (window.focus) {newwindow.focus()}
    return false;
}


$(document).on('click','a.view-barang', function(e) {
    e.preventDefault();
    var id = $(this).attr('data-item');
    var url = '/penjualan/komposisi/'+id;
    popup(url,'Komposisi');
    // alert(url);
    // $('#jumlah_update').val($(this).attr('data-qty'));

});

$(document).ready(function(){

    $('#tanggal').datetextentry();
});

$(document).on('keydown','input', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
        var inputs = $(this).closest('.penjualan-form').find(':input:visible');
              
        inputs.eq( inputs.index(this)+ 1 ).focus().select();
        $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        }, 10);


    }
});


$(document).on('keydown','.jq-dte-day, .jq-dte-month',function(e){
   
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13){
        e.preventDefault();

        cekResep($('#customer_id').val(),$('#tanggal').val());
    }
});

";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>