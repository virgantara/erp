<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Penjualan */

$this->title = 'Retur Barang Tak Bertuan';
$this->params['breadcrumbs'][] = ['label' => 'Retur', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penjualan-create">


    <?= $this->render('_tanpa_resep', [
        'model' => $model,
        'pasien'=>$pasien,
        'penjualan' => $penjualan,
        'penjualanResep'=>$penjualanResep,
        'dataProvider' => $dataProvider
    ]) ?>

</div>
