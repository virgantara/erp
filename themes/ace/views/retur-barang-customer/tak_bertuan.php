<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Penjualan */

$this->title = 'Retur Barang';
$this->params['breadcrumbs'][] = ['label' => 'Retur', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penjualan-create">


    <?= $this->render('_tak_bertuan', [
        'model' => $model,
        'penjualan' => $penjualan,
         'penjualanResep'=>$penjualanResep
        // 'jenis_rawat'=>$jenis_rawat
    ]) ?>

</div>
