<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\web\JsExpression;
use keygenqt\autocompleteAjax\AutocompleteAjax;
use yii\jui\AutoComplete;

use app\models\JenisResep;
$listJenisResep = \app\models\JenisResep::getListJenisReseps();
use kartik\date\DatePicker;
use app\models\Departemen;
/* @var $this yii\web\View */
/* @var $model app\models\Penjualan */

$rawat = [1 => 'Rawat Jalan',2=>'Rawat Inap',3=>'IGD'];
?>
<style type="text/css">
    .ui-state-focus {
  background: none !important;
  background-color: #4F99C6 !important;
  /*border: none !important;*/
}
</style>
<div class="penjualan-form">
<h3>Data Retur <?=$rawat[$jenis_rawat];?></h3>
<div class="row">
    <div class="col-sm-12">
     <?php $form = ActiveForm::begin(); ?> 
   <?= $form->field($model, 'uuid')->hiddenInput()->label(false) ?>
    <div class="form-group col-xs-12 col-lg-12">
        <div class="profile-user-info profile-user-info-striped">


            <div class="profile-info-row">
                <div class="profile-info-name"> Tanggal Retur </div>

                <div class="profile-info-value">
                    
                     <input name="tanggal"  type="text" id="tanggal" value="<?=date('Y-m-d');?>"/>
                </div>
            </div>
        </div>
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name"> UNIT dituju </div>

                <div class="profile-info-value pull-left">
                    <?php $listDataDept=Departemen::getListDepartemens();?>
<?= $form->field($model, 'departemen_penerima_id')->dropDownList($listDataDept, ['prompt'=>'..Pilih Unit Tujuan..','class'=>'form-control'])->label(false);?>                  
                </div>
            </div>

        </div>

       
        <div class="profile-user-info profile-user-info-striped">


            <div class="profile-info-row">
                <div class="profile-info-name"> Nama Barang</div>

                <div class="profile-info-value">
                              <?php 


    // $url = \yii\helpers\Url::to(['/sales-stok-gudang/ajax-barang']);
    
   
echo AutoComplete::widget([
    'name' => 'nama_barang',
    'id' => 'nama_barang',
    'clientOptions' => [
    'source' => Url::to(['departemen-stok/ajax-barang']),
    'autoFill'=>true,
    'minLength'=>'1',
    'create' => new JsExpression('function(event, ui) {

      $(event.target).autocomplete("instance")._renderItem = function(ul, item) {
        var row = "<div class=\'row\'><div class=\'col-lg-6 col-xs-6 col-sm-6 col-md-6\'>" + item.kode + "</div><div class=\'col-lg-6 col-xs-6 col-sm-6 col-md-6\'>Stok : " + item.stok + "</div><div class=\'col-lg-12 col-xs-12 col-sm-12 col-md-12\'>HJ : " + item.harga_jual + "</div><div class=\'col-lg-12 col-xs-12 col-sm-12 col-md-12\'>" + item.nama + "</div></div>";
        return $("<li>").append(row).appendTo(ul);
       };
    }'),
    'select' => new JsExpression("function( event, ui ) {
        $('#barang_id').val(ui.item.id);
        $('#hj').val(ui.item.harga_jual);
        $('#hb').val(ui.item.harga_beli);        
        
     }")],
    'options' => [
        'size' => '40',
        'tabindex' => 6
    ]
 ]);
    ?>

    <input type="hidden" id="barang_id"/>
    <input type="hidden" id="hb"/>
    <input type="hidden" id="hj"/>
    <button type="button" id="btn-insert" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Input</button> 
                </div>
            </div>
        </div>

</div>
    <div class="col-sm-12">
         
    <?= $form->errorSummary($model) ?>
       <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
         
    </div>
<?php endif; ?>
 <?php if (Yii::$app->session->hasFlash('danger')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('danger') ?>
         
    </div>
<?php endif; ?>

<div id="info" style="display: none;"></div>
<table class="table table-striped table-bordered" id="table-item">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama</th>
                <th style="text-align: center;">HB</th>
                <th style="text-align: center;">HJ</th>
                
                <th style="text-align: center;" width="15%">Jumlah Retur</th>                
            </tr>
        </thead>
        <tbody>
            
            
        </tbody>
    </table>
    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
      <?php ActiveForm::end(); ?>   
    </div><!-- /.col -->


</div>

<?php
$script = "


function refreshTable(hsl){
    var row = '';
    $('#table-item > tbody').empty();
    var ii = 0, jj = 0;
    $.each(hsl.items,function(i,ret){
        ii++;
        row += '<tr>';
        row += '<td>'+eval(ii)+'</td>';

        row += '<td>'+ret.kd_brg+'</td>';
        row += '<td>'+ret.nm_brg+'</td>';
        row += '<td>'+ret.hb+'</td>';
        row += '<td>'+ret.hj+'</td>';
        row += '<td><input type=\"text\" name=\"qty_'+ret.id+'\"/></td>';
        row += '</tr>';
    

        
    });

    // row += '<tr>';
    // row += '<td colspan=\"7\" style=\"text-align:right\"><strong>Total Biaya</strong></td>';
    // row += '<td style=\"text-align:right\"><strong>'+hsl.total+'</strong></td>';
    // row += '<td></td>';
    
    // row += '</tr>';

    // $('#total_biaya').html(hsl.total);

    $('#table-item').append(row);
}

$(document).ready(function(){

    $('#tanggal').datetextentry();

    $('#btn-insert').click(function(){
        var dp = new Object;
        dp.barang_id = $('#barang_id').val();
        dp.uuid = $('#returbarangcustomer-uuid').val();
        dp.tanggal = $('#tanggal').val();
        dp.hb = $('#hb').val();
        dp.hj = $('#hj').val();
        $.ajax({
            url : '".\yii\helpers\Url::toRoute(['retur-barang-customer/ajax-add-item'])."',
            type : 'POST',
            data : {
                dataPost : dp
            },
            beforeSend: function(){
                $('#info').hide();
            },
            error : function(err){
                $('#info').hide();
                console.log(err.responseText);
            },
            success : function(res){
                var res = $.parseJSON(res);
                $('#info').show();
                if(res.code == '200'){
                    $('#info').html('<div class=\"alert alert-'+res.short+'\">'+res.message+'</div>');

                    refreshTable(res);
                    $('#nama_barang').val('');
                    $('#nama_barang').focus();
                }   

                else{
                    $('#info').html('<div class=\"alert alert-'+res.short+'\">'+res.message+'</div>');
                }    
            }
        });
    });
});

$(document).on('keydown','input', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
        var inputs = $(this).closest('.penjualan-form').find(':input:visible');
              
        inputs.eq( inputs.index(this)+ 1 ).focus().select();
        $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        }, 10);


    }
});


$(document).on('keydown','.jq-dte-day, .jq-dte-month',function(e){
   
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13){
        e.preventDefault();

        // cekResep($('#customer_id').val(),$('#tanggal').val());
    }
});

";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>