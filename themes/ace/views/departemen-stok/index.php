<?php

use yii\helpers\Html;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PerusahaanSubStokSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Unit Stoks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-sub-stok-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
     <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
         
    </div>
<?php endif; ?>
    <p>
        <?= Html::a('Create Stok Unit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
$gridColumns =  [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'namaDepartemen',
                'label' => 'Unit',
                'format' => 'raw',
                'filter'=>$departemens,
                'value'=>function($model,$url){
                    return $model->namaDepartemen;
                    
                },
            ],
            [
                'attribute' => 'jenisBarang',
                'label' => 'Jenis',
                'format' => 'raw',
                'filter'=>$listJenisBarang,
                'value'=>function($model,$url){
                    return $model->jenisBarang;
                    
                },
            ],
            'kodeBarang',
            'namaBarang',
            
            'barang.id_satuan',
            'hargaJual',
            'hargaBeli',
            // 'stok_akhir',
            // 'stok_awal',
            //'created',
            //'bulan',
            //'tahun',
            //'tanggal',
            //'stok_bulan_lalu',
            'stok',
            'stok_minimal',
            'stok_maksimal',
            //'ro_item_id',
            [
             
                'label' => 'Removed',
                'format' => 'raw',
                'visible' => Yii::$app->user->can('gudang'),
                'filter'=>["1"=>"Removed","0"=>"Not Removed"],
                'value'=>function($model,$url){

                    $st = '';
                    $label = '';
                    switch($model->is_hapus)
                    {
                        case 1 : 
                            $st = 'danger';
                            $label = 'Removed';
                        break;
                        case 0 : 
                            $st = 'success';
                            $label = 'Not Removed';
                        break;

                    }
                    
                    
                    return '<button type="button" class="btn btn-'.$st.' btn-sm" >
                               <span>'.$label.'</span>
                            </button>';
                    
                },
            ],
            [
             
                'label' => 'Kondisi Stok',
                'format' => 'raw',
                // 'filter'=>["1"=>"Disetujui","0"=>"Belum","2"=>"Diproses"],
                'value'=>function($model,$url){

                    $st = '';
                    $label = '';

                    if ($model->stok > 0 && $model->stok <= $model->stok_minimal) {
                        $label = 'Kurang';
                        $st = 'warning';
                    }

                    else if($model->stok <= 0){
                        $label = 'Habis';
                        $st = 'danger';
                            
                    }

                    else{
                        $label = 'Mencukupi';
                        $st = 'success';
                    }
                    
                    return '<button type="button" class="btn btn-'.$st.' btn-sm" >
                               <span>'.$label.'</span>
                            </button>';
                    
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {restore}',
                'buttons' => [
                    'restore' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, [
                                   'title'        => 'Restore',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to restore this item?'),
                                    'data-method'  => 'post',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                   'title'        => 'Remove',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to remove this item?'),
                                    'data-method'  => 'post',
                        ]);
                    }
                ],
                'visibleButtons' => [
                    'update' => Yii::$app->user->identity->access_role == 'kepalaCabang',
                    'restore' => Yii::$app->user->can('gudang'),
                    'delete' => Yii::$app->user->can('gudang'), 
                ]
            ],
        ];
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'pjax' => true
    ]); ?>
</div>
