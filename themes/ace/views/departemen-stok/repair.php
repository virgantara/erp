<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\MasterJenisBarang;


$listJenis=MasterJenisBarang::getList();
$theme = $this->theme;
/* @var $this yii\web\View */
/* @var $model app\models\BarangOpname */
/* @var $form yii\widgets\ActiveForm */
$listDepartment = \yii\helpers\ArrayHelper::map(\app\models\Departemen::find()->where(['id'=>Yii::$app->user->identity->departemen])->all(),'id','nama');

/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanSubStok */

$this->title = 'Repair Stok unit';
$this->params['breadcrumbs'][] = $this->title;


?>

<style type="text/css">
    /* RADIOS & CHECKBOXES STYLES */
/* SOURCE: https://gist.github.com/dciccale/1367918 */
/* No more blue blur border */
input {
  outline: none;
}

/* base styles */
input[type="checkbox"] {
    height: 20px;
    width: 20px;
    vertical-align: middle;
    margin: 0 0.4em 0.4em 0;
    background: rgba(255, 255, 255, 1);
    border: 1px solid #AAAAAA;
    -webkit-appearance: none;
}

/* border radius for radio*/


/* border radius for checkbox */
input[type="checkbox"] {
    border-radius: 2px;
}

/* hover state */
input[type="checkbox"]:not(:disabled):hover {
    border: 1px solid rgba(58, 197, 201, 1);
}

/* active state */
input[type="checkbox"]:active:not(:disabled) {
    border: 1px solid rgba(58, 197, 201, 1);
}

/* input checked border color */

input[type="checkbox"]:checked {
    border: 1px solid rgba(58, 197, 201, 1);
}

input[type="checkbox"]:checked:not(:disabled) {
    background: rgba(58, 197, 201, 1);
}


/* checkbox checked */
input[type="checkbox"]:checked:before {
    font-weight: bold;
    color: white;
    content: '\2713';
    margin-left: 2px;
    font-size: 14px;
}
</style>
<div class="perusahaan-sub-stok-create">

    <h1><?= Html::encode($this->title) ?></h1>

  <div class="barang-opname-form">

    <?php $form = ActiveForm::begin([
        'action' => ['departemen-stok/repair'],
        'method' => 'GET',
        'options' => [

        'id' => 'form-opname',

        'class' => 'form-horizontal',
        'role' => 'form'
        ]
    ]); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Unit</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('dept_id',$_GET['dept_id'] ?: '',$listDepartment, ['id'=>'dept_id']);?>

        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Jenis Barang</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('jenis_barang_id',$_GET['jenis_barang_id'] ?: '', $listJenis, ['prompt'=>'..Pilih Jenis Barang..','id'=>'jenis_barang_id']);?>

        </div>
    </div>
    
     <div class="clearfix form-actions">
    <div class="col-md-offset-2 col-md-10">
    <input type="hidden" name="cari" value="1"/>
    <button type="submit" class="btn btn-success" name="btn-cari"><i class="fa fa-search"></i> Cari Barang</button>
        </div></div>


        </div>
    </div>
      <?php ActiveForm::end(); ?>

    <?php $form = ActiveForm::begin([
        'action' => ['departemen-stok/remove-duplicate'],
        'options' => [
            'id' => 'form-opname',
        'class' => 'form-horizontal',
        'role' => 'form'
        ]
    ]); ?>
    <input type="hidden" name="simpan" value="1"/>
   

   <div class="form-group">


    <input type="hidden" name="dept_id_pilih" value="<?=$_GET['dept_id'] ?: 0;?>"/>
    <input type="hidden" name="jenis_barang_id_pilih" value="<?=$_GET['jenis_barang_id'] ?: 0;?>"/>

     <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
         
    </div>
<?php endif; ?>
    <table class="table table-bordered" id="tabel-opname">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th><input type="checkbox" id="nama_item">Nama</th>
                <th>Satuan</th>
                <th>Stok</th>
                 <th>Option</th>
            </tr>
        </thead>
        <body>
            <?php 
            foreach($list as $q=>$m)
            {
                $dept_id = !empty($_POST['dept_id']) ? $_POST['dept_id'] : 0;
            ?>
             <tr>
                <td><span class="numbering"><?=($q+1);?></span></td>
                <td><?=($m->barang->kode_barang);?></td>
                <td>
                    <label for="<?=$m->id;?>"><input id="<?=$m->id;?>" type="checkbox" class="checkbox_nama" data-item="<?=$m->id;?>"><?=($m->barang->nama_barang);?>
                        </label>
                </td>
                <td><?=($m->barang->id_satuan);?></td>
                
                <td>
                    
<input type="text" name="stok_awal_<?=$m->id;?>" value="<?=$m->stok;?>" size="5"/>    
                </td>
                <td>
                    <a class="btn btn-danger btn-remove" href="javascript:void(0)" data-item="<?=$m->id;?>"><i class="fa fa-trash"></i> Remove</a>

                </td>
            </tr>
                <?php 
            }
                ?>
        
            
        </body>
    </table>
    <div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        
        <button type="submit" class="btn btn-warning" name="btn-update" value="1"><i class="fa fa-adjust"></i> Update Data</button>
    </div>
</div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
<?php
$script = "

function reorderNumber(){
    var i = 1;
    $('.numbering').each(function(){
        $(this).html(i);
        i++;
    });
}

$(document).ready(function(){

    $('#nama_item').change(function(){

        var value = this.checked;
        $('.checkbox_nama').each(function(){
            $(this).prop('checked',value);
        });
    });

    
    $('.btn-remove').click(function(){
        var opsi = confirm('Hapus item ini dari unit stok?');
        if(opsi){
            var tmp = $(this).attr('data-item');
            var btn = $(this);
            $.ajax({
                type:'POST',
                url:'".\yii\helpers\Url::toRoute(['departemen-stok/ajax-remove-unit-stok'])."',
                data:{
                    dataPost : tmp
                },
                async : true,
                error : function(e){
                    console.log(e.responseText);
                },
                beforeSend : function(){
                    
                },
                success:function(html){
                   btn.parent().parent().remove();
                   reorderNumber();
                }
            });
        }
        
    });

   
});
";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>
</div>
