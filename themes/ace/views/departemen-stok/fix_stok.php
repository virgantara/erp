<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\MasterJenisBarang;


$listJenis=MasterJenisBarang::getList();
$theme = $this->theme;
/* @var $this yii\web\View */
/* @var $model app\models\BarangOpname */
/* @var $form yii\widgets\ActiveForm */
$listDepartment = \yii\helpers\ArrayHelper::map(\app\models\Departemen::find()->where(['id'=>Yii::$app->user->identity->departemen])->all(),'id','nama');

/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanSubStok */

$this->title = 'Fixing Stocks';
$this->params['breadcrumbs'][] = $this->title;
$bulans = [
    '01' => 'Januari',
    '02' => 'Februari',
    '03' => 'Maret',
    '04' => 'April',
    '05' => 'Mei',
    '06' => 'Juni',
    '07' => 'Juli',
    '08' => 'Agustus',
    '09' => 'September',
    '10' => 'Oktober',
    '11' => 'November',
    '12' => 'Desember',
];

$tahun = !empty($_GET['tahun']) ? $_GET['tahun'] : date('Y');

$listTahun = [];

for($i = 2018;$i<date('Y')+5;$i++)
    $listTahun[$i] = $i;

?>

<style type="text/css">
    /* RADIOS & CHECKBOXES STYLES */
/* SOURCE: https://gist.github.com/dciccale/1367918 */
/* No more blue blur border */
input {
  outline: none;
}

/* base styles */
input[type="checkbox"] {
    height: 20px;
    width: 20px;
    vertical-align: middle;
    margin: 0 0.4em 0.4em 0;
    background: rgba(255, 255, 255, 1);
    border: 1px solid #AAAAAA;
    -webkit-appearance: none;
}

/* border radius for radio*/


/* border radius for checkbox */
input[type="checkbox"] {
    border-radius: 2px;
}

/* hover state */
input[type="checkbox"]:not(:disabled):hover {
    border: 1px solid rgba(58, 197, 201, 1);
}

/* active state */
input[type="checkbox"]:active:not(:disabled) {
    border: 1px solid rgba(58, 197, 201, 1);
}

/* input checked border color */

input[type="checkbox"]:checked {
    border: 1px solid rgba(58, 197, 201, 1);
}

input[type="checkbox"]:checked:not(:disabled) {
    background: rgba(58, 197, 201, 1);
}


/* checkbox checked */
input[type="checkbox"]:checked:before {
    font-weight: bold;
    color: white;
    content: '\2713';
    margin-left: 2px;
    font-size: 14px;
}
</style>
<div class="perusahaan-sub-stok-create">

    <h1><?= Html::encode($this->title) ?></h1>

  <div class="barang-opname-form">

    <?php $form = ActiveForm::begin([
        'action' => ['departemen-stok/merge-stok'],
        'method' => 'GET',
        'options' => [

        'id' => 'form-opname',

        'class' => 'form-horizontal',
        'role' => 'form'
        ]
    ]); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Unit</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('dept_id',$_GET['dept_id'] ?: '',$listDepartment, ['id'=>'dept_id']);?>

        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Jenis Barang</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('jenis_barang_id',$_GET['jenis_barang_id'] ?: '', $listJenis, ['prompt'=>'..Pilih Jenis Barang..','id'=>'jenis_barang_id']);?>

        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Bulan</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('bulan',$_GET['bulan'] ?: date('m'), $bulans, ['prompt'=>'..Pilih Bulan..','id'=>'bulan']);?>

        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Tahun</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('tahun',$_GET['tahun'] ?: '', $listTahun, ['prompt'=>'..Pilih Tahun..','id'=>'tahun']);?>

        </div>
    </div>
     <div class="clearfix form-actions">
    <div class="col-md-offset-2 col-md-10">
    <input type="hidden" name="cari" value="1"/>
    <button type="submit" class="btn btn-success" name="btn-cari"><i class="fa fa-search"></i> Cari Barang</button>
        </div></div>


        </div>
    </div>
      <?php ActiveForm::end(); ?>

    <?php $form = ActiveForm::begin([
        'action' => ['departemen-stok/fix-stok-awal-bulan'],
        'method' => 'GET',
        'options' => [
            'id' => 'form-opname',
        'class' => 'form-horizontal',
        'role' => 'form'
        ]
    ]); ?>
    <input type="hidden" name="simpan" value="1"/>
   

   <div class="form-group">

    <input type="hidden" name="bulan_pilih" value="<?=$_GET['bulan'] ?: date('m');?>"/>
    <input type="hidden" name="dept_id_pilih" value="<?=$_GET['dept_id'] ?: 0;?>"/>
    <input type="hidden" name="jenis_barang_id_pilih" value="<?=$_GET['jenis_barang_id'] ?: 0;?>"/>
    <input type="hidden" name="tahun_pilih" value="<?=$_GET['tahun'] ?: date('Y');?>"/>
     <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
         
    </div>
<?php endif; ?>
    <table class="table table-bordered" id="tabel-opname">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th><input type="checkbox" id="nama_item">Nama</th>
                <th>Satuan</th>
                <th>Out</th>
                <th>In</th>
                <th>Sisa</th>
                <th>Tgl Opname</th>
                 <th>Option</th>
            </tr>
        </thead>
        <body>
            <?php 

                        $counter = 0;
            $dept_id = $_GET['dept_id'];
            $tgl = $_GET['tahun'].'-'.$_GET['bulan'].'-01';
            $nowMonth = $_GET['tahun'].'-'.$_GET['bulan'];
            // $prevMonth = date('Y-m-d',strtotime('-1 month',strtotime($tgl)));
            $prevMonth = date('Y-m',strtotime('-1 month',strtotime($tgl)));
            $query = \app\models\DepartemenStok::find();
            $query->where(['<>','barang.nama_barang','-']);
            $query->andWhere(['departemen_id'=>$dept_id]);
            $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id']]);
            // $query->andWhere(['departemen_id'=>Yii::$app->user->identity->departemen]);
            $query->andWhere(['barang.is_hapus'=>0]);
            $query->joinWith(['barang as barang']);
            $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $listBarang = $query->all();

            foreach($listBarang as $q => $b)
            {

               
                $query = \app\models\StokHistory::find()->alias('bo');
                $query->where(['<>','barang.nama_barang','-']);
                $query->andWhere(['bo.departemen_id'=>$dept_id]);
                $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id']]);
                // $query->andWhere(['created_at'=>$tahunbulan]);
                $query->andFilterWhere(['between', 'bo.created_at', $nowMonth.'-01 00:00:00', $nowMonth.'-31 23:59:59']);
                $query->andWhere(['barang.is_hapus'=>0,'bo.barang_id'=>$b->barang_id]);
                $query->joinWith(['barang as barang']);
                // $query->limit(1)
                $query->orderBy(['bo.created_at'=>SORT_ASC]);
                // $tgl = $_GET['tahun'].'-'.$_GET['bulan'].'-01';
                // $nextMonth = date('m',strtotime('+1 month',strtotime($tgl)));
                // $sd = $_GET['tahun'].'-'.$_GET['bulan'].'-01';
                // $ed = date('Y-m-t',strtotime($sd));
                // print_r($sd);exit;
                $list = $query->all();

                // if(count($list) == 0) continue;
            
               
                $stok_awal_bulan = \app\models\StokHistory::getLastMonthStok($b->barang_id, $dept_id,$prevMonth);

                if(empty($stok_awal_bulan)) continue;

                 $counter ++;

                // print_r($stok_awal_bulan);exit;
                ?>
                 <tr>
                <td><?=$counter;?></td>
                <td><?=$b->barang->kode_barang;?></td>
                <td><?=$b->barang->nama_barang;?></td>
                <td>Awal Bulan</td>
                <td></td>
                <td></td>
                <td><?=$stok_awal_bulan->sisa;?></td>
                <td><?=$stok_awal_bulan->created_at;?></td>
                <td></td>
            </tr>
                <?php
                foreach($list as $sq=>$m)
                {

                    // $date = !empty($_POST['tanggal']) ? $_POST['tanggal'] : date('Y-m-d');
                    // $dept_id = !empty($_POST['dept_id']) ? $_POST['dept_id'] : 0;
                    // $tanggal = date('d',strtotime($date));
                    // $bulan = date('m',strtotime($date));
                    // $tahun = date('Y',strtotime($date));

                    //  $stokOp = \app\models\BarangOpname::find()->where([
                    //     'departemen_stok_id' => $m->id,
                    //     'tahun' => $tahun.$bulan
                    // ])->one();

                    //  $selisih = 0;
                    //  $stok_riil = '';
                    //  if(!empty($stokOp))
                    //  {
                    //     $stok_riil = !empty($stokOp) ? $stokOp->stok_riil : $m->stok;
                    //     $selisih = $m->stok - $stok_riil;   
                    //  }
                    
                //  // if($m->stok < 300) continue;
                //  $tmp_riil = $stok_riil;// != 0 ? $stok_riil : $m->stok;
            ?>
             <tr>
                <td></td>
                <td><?=($m->barang->kode_barang);?></td>
                <td>
                    <?=($m->barang->nama_barang);?>
                    
                </td>
                <td><?=($m->barang->id_satuan);?></td>
                <td><?=($m->qty_out);?>
                <td><?=($m->qty_in);?>
                <td><?=($m->sisa);?>
                    
                </td>
                <td><?=$m->created_at;?></td>
                <td>
                    <?=$m->keterangan;?>
                   <!--  <a class="btn btn-warning btn-merge" href="javascript:void(0)" data-item="<?=$m->id;?>"><i class="fa fa-adjust"></i> Merge Stocks</a>
                    <a class="btn btn-danger btn-remove" href="javascript:void(0)" data-item="<?=$m->id;?>"><i class="fa fa-trash"></i> Remove</a>
 -->
                </td>
            </tr>
                <?php 
            }
        }
                ?>
                 <!-- <tr id="show_more">
                <td colspan="7" style="text-align: center;" >

                    <a class="show_more_main btn btn-info" style="width: 100%" id="show_more_main" href="javascript:void(0)">
                        <span class="show_more" title="Load more posts">Show more</span>
                        <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
                    </a>

                </td>
            </tr> -->
            
        </body>
    </table>
    <div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        
        <button type="submit" class="btn btn-warning" ><i class="fa fa-adjust"></i> Update Data</button>
    </div>
</div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
<?php
$script = "

function reorderNumber(){
    var i = 1;
    $('.numbering').each(function(){
        $(this).html(i);
        i++;
    });
}

$(document).ready(function(){

    $('#nama_item').change(function(){

        var value = this.checked;
        $('.checkbox_nama').each(function(){
            $(this).prop('checked',value);
        });
    });

    $('.btn-merge').click(function(){
        var opsi = confirm('Merge stok item ini dengan yang lain?');
        if(opsi){
            var tmp = $(this).attr('data-item');
            $.ajax({
                type:'POST',
                url:'".\yii\helpers\Url::toRoute(['departemen-stok/ajax-merge'])."',
                data:{
                    dataPost : tmp
                },
                async : true,
                error : function(e){
                    console.log(e.responseText);
                },
                beforeSend : function(){
                    
                },
                success:function(html){
                   
                   window.location.reload(1);
                }
            });
        }
    });

    $('.btn-remove').click(function(){
        // var opsi = confirm('Hapus item ini dari unit stok?');
        // if(opsi){
            var tmp = $(this).attr('data-item');
            var btn = $(this);
            $.ajax({
                type:'POST',
                url:'".\yii\helpers\Url::toRoute(['departemen-stok/ajax-remove-stok'])."',
                data:{
                    dataPost : tmp
                },
                async : true,
                error : function(e){
                    console.log(e.responseText);
                },
                beforeSend : function(){
                    
                },
                success:function(html){
                   btn.parent().parent().remove();
                   reorderNumber();
                }
            });
        // }
        
    });

   
});
";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>
</div>
