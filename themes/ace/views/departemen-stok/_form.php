<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use keygenqt\autocompleteAjax\AutocompleteAjax;
use app\models\MasterJenisBarang;
use kartik\depdrop\DepDrop;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanSubStok */
/* @var $form yii\widgets\ActiveForm */

$listDepartment = \app\models\Departemen::getListDepartemens();
$listJenis=MasterJenisBarang::getList();

$model->departemen_id = Yii::$app->user->identity->access_role == 'kepalaCabang' ? $model->departemen_id : Yii::$app->user->identity->departemen;

?>

<div class="perusahaan-sub-stok-form">

    <?php $form = ActiveForm::begin(['options'=>[
        'id' => 'form-stok'
    ]]); ?>
     <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
         
    </div>
<?php endif; ?>

 <?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('error') ?>
         
    </div>
<?php endif; ?>
    <?= $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']) ?>
    <?= $form->field($model, 'departemen_id')->dropDownList($listDepartment, ['prompt'=>'..Pilih Departemen..']); ?>
      
<label>Cari Barang</label>
      <?php 
       
echo AutoComplete::widget([
    'name' => 'nama_barang',
    'id' => 'nama_barang',
    'clientOptions' => [
    'source' =>new JsExpression('function(request, response) {
                $.getJSON("'.Url::to(['departemen-stok/ajax-barang-not-in-departemen/']).'", {
                    term: request.term,
                    dept_id: '.Yii::$app->user->identity->departemen.'
                }, response);
     }'),
    'autoFill'=>true,
    'minLength'=>'3',
    'create' => new JsExpression('function(event, ui) {

      $(event.target).autocomplete("instance")._renderItem = function(ul, item) {
        var row = "<div class=\'row\'><div class=\'col-lg-6 col-xs-6 col-sm-6 col-md-6\'>" + item.kode + "</div><div class=\'col-lg-6 col-xs-6 col-sm-6 col-md-6\'></div><div class=\'col-lg-12 col-xs-12 col-sm-12 col-md-12\'>HJ : " + item.harga_jual + "</div><div class=\'col-lg-12 col-xs-12 col-sm-12 col-md-12\'>" + item.nama + "</div></div>";
        return $("<li>").append(row).appendTo(ul);
       };
    }'),
    'select' => new JsExpression("function( event, ui ) {
        // $('#jml_minta').focus();
        // console.log(ui.item);
        $('#departemenstok-barang_id').val(ui.item.id);
        $('#nama_brg').val(ui.item.nama);
        // $('#bid').val(ui.item.id);
        // $('#item_id').val(ui.item.id);
        // $('#jml_minta').val('0');
        // $('#satuan').val(ui.item.satuan);
     }")],
    'options' => [
        'class' => 'form-control',
        'placeholder' => 'Ketik Nama Obat'
        // 'tabindex' => 6
    ]
 ]);
 ?>  <label>Barang</label>
 <input type="text" readonly="readonly" class="form-control" id="nama_brg" value="<?=$model->barang->nama_barang;?>"/>
     <?= $form->field($model, 'barang_id')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'qty')->textInput() ?>
    <?= $form->field($model, 'stok_minimal')->textInput() ?>
    <?= $form->field($model, 'stok_maksimal')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'btn-simpan']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php 

if(!$model->isNewRecord)
{
  

$script = "


$('#btn-simpan').click(function(e){
    e.preventDefault();
    var stokAwal = '".$model->stok."';
    var stokBaru = $('#departemenstok-stok').val();
    if(stokAwal != stokBaru){
        
        Swal.fire({
          title: 'Ada perbedaan stok',
          text: 'Update stok ini akan dianggap sebagai stok opname dan berpengaruh pada kartu stok. Simpan perubahaan ini?',
          icon: 'warning',
          width: 800,
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, update!'
        }).then((result) => {
          if (result.value) {
            $('#form-stok').submit();
          }
        })
       
    }

    else{
        $('#form-stok').submit();
    }
    
});


";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);

}
?>