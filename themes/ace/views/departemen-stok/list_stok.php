<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\MasterJenisBarang;


$theme = $this->theme;
/* @var $this yii\web\View */
/* @var $model app\models\BarangOpname */
/* @var $form yii\widgets\ActiveForm */
/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanSubStok */

$this->title = 'Stok Unit';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="perusahaan-sub-stok-create">

    <h1><?= Html::encode($this->title) ?></h1>

  <div class="barang-opname-form">


    <table class="table table-bordered" id="tabel-opname">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Satuan</th>
                <th>Stok</th>
                 <th>Stok Minimal</th>
            </tr>
        </thead>
        <body>
            <?php 
            foreach($result as $q=>$m)
            {
                $m = (object)$m;
            ?>
             <tr>
                <td><span class="numbering"><?=($q+1);?></span></td>
                <td><?=($m->kd);?></td>
                <td><?=($m->nm);?></td>
                <td><?=$m->satuan;?></td>
                <td><?=($m->qty);?></td>
                <td><?=$m->min;?></td>
                
            </tr>
                <?php 
            }
                ?>
        
            
        </body>
    </table>
    
    </div>
    

</div>
<?php
$script = "

";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>
</div>
