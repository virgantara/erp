<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanSubStok */

$this->title = $model->namaBarang.' | '.$model->namaDepartemen;
$this->params['breadcrumbs'][] = ['label' => 'Stok Unit', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-sub-stok-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
    // if(Yii::$app->user->can('gudang'))
    // {
    ?>
    <p>

        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Remove', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to remove this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php 
    // }
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'barang.kode_barang',
            'namaBarang',
            'namaDepartemen',
            'stok_akhir',
            'stok_awal',
            'created_at',
            'bulan',
            'tahun',
            'tanggal',
            'stok_bulan_lalu',
            'stok',
            // 'ro_item_id',
        ],
    ]) ?>

</div>
