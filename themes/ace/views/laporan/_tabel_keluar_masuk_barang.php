 <?php 
if(!empty($export)){
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="laporan_rekap_in_out.xls"');
            header('Cache-Control: max-age=0');

?>
    <table>
        <tr>
            <td colspan="7" style="text-align: center">
                
                <h1>Laporan Rekapitulasi Keluar Masuk Barang</h1>
            </td>
        </tr>
    </table>
    <?php
}
?>
 <table class="table table-bordered">
    <thead>
        <tr>
            <th width="5%" style="text-align: center;">No</th>
            <th style="text-align: center;">Kode</th>
            <th style="text-align: center;">Nama</th>
            <th style="text-align: center;">Barang<br>Masuk</th>
            <th style="text-align: center;">Barang<br>Masuk</th>
            <th style="text-align: center;">Total</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        

        foreach($results as $jb)
        {
            if(count($jb['items']) == 0) continue;
        ?>

        <tr>
            <th colspan="7"><?=$jb['nama'];?></th>
        </tr>

        <?php 
            $i=0;    
            foreach($jb['items'] as $m)
            {
                $i++;
        ?>
        <tr>
            <td style="text-align: center;"><?=($i);?></td>
            <td style="text-align: center;"><?=$m['kd'];?></td>
            <td style="text-align: center;"><?=$m['nm'];?></td>
            <td style="text-align: center;"><?=$m['masuk'];?></td>
            <td style="text-align: center;"><?=$m['keluar'];?></td>
            <td style="text-align: right;"><?=$m['subtotal'];?></td>
        </tr>
        <?php

            }
        }
        ?>
    </tbody>
</table>