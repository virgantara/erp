 <?php 
if(!empty($export)){
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="laporan_rekap_opname.xls"');
            header('Cache-Control: max-age=0');

?>
    <table>
        <tr>
            <td colspan="7" style="text-align: center">
                
                <h1>Laporan Rekapitulasi Stok Opname Barang</h1>
            </td>
        </tr>
    </table>
    <?php
}
?>
 <table class="table table-bordered">
    <thead>
        <tr>
            <th width="5%" style="text-align: center;">No</th>
            <th style="text-align: center;">Kode</th>
            <th style="text-align: center;">Nama</th>
            <th style="text-align: center;">Satuan</th>
            <th style="text-align: center;">Jumlah</th>
            <th style="text-align: center;">Harga</th>
            <th style="text-align: center;">Total</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        

        foreach($list as $jb)
        {
        ?>

        <tr>
            <th colspan="7"><?=$jb['nama'];?></th>
        </tr>

        <?php 
            $i=0;    
            foreach($jb['list'] as $m)
            {
                $i++;
        ?>
        <tr>
            <td style="text-align: center;"><?=($i);?></td>
            <td style="text-align: center;"><?=$m['kode'];?></td>
            <td style="text-align: center;"><?=$m['nama'];?></td>
            <td style="text-align: center;"><?=$m['satuan'];?></td>
            <td style="text-align: center;"><?=$m['stok_riil'];?></td>
            <td style="text-align: right;"><?=$m['hb'];?></td>
            <td style="text-align: right;"><?=$m['subtotal'];?></td>
        </tr>
        <?php

            }
        }
        ?>
    </tbody>
</table>