<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
 use yii\jui\AutoComplete;
    use yii\helpers\Url;
    use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\models\BarangOpname */
/* @var $form yii\widgets\ActiveForm */


$tanggal = !empty($_POST['tanggal']) ? $_POST['tanggal'] : date('Y-m-d');
$this->title = 'Laporan Rekapitulasi Keluar Masuk Barang';
// $this->params['breadcrumbs'][] = ['label' => 'Stok Opname', 'url' => ['barang-opname/create']];
$this->params['breadcrumbs'][] = $this->title;
$listDepartment = \app\models\Departemen::getListDepartemens();
$barang_id = !empty($_GET['barang_id']) ? $_GET['barang_id'] : '';
$nama_barang = !empty($_GET['nama_barang']) ? $_GET['nama_barang'] : '';
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="barang-opname-form">

     <?php $form = ActiveForm::begin([
        'action' => ['laporan/keluar-masuk-barang'],
        'method' => 'get',
        'options' => [
            'id' => 'form-opname',
            
            'class' => 'form-horizontal',
            'role' => 'form'
        ]
    ]); ?>
    
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Tanggal Awal</label>
        <div class="col-sm-2">
           <?= \yii\jui\DatePicker::widget([
             'options' => ['placeholder' => 'Pilih tanggal awal ...','id'=>'tanggal_awal'],
             'name' => 'tanggal_awal',
             'value' => $_GET['tanggal_awal'] ?: date('01-m-Y'),

            'dateFormat' => 'php:d-m-Y',
        ]
    ) ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Tanggal Akhir</label>
        <div class="col-sm-2">
           <?= \yii\jui\DatePicker::widget([
             'options' => ['placeholder' => 'Pilih tanggal akhir ...','id'=>'tanggal_akhir'],
             'name' => 'tanggal_akhir',
             'value' => $_GET['tanggal_akhir'] ?: date('d-m-Y'),
            'dateFormat' => 'php:d-m-Y',
        ]
    ) ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Kode/Nama Barang </label>

        <div class="col-sm-2">
               <?php 
            AutoComplete::widget([
    'name' => 'nama_barang_item',
    'id' => 'nama_barang_item',

    'clientOptions' => [
    'source' => Url::to(['sales-master-barang/ajax-search']),
    'autoFill'=>true,
    'minLength'=>'3',
    'select' => new JsExpression("function( event, ui ) {
        $('#barang_id').val(ui.item.id);
        $('#nama_barang').val(ui.item.nama);
     }")],
    'options' => [
        'size' => '40',
        // 'value' => '-asdf',
    ]
 ]); 
 ?>
            <input type="text" id="nama_barang_item" placeholder="Kode/Nama Barang" class="form-control" value="<?=$nama_barang;?>" name="nama_barang"/>
             <input type="hidden" id="barang_id" name="barang_id" value="<?=$barang_id;?>"/>
             
        </div>
    </div>

        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> </label>
            <div class="col-sm-2">
 <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Cari', ['class' => 'btn btn-info','name'=>'search','value'=>1]) ?>
  <button type="reset" id="btn-reset" class="btn btn-default">Reset</button>
 <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Export XLS', ['class' => 'btn btn-success','name'=>'export','value'=>1]) ?>   
            </div>
  
        </div>
      <?php ActiveForm::end(); ?>
       <?php 
   echo $this->render('_tabel_keluar_masuk_barang', [
             'results' => $results,
            'model' => $model,
        ]); 
    ?>
  

  
</div>

<?php


$this->registerJs(' 
    $("#btn-reset").click(function(e){
        e.preventDefault();
        
        $("input").val("");
    });
    

    ', \yii\web\View::POS_READY);

?>