<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
// use keygenqt\autocompleteAjax\AutocompleteAjax;
// use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SalesStokGudangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Rekap Penjualan Obat';
$this->params['breadcrumbs'][] = $this->title;

$model->tanggal_awal = !empty($_GET['SalesMasterBarang']['tanggal_awal']) ? $_GET['SalesMasterBarang']['tanggal_awal'] : date('Y-m-01');
$model->tanggal_akhir = !empty($_GET['SalesMasterBarang']['tanggal_akhir']) ? $_GET['SalesMasterBarang']['tanggal_akhir'] : date('Y-m-d');


?>

    <h1><?= Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-xs-12">
      
<?php $form = ActiveForm::begin([
	'method' => 'get',
    'options' => ['class' => 'form-horizontal'],

	'action' => array('laporan/penjualan-rekap')
]);
?>
<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Unit</label>
    <div class="col-sm-10">
        <?= $form->field($model, 'dept_id',['options'=>['class'=>'form-group col-xs-12 col-lg-6']])->dropDownList(\yii\helpers\ArrayHelper::map($listDepartment,'id','nama'),['prompt'=>'Pilih Unit'])->label(false); ?>   

    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="form-field-2"> Tanggal</label>
    <div class="col-sm-10">
        <?php

$model->date_range = !empty($_GET['SalesMasterBarang']) ? $_GET['SalesMasterBarang']['date_range'] : date('01-m-Y').' sampai '.date('t-m-Y');

echo $form->field($model, 'date_range', [
    'addon'=>['prepend'=>['content'=>'<i class="fa fa-calendar"></i>']],
    'options'=>['class'=>'drp-container form-group col-xs-12 col-lg-6']
])->widget(DateRangePicker::classname(), [
    'useWithAddon'=>true,
    'convertFormat'=>true,
    'startAttribute' => 'tanggal_awal',
    'endAttribute' => 'tanggal_akhir',
    'pluginOptions'=>[
        'locale'=>[
            'format'=>'d-m-Y',
            'separator'=>' sampai ',
        ],
        'opens'=>'left'
    ],
    'options'=>[
        'autocomplete' => 'off',

    ]
])->label(false);

     ?>

    </div>
</div>
<div class="form-group">

    <div class="col-sm-offset-2">
        <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Cari', ['class' => 'btn btn-info','name'=>'search','value'=>1]) ?>    
        <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Export XLS', ['class' => 'btn btn-success','name'=>'export','value'=>1]) ?>
            </div>
</div>




    <?php ActiveForm::end(); ?>
</div></div>
 <?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-exclamation"></i> <?= Yii::$app->session->getFlash('error') ?>
         
    </div>
<?php endif; ?>


 <table class="table table-bordered table-striped">
        <thead>
            <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Qty</th>
            <!-- <th>HB</th> -->
            <th>Harga Jual</th>
            <th>Subtotal</th>
            
        </tr>
        </thead>
        <tbody>
            <?php 
            $total = 0;
            $i = 0;
            foreach($results as $key => $model)
            {
                $model = (object)$model;
                $qty = $model->qty;
                
                if($qty == 0) continue;
                
                $subtotal = $qty * $model->hj;
                $total += $subtotal;
            ?>
            <tr>
                <td><?=($i+1);?></td>
                <td><?=$model->kd;?></td>
                <td><?=$model->nm;?></td>
                <td><?=round($qty,2);?></td>
               <td style="text-align: right;"><?=$export ? round($model->hj,2): \app\helpers\MyHelper::formatRupiah($model->hj,2);?></td>
                <td style="text-align: right;"><?=$export ? round($subtotal,2): \app\helpers\MyHelper::formatRupiah($subtotal,2);?></td>
                

            </tr>
            <?php 
            $i++;
           }
            ?>

        </tbody>
        <tfoot>
            <tr>
                <td colspan="5" style="text-align: right">Total </td>
                <td style="text-align: right;"><?=$export ? round($total,2) : \app\helpers\MyHelper::formatRupiah($total,2);?></td>
                
            </tr>
        </tfoot>
    </table>
</div>
