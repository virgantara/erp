<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BarangOpname */
/* @var $form yii\widgets\ActiveForm */


$tanggal = !empty($_POST['tanggal']) ? $_POST['tanggal'] : date('Y-m-d');
$this->title = 'Laporan Rekapitulasi Stok Opname';
$this->params['breadcrumbs'][] = ['label' => 'Stok Opname', 'url' => ['barang-opname/create']];
$this->params['breadcrumbs'][] = $this->title;
$listDepartment = \app\models\Departemen::getListDepartemens();

?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="barang-opname-form">

     <?php $form = ActiveForm::begin([
        'action' => ['laporan/opname-rekap'],
        'method' => 'get',
        'options' => [
            'id' => 'form-opname',
            
            'class' => 'form-horizontal',
            'role' => 'form'
        ]
    ]); ?>
    
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Tanggal Awal</label>
        <div class="col-sm-2">
           <?= \yii\jui\DatePicker::widget([
             'options' => ['placeholder' => 'Pilih tanggal awal ...','id'=>'tanggal_awal'],
             'name' => 'tanggal_awal',
             'value' => $_GET['tanggal_awal'] ?: date('01-m-Y'),

            'dateFormat' => 'php:d-m-Y',
        ]
    ) ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Tanggal Akhir</label>
        <div class="col-sm-2">
           <?= \yii\jui\DatePicker::widget([
             'options' => ['placeholder' => 'Pilih tanggal akhir ...','id'=>'tanggal_akhir'],
             'name' => 'tanggal_akhir',
             'value' => $_GET['tanggal_akhir'] ?: date('d-m-Y'),
            'dateFormat' => 'php:d-m-Y',
        ]
    ) ?>
        </div>
    </div>
    

        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> </label>
            <div class="col-sm-2">
 <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Cari', ['class' => 'btn btn-info','name'=>'search','value'=>1]) ?>    
 <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Export XLS', ['class' => 'btn btn-success','name'=>'export','value'=>1]) ?>   
            </div>
  
        </div>
      <?php ActiveForm::end(); ?>
       <?php 
   echo $this->render('_tabel_rekap', [
             'list' => $list,
            'model' => $model,
        ]); 
    ?>
  

  
</div>
