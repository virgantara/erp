 <?php 
use app\models\DepartemenStokHistory;

if(!empty($export)){
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="laporan_opname.xls"');
            header('Cache-Control: max-age=0');


?>
    <table>
        <tr>
            <td colspan="8" style="text-align: center">
                
                <h1>Laporan Stok Opname Barang</h1>
            </td>
        </tr>
    </table>
    <?php
}
?>
 <table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Harga Beli</th>
            <th>Stok</th>
            <!-- <th>Masuk</th>
             <th>Keluar</th>-->
            <th>Selisih</th>
            <th>Total</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $i=0;

        $qty_in = 0;
        $qty_out = 0;

        $total = 0;

        foreach($list as $m)
        {
            $subtotal = $m->selisih * $m->barang->harga_beli;
            // $sub_selisih = $m->selisih * $m->barang->harga_beli;
            $total += $subtotal;
            $i++;
        ?>
        <tr>
            <td><?=($i);?></td>
            <td><?=$m->barang->kode_barang;?></td>
            <td><?=$m->barang->nama_barang;?></td>
            <td><?=\app\helpers\MyHelper::formatRupiah($m->barang->harga_beli,2);?></td>
            <td><?=$m->stok;?></td>
            <td><?=$m->selisih;?></td>
            <td align="right"><?=\app\helpers\MyHelper::formatRupiah($subtotal,2);?></td>
            <td>-</td>
        </tr>
        <?php 
            }
        
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6" align="right">Grand Total</td>
            <td style="text-align: right"><strong><?=\app\helpers\MyHelper::formatRupiah($total,2);?></strong></td>
            <td>-</td>
        </tr>
    </tfoot>
</table>