<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\MasterJenisBarang;

/* @var $this yii\web\View */
/* @var $model app\models\BarangOpname */
/* @var $form yii\widgets\ActiveForm */


$tanggal = !empty($_POST['tanggal']) ? $_POST['tanggal'] : date('Y-m-d');
$this->title = 'Laporan Stok Bulanan';
$this->params['breadcrumbs'][] = $this->title;
$listDepartment = \app\models\Departemen::getListDepartemens();


$bulans = [
    '01' => 'Januari',
    '02' => 'Februari',
    '03' => 'Maret',
    '04' => 'April',
    '05' => 'Mei',
    '06' => 'Juni',
    '07' => 'Juli',
    '08' => 'Agustus',
    '09' => 'September',
    '10' => 'Oktober',
    '11' => 'November',
    '12' => 'Desember',
];

?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="barang-opname-form">

     <?php $form = ActiveForm::begin([
        'action' => ['laporan/stok-bulanan'],
        'options' => [
            'id' => 'form-bulanan',
        'class' => 'form-horizontal',
        'role' => 'form'
        ]
    ]); ?>
     <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Unit</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('dept_id',!empty($_POST['dept_id']) ? $_POST['dept_id'] : $_POST['dept_id'],$listDepartment, ['prompt'=>'..Pilih Unit..','id'=>'dept_id']);?>

        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Jenis Barang</label>
        <div class="col-sm-2">
          <?= Html::dropDownList('bulan',$_POST['bulan'] ?: '', $bulans, ['prompt'=>'..Pilih Bulan..','id'=>'bulan']);?>

        </div>
    </div>
    

        <div class="form-group">
            <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> </label>
            <div class="col-sm-2">
 <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Cari', ['class' => 'btn btn-info','name'=>'search','value'=>1]) ?>    
 <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Export XLS', ['class' => 'btn btn-success','name'=>'export','value'=>1]) ?>   
            </div>
  
        </div>
      <?php ActiveForm::end(); ?>
       <?php 
   echo $this->render('_stok_bulanan', [
             'list' => $list,
            'model' => $model,
        ]); 
    ?>
  

  
</div>
