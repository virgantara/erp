<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

// use keygenqt\autocompleteAjax\AutocompleteAjax;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SalesStokGudangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use app\models\JenisResep;
$listJenisResep = \app\models\JenisResep::getListJenisReseps();

$this->title = 'Laporan Resep Per Pasien';
$this->params['breadcrumbs'][] = $this->title;

$model->tanggal_awal = !empty($_GET['Penjualan']['tanggal_awal']) ? $_GET['Penjualan']['tanggal_awal'] : date('Y-m-01');
$model->tanggal_akhir = !empty($_GET['Penjualan']['tanggal_akhir']) ? $_GET['Penjualan']['tanggal_akhir'] : date('Y-m-d');
?>

<style type="text/css">
    .even{
        background-color: #A9F5E1
    }

    .odd{
        background-color: #D0F5A9   
    }
</style>
<div class="sales-stok-gudang-index">

    <h1><?= Html::encode($this->title) ?></h1>
  
    <?php $form = ActiveForm::begin([
    	'method' => 'get',
    	'action' => ['laporan/resep-pasien'],
        'options' => [
            'class' => 'form-horizontal'
        ]
    ]); ?>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Tanggal Awal</label>
        <div class="col-lg-2 col-sm-10">
          <?= yii\jui\DatePicker::widget(
            [
                'model' => $model,
                'attribute' => 'tanggal_awal',
            // 'value' => date('d-m-Y'),
            'options' => ['placeholder' => 'Pilih tanggal awal ...'],
            // 'formatter' => [
                'dateFormat' => 'php:d-m-Y',
                // 'todayHighlight' => true
            // ]
        ]      
    ) ?> 
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Tanggal Akhir</label>
        <div class="col-lg-2 col-sm-10">
          <?= yii\jui\DatePicker::widget(
            [
                'model' => $model,
                'attribute' => 'tanggal_akhir',
            // 'value' => date('d-m-Y'),
            'options' => ['placeholder' => 'Pilih tanggal akhir ...'],
            // 'formatter' => [
                'dateFormat' => 'php:d-m-Y',
                // 'todayHighlight' => true
            // ]
        ]      
    ) ?> 
        </div>
    </div>
    
      <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> No RM</label>
        <div class="col-lg-2 col-sm-10">
        
             <input name="customer_id"  type="text" id="customer_id" value="<?=!empty($_GET['customer_id']) ? $_GET['customer_id'] : '';?>"/> 
             
        </div>
    </div>
    <div class="col-sm-2">

 
</div>
<div class="col-sm-3">

    <div class="form-group"><br>
        <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Cari', ['class' => 'btn btn-info','name'=>'search','value'=>1]) ?>    
        <?= Html::submitButton(' <i class="ace-icon fa fa-check bigger-110"></i>Export XLS', ['class' => 'btn btn-success','name'=>'export','value'=>1]) ?>    
    </div>

</div>
     


    <?php ActiveForm::end(); ?>

    <table class="table table-bordered">
    	<thead>
    		<tr>
            <th>No</th>
            <th>Tgl</th>
    		<th>Nama Px</th>
    		<th>No RM</th>
    		<th>No Resep</th>
    		<th>Jenis<br>Resep</th>
            <th>Poli</th>
            <th>Dokter</th>
            <th>Kode Obat</th>
            <th>Nama Obat</th>
            <th>Qty</th>
            <th>Subtotal</th>
            <th>Total</th>
    	</tr>
    	</thead>
    	<tbody>
    		<?php 
            $total = 0;
            

            $class = '';
            if(!empty($results[0]['data']))
            {
    		foreach($results as $key => $obj)
    		{

                
                $i++;
                $model = $obj['data'];
                $items = $obj['items'];
                // $subtotal = $model['sb_blt'];
                $total += round($model->total);
                $kd = '';$nm = '';$qty = 0; $hb = 0;$hj = 0;
                if(!empty($items[0])){
                    $kd = $items[0]['kd'];
                    $nm = $items[0]['nm'];
                    $qty = $items[0]['qty'];
                    $hj = $items[0]['hj'];
                    $hb = $items[0]['hb'];
                }

                $sub = ceil($qty) * $hj; 
                $class = $key % 2 == 0 ? 'even' : 'odd';

                // print_r($items[0]);exit;
    		?>
    		<tr>
                <td class="<?=$class;?>"><?=$key+1;?></td>
    			<td class="<?=$class;?>"><?=date('d/m/Y',strtotime($model->tanggal));?></td>
                <td class="<?=$class;?>"><?=$model->nm;?></td>
                <td class="<?=$class;?>"><?=$model->pid;?></td>
                <td class="<?=$class;?>"><?=$model->kode_penjualan;?></td>
                <td class="<?=$class;?>"><?=$listJenisResep[$model->jrid];?></td>
                <td class="<?=$class;?>"><?=$model->unm;?></td>
                <td class="<?=$class;?>"><?=$model->dnm;?></td>
                <td class="<?=$class;?>"><?=$kd;?></td>
                <td class="<?=$class;?>"><?=$nm;?></td>
                <td  class="<?=$class;?>" style="text-align: center;"><?=$qty;?></td>
                <td  class="<?=$class;?>" style="text-align: right;"><?=\app\helpers\MyHelper::formatRupiah($sub,0);?></td>
                <td  class="<?=$class;?>" style="text-align: right;font-weight: bold;"><?=\app\helpers\MyHelper::formatRupiah($model->total,0);?></td>

    		</tr>
    		<?php 
                foreach($items as $q => $it)
                {
                    if($q==0 ) continue;



                     $sub = $it['qty'] * $it['hj'];

?>
            <tr>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"><?=$it['kd'];?></td>
                <td class="<?=$class;?>"><?=$it['nm'];?></td>
                <td class="<?=$class;?>" style="text-align: center;"><?=$it['qty'];?></td>
                 <td  class="<?=$class;?>" style="text-align: right;;"><?=\app\helpers\MyHelper::formatRupiah($sub,0,1);?></td>
                <td class="<?=$class;?>"></td>
            </tr>
<?php
                }
    	   }
        }

        else{

    		?>
            <tr><td colspan="13">Data tidak ditemukan</td></tr>
            <?php
        }
?>
    	</tbody>
        <tfoot>
            <tr>
                <td colspan="12" style="text-align: right;font-weight: bold">Total</td>
                <td style="text-align: right;font-weight: bold"><?=\app\helpers\MyHelper::formatRupiah($total,0,1);?></td>
                
            </tr>
        </tfoot>
    </table>
   
</div>
<?php

$uid = !empty($_GET['unit_id']) ? $_GET['unit_id'] : '';
$script = "

function fetchAllRefUnit(tipe){
    $.ajax({
        type : 'POST',
        data : 'tipe='+tipe,
        url : '".\yii\helpers\Url::toRoute(['api/ajax-all-ref-unit'])."',

        success : function(data){
            var data = $.parseJSON(data);
            
            $('#unit_id').empty();
            var row = '';
            row += '<option value=\"\">- Pilih Unit -</option>';
            $.each(data,function(i, obj){
                if(obj.id == '".$uid."')
                    row += '<option selected value='+obj.id+'>'+obj.label+'</option>';
                else
                    row += '<option value='+obj.id+'>'+obj.label+'</option>';
            });

            $('#unit_id').append(row);
            
        }

    });
}

$(document).ready(function(){
    fetchAllRefUnit(1);

    $('#jenis_rawat').change(function(){
        fetchAllRefUnit($(this).val());
    });
});

";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>