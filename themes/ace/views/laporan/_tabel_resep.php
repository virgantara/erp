 <?php 
if($export){
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="laporan_resep.xls"');
    header('Cache-Control: max-age=0');
}
?>
 <table class="table table-bordered table-striped">
    	<thead>
    		<tr>
            <th>No</th>
            <th>Tgl</th>
    		<th>Nama Px</th>
    		<th>No RM</th>
    		<th>No Resep</th>
    		<th>Jenis<br>Resep</th>
            <th>Poli</th>
            <th>Dokter</th>
            <th>Jumlah ke<br>BPJS</th>
            <th>Jumlah ke<br>Apotik</th>
            <th>Total Jumlah</th>
    		
    	</tr>
    	</thead>
    	<tbody>
    		<?php 
            $total = 0;
            $total_ke_apotek = 0;
            $total_ke_bpjs = 0;

    		foreach($results as $key => $model)
            {
                $model = (object)$model;
                
                $total += round($model->sb);

    		?>
    		<tr>
                <td><?=($key+1);?></td>
    			<td><?=date('d/m/Y',strtotime($model->tgl));?></td>
                <td><?=$model->nm;?></td>
    			<td><?=$model->rm;?></td>
    			<td><?=$model->trx;?></td>
                <td><?=$listJenisResep[$model->jr];?></td>
                <td><?=$model->unm;?></td>
                <td><?=$model->dnm;?></td>
                <td style="text-align: right"><?=$export ? round($model->jb,0): \app\helpers\MyHelper::formatRupiah($model->jb,0);?> </td>
                <td style="text-align: right"><?=$export ? round($model->ja,0): \app\helpers\MyHelper::formatRupiah($model->ja,0);?></td>
                <td style="text-align: right"><?=$export ? round($model->sb,0): \app\helpers\MyHelper::formatRupiah($model->sb,0);?></td>
                

    		</tr>
    		<?php 
    	   }
    		?>

    	</tbody>
        <tfoot>
            <tr>
                <td colspan="8" style="text-align: right">Total</td>
                <td style="text-align: right"><?=$export ? round($total_ke_bpjs,0) : \app\helpers\MyHelper::formatRupiah($total_ke_bpjs,0);?></td>
                <td style="text-align: right"><?=$export ? round($total_ke_apotek,0) : \app\helpers\MyHelper::formatRupiah($total_ke_apotek,0);?></td>
                <td style="text-align: right"><?=$export ? round($total,0) : \app\helpers\MyHelper::formatRupiah($total,0);?></td>
                
            </tr>
        </tfoot>
    </table>
   