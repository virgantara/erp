 <?php 
if(!empty($export)){
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="laporan_stok_bulanan.xls"');
            header('Cache-Control: max-age=0');

?>
    <table>
        <tr>
            <td colspan="8" style="text-align: center">
                
                <h1>Laporan Stok Opname Barang</h1>
            </td>
        </tr>
    </table>
    <?php
}
?>
 <table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Harga Beli</th>
            <!-- <th>Satuan</th> -->
            <th>Stok Akhir Bln<br>Sblm</th>
            <th>Masuk</th>
            <th>Keluar</th>
            <th>Stok Awal Bln<br>Skrg</th>
            <th>Keterangan</th>
            <!-- <th>Total</th> -->
        </tr>
    </thead>
    <tbody>
        <?php 
        $i=0;

        foreach($list as $m)
        {
            $i++;
        ?>
        <tr>
            <td><?=($i);?></td>
            <td><?=$m['kb'];?></td>
            <td><?=$m['nm'];?></td>
            <td class="text-right"><?=\app\helpers\MyHelper::formatRupiah($m['hb'],2);?></td>
            <td><?=$m['stoklalu'];?></td>
            <td><?=$m['masuk'];?></td>
            <td><?=$m['keluar'];?></td>
            <td><?=$m['stokawal'];?></td>
            <td></td>
        </tr>
        <?php 
            }
        
        ?>
    </tbody>
</table>