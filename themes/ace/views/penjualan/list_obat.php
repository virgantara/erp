<table class="table table-striped table-bordered" id="tabel-komposisi">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Stok</th>
            <th>Satuan</th>
            <th>HB</th>
            <th>HJ</th>
            <th>Option</th>
        </tr>
    </thead>
    <tbody>
<?php 


    $i = 0;
    foreach($results as $obj)
    {
        $i++;
        $obj = (object)$obj;
        
        if($obj->nm == '-') continue;    
        
        $row .= '<tr>';
        $row .= '<td>'.($i).'</td>';
        $row .= '<td>'.$obj->kd.'</td>';
        $row .= '<td>'.$obj->nm.'</td>';
        $row .= '<td>'.$obj->stok.'</td>';
        $row .= '<td>'.$obj->sat.'</td>';
        $row .= '<td>'.$obj->hb.'</td>';
        $row .= '<td>'.$obj->hj.'</td>';
        $row .= '<td></td>';
        $row .= '</tr>';
   
        
    }
   
    echo $row;
?>
    </tbody>
</table>

<?php
$script = "

$(document).on('click','a.print-etiket', function(e) {

    var id = $(this).attr('data-item');
    var urlResep = '/penjualan/print-etiket?id='+id;
    popitup(urlResep,'Etiket',0);
    
});



";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);


?>