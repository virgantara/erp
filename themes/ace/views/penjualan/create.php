<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Penjualan */

$this->title = 'Penjualan';
$this->params['breadcrumbs'][] = ['label' => 'Penjualan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penjualan-create">


    <?= $this->render('_form', [
        'model' => $model,
        'list_obat' => $list_obat,
        'jenis_rawat'=>$jenis_rawat,
        'kode_transaksi' => $kode_transaksi
    ]) ?>

</div>
