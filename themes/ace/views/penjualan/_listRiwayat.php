<?php
use yii\helpers\Url;
use yii\helpers\Html;

use app\models\JenisResep;
$listJenisResep = \app\models\JenisResep::getListJenisReseps();


?>
 <table class="table table-bordered">
        <thead>
            <tr>
            <th>No</th>
            <th>Tgl</th>
           
            <th>No Resep</th>
            <th>Jenis<br>Resep</th>
            <th>Poli</th>
            <th>Dokter</th>
            <th>Kode Obat</th>
            <th>Nama Obat</th>
            <th>Qty</th>
            <th>Signa</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $total = 0;
            

            $class = '';
            if(!empty($results[0]['data']))
            {
            foreach($results as $key => $obj)
            {

                
                $i++;
                $model = $obj['data'];
                $items = $obj['items'];
                // $subtotal = $model['sb_blt'];
                $total += $model->total;
                $kd = '';$nm = '';$qty = 0; $hb = 0;$hj = 0;$s1 = '';$s2='';
                if(!empty($items[0])){
                    $kd = $items[0]['kd'];
                    $nm = $items[0]['nm'];
                    $qty = $items[0]['qty'];
                    $hj = $items[0]['hj'];
                    $hb = $items[0]['hb'];
                    $s1 = $items[0]['sig1'];
                    $s2 = $items[0]['sig2'];
                }

                $sub = ceil($qty) * $hj; 
                $class = $key % 2 == 0 ? 'even' : 'odd';
                // print_r($items[0]);exit;
            ?>
            <tr>
                <td class="<?=$class;?>"><?=$key+1;?></td>
                <td class="<?=$class;?>"><?=date('d/m/Y',strtotime($model->tanggal));?></td>
                
                <td class="<?=$class;?>"><?=$model->kode_penjualan;?></td>
                <td class="<?=$class;?>"><?=$listJenisResep[$model->jrid];?></td>
                <td class="<?=$class;?>"><?=$model->unm;?></td>
                <td class="<?=$class;?>"><?=$model->dnm;?></td>
                <td class="<?=$class;?>"><?=$kd;?></td>
                <td class="<?=$class;?>"><?=$nm;?></td>
                <td  class="<?=$class;?>" style="text-align: center;"><?=$qty;?></td>
                <td  class="<?=$class;?>" style="text-align: right;">
                    
                    <?php
                    echo $s1.' x '.$s2;
                    ?>
                </td>
      
            </tr>
            <?php 
                foreach($items as $q => $it)
                {
                    if($q==0 ) continue;

                     $sub = ceil($it['qty']) * $it['hj']; 

                     $s1 = $it['sig1'];
                     $s2 = $it['sig2'];
?>
            <tr>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"></td>
                <td class="<?=$class;?>"><?=$it['kd'];?></td>
                <td class="<?=$class;?>"><?=$it['nm'];?></td>
                <td class="<?=$class;?>" style="text-align: center;"><?=$it['qty'];?></td>
                <td  class="<?=$class;?>" style="text-align: right;;">
                    <?php
                    echo $s1.' x '.$s2;
                    ?>
                        
                    </td>
                
            </tr>
<?php
                }
           }
        }

        else{

            ?>
            <tr><td colspan="10">Data tidak ditemukan</td></tr>
            <?php
        }
?>
        </tbody>
       
    </table>