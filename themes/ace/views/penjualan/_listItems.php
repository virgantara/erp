<?php 
use yii\helpers\Url;

?>
<div class="row" id="listItems">
<input type="hidden" id="cart_id"/>
<table class="table table-striped table-bordered" id="table-item">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama</th>
                <th style="text-align: center;">Signa 1</th>
                <th style="text-align: center;">Signa 2</th>
                <th style="text-align: center;">Harga</th>
                <th style="text-align: center;">Qty</th>
                
                <th style="text-align: center;">Subtotal</th>
                <th style="text-align: center;">Option</th>                
            </tr>
        </thead>
        <tbody>
            <?php 
            $ii = 0;
            $jj = 0;
            $total = 0;
            
            foreach($rows as $q => $ret)
            {
                $row = '';
                if($ret->is_racikan == '1')
                {

                    if($ii == 0){
                        $row .= '<tr><td colspan="9" style="text-align:left">Racikan</td></tr>';
                    }

                    //  if($ret->qty > 1)
                    //     $subtotal_bulat = round($ret->harga) * ceil($ret->qty);
                    // else
                    $subtotal_bulat = round($ret->harga,2) * ceil($ret->qty);
                    
                    $subtotal = $subtotal_bulat;
                    $total += $subtotal_bulat;
                    $ii++;
                    $row .= '<tr>';
                    $row .= '<td>'.($ii).'</td>';
                    $row .= '<td>'.$ret->departemenStok->barang->kode_barang.'</td>';
                    $row .= '<td>'.$ret->departemenStok->barang->nama_barang.'</td>';
                    $row .= '<td style="text-align:center">'.$ret->signa1.'</td>';
                    $row .= '<td style="text-align:center">'.$ret->signa2.'</td>';
                    $row .= '<td style="text-align:right">'.\app\helpers\MyHelper::formatRupiah($ret->harga).'</td>';
                    $row .= '<td style="text-align:center">'.$ret->qty_bulat.'</td>';
                    $row .= '<td style="text-align:right">'. \app\helpers\MyHelper::formatRupiah($ret->subtotal_bulat).'</td>';
                    $row .= '<td><a href="javascript:void(0)" class="cart-update" data-item="'.$ret->id.'"><i class="glyphicon glyphicon-pencil"></i></a>';
                    // $row .= '&nbsp;<a href="javascript:void(0)" class="cart-delete" data-item="'.$ret->id.'"><i class="glyphicon glyphicon-trash"></i></a></td>';
                    $row .= '</tr>';
                    echo $row;
                }

                else
                {
                
                
                    if($jj == 0){
                        $row .= '<tr><td colspan="9" style="text-align:left">Non-Racikan</td></tr>';
                    }
                    // print_r($ret->attributes);
                    // if($ret->qty > 1)
                        // $subtotal_bulat = round($ret->harga) * ceil($ret->qty);
                    // else
                    //     $subtotal_bulat = round($ret->harga) * $ret->qty;
                    
                    
                    $subtotal = round($ret->harga,2) * $ret->qty;
                    
                    $total += $subtotal;

                    $jj++;
                    $row .= '<tr>';
                    $row .= '<td>'.($jj).'</td>';
                    $row .= '<td>'.$ret->departemenStok->barang->kode_barang.'</td>';
                    $row .= '<td>'.$ret->departemenStok->barang->nama_barang.'</td>';
                     $row .= '<td style="text-align:center">'.$ret->signa1.'</td>';
                    $row .= '<td style="text-align:center">'.$ret->signa2.'</td>';
                    $row .= '<td style="text-align:right">'.\app\helpers\MyHelper::formatRupiah($ret->harga).'</td>';
                    $row .= '<td style="text-align:center">'.$ret->qty.'</td>';
                    $row .= '<td style="text-align:right">'.\app\helpers\MyHelper::formatRupiah($subtotal).'</td>';
                    $row .= '<td><a href="javascript:void(0)" class="cart-update" data-item="'.$ret->id.'"><i class="glyphicon glyphicon-pencil"></i></a>';
                    // $row .= '&nbsp;<a href="javascript:void(0)" class="cart-delete" data-item="'.$ret->id.'"><i class="glyphicon glyphicon-trash"></i></a></td>';
                    $row .= '</tr>';
                    echo $row;
                }
            }

            ?>

        </tbody>
        <tfoot>
            <tr>
                <td colspan="7">Total</td>
                <td style="text-align: right"><?=\app\helpers\MyHelper::formatRupiah($total);?></td>
                <td></td>
            </tr>
        </tfoot>
    </table>


<?php
$script = "

function popitup(url,label,pos) {
    var w = screen.width * 0.8;
    var h = 800;
    var left = pos == 1 ? screen.width - w : 0;
    var top = pos == 1 ? screen.height - h : 0;
    
    window.open(url,label,'height='+h+',width='+w+',top='+top+',left='+left);
    
}


function resetNonracik(){
    $('#nama_barang').val('');
    $('#signa1_nonracik').val(0);
    $('#signa2_nonracik').val(0);
    $('#jumlah_hari_nonracik').val(0);
    $('#qty_nonracik').val(0);
    $('#jumlah_ke_apotik_nonracik').val(0);
}


$(document).on('keydown','.calc_kekuatan_modal', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
        
        var signa1 = isNaN($('#signa1_update_form').val()) ? 0 : $('#signa1_update_form').val();
        var signa2 = isNaN($('#signa2_update_form').val()) ? 0 : $('#signa2_update_form').val();
        var jumlah_hari = isNaN($('#jumlah_hari_update_form').val()) ? 0 : $('#jumlah_hari_update_form').val();
        if($('#stok_update_form').val() == '' || $('#stok_update_form').val() == '0')
            $('#stok_update_form').val(signa1 * signa2 * jumlah_hari);
        var kekuatan = isNaN($('#kekuatan_update_form').val()) ? 0 : $('#kekuatan_update_form').val();
        var dosis_minta = isNaN($('#dosis_minta_update_form').val()) ? 0 : $('#dosis_minta_update_form').val();
        var jml_racikan = isNaN($('#stok_update_form').val()) ? 0 : $('#stok_update_form').val();

        var hasil = eval(jml_racikan) * eval(dosis_minta) / eval(kekuatan);
        hasil = isNaN(hasil) ? 0 : hasil;
        $('#qty_update_form').val(hasil);
        $('#jumlah_ke_stok_update_form').val(Math.ceil(hasil));
        $('#jumlah_ke_apotik_update_form').val(Math.ceil(hasil));


    }

    
});

$(document).on('keydown','.calc_kekuatan', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();

        var signa1 = isNaN($('#signa1').val()) ? 0 : $('#signa1').val();
        var signa2 = isNaN($('#signa2').val()) ? 0 : $('#signa2').val();
        var jumlah_hari = isNaN($('#jumlah_hari').val()) ? 0 : $('#jumlah_hari').val();
        if($('#stok').val() == '' || $('#stok').val() == '0')
            $('#stok').val(signa1 * signa2 * jumlah_hari);

        var kekuatan = isNaN($('#kekuatan').val()) ? 0 : $('#kekuatan').val();
        var dosis_minta = isNaN($('#dosis_minta').val()) ? 0 : $('#dosis_minta').val();
        var jml_racikan = isNaN($('#stok').val()) ? 0 : $('#stok').val();

        var hasil = eval(jml_racikan) * eval(dosis_minta) / eval(kekuatan);
        hasil = isNaN(hasil) ? 0 : hasil;
        $('#qty').val(hasil);
        $('#jumlah_ke_stok').val(Math.ceil(hasil));
        $('#jumlah_ke_apotik').val(Math.ceil(hasil));
    }

    
});

$(document).on('keydown','.calc_qtynon', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
        

        var signa1 = $('#signa1_nonracik').val();
        var signa2 = $('#signa2_nonracik').val();
        var jmlhari = $('#jumlah_hari_nonracik').val();

        signa1 = isNaN(signa1) ? 0 : signa1;
        signa2 = isNaN(signa2) ? 0 : signa2;
        jmlhari = isNaN(jmlhari) ? 0 : jmlhari;
        var qty = eval(signa1) * eval(signa2) * eval(jmlhari);

        $('#qty_nonracik').val(qty);
        $('#jumlah_ke_apotik_nonracik').val(qty);

    }

    
});


$(document).on('keydown','.calc_qtynon_update', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
        

        var signa1 = $('#signa1_nonracik_update').val();
        var signa2 = $('#signa2_nonracik_update').val();
        var jmlhari = $('#jumlah_hari_nonracik_update').val();

        signa1 = isNaN(signa1) ? 0 : signa1;
        signa2 = isNaN(signa2) ? 0 : signa2;
        jmlhari = isNaN(jmlhari) ? 0 : jmlhari;
        var qty = eval(signa1) * eval(signa2) * eval(jmlhari);

        $('#qty_nonracik_update').val(qty);
        $('#jumlah_ke_apotik_nonracik_update').val(qty);

    }

    
});

function cekResep(customer_id, tgl){
    $.ajax({
        url : '".\yii\helpers\Url::toRoute(['penjualan/ajax-cek-resep'])."',
        type : 'POST',
        data : 'customer_id='+customer_id+'&tgl='+tgl,
        beforeSend : function(){
            $('#loading').show();
            $('#warning-msg').html('');
            $('#warning-msg').hide();
        },
        error : function(e){
            $('#loading').hide();
            console.log(e.responseText);
            $('#warning-msg').html('');
            $('#warning-msg').hide();
        },
        success : function(hasil){
            var data = $.parseJSON(hasil);
            $('#loading').hide();
            
            if(data.is_exist){
                $('#warning-msg').html('<i class=\"fa fa-exclamation-triangle\"></i> Sudah ada resep pada tanggal yg sama');
                $('#warning-msg').show();
            }

        }
    });
}


$(document).on('keydown','.jq-dte-day, .jq-dte-month',function(e){
   
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13){
        e.preventDefault();

        cekResep($('#customer_id').val(),$('#tanggal').val());
    }
});


$(document).on('keydown','#customer_id',function(e){
   
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13){
         e.preventDefault();

        var jenis_rawat = $('#jenis_rawat').val();
        var urlPx = jenis_rawat == 1 ? '".\yii\helpers\Url::toRoute(['api/ajax-pasien'])."' : '".\yii\helpers\Url::toRoute(['api/ajax-pasien-daftar'])."';
        $.ajax({
            url : urlPx,
            type : 'GET',
            data : 'term='+$(this).val()+'&jenis_rawat='+$('#jenis_rawat').val(),
            beforeSend : function(){
                $('#loading').show();
            },
            error : function(){
                $('#loading').hide();
            },
            success : function(hasil){
                var data = $.parseJSON(hasil)[0];
                $('#loading').hide();
                 if(data.id != 0){

                    $('#pasien_id').val(data.id);
                    cekResep(data.id,$('#tanggal').val());
                    $('#pasien_nama').val(data.namapx);
                    loadItemHistory(data.id);
                    $('#jenis_pasien').val(data.namagol);
                    $('#jenis_resep_nama').val(data.namagol);
                    $('#jenis_resep_id').val(data.jenispx);
                    $('#unit_pasien').val(data.namaunit);
                    $('#unit_id').val(data.kodeunit);
                    $('#kode_daftar').val(data.nodaftar);
                    $('#id_rawat_inap').val(data.id_rawat_inap);
                    $('#tgldaftar').datetextentry('set_date',data.tgldaftar); 
                    $('#dokter_id').val(data.id_dokter);
                    $('#dokter_nama').val(data.nama_dokter);

                }
            }
        });
    }
});

$(document).on('click','a.cart-update', function(e) {

    var id = $(this).attr('data-item');
   
    $.ajax({
        type : 'POST',
        url : '".\yii\helpers\Url::toRoute(['cart/ajax-get-item'])."',
        data : {dataItem:id},
        beforeSend: function(){

        },
        success : function(data){
            var hsl = jQuery.parseJSON(data);

            if(hsl.code == '200'){
                if(hsl.is_racikan == 1){
                    $('#modal-update-racik-cart').trigger('click');
                    $('#cart_id').val(hsl.id);
                    
                    $('#kode_racikan_update_form').val(hsl.kode_racikan);
                    $('#dept_stok_id_update_form').val(hsl.departemen_stok_id);
                    $('#nama_barang_item_update_form').val(hsl.nama_barang);
                    $('#signa1_update_form').val(hsl.signa1);
                    $('#signa2_update_form').val(hsl.signa2);
                    $('#qty_update_form').val(hsl.qty);
                    $('#kekuatan_update_form').val(hsl.kekuatan);
                    $('#dosis_minta_update_form').val(hsl.dosis_minta);
                    $('#barang_id_update_form').val(hsl.barang_id);
                    $('#harga_jual_update_form').val(hsl.harga_jual);
                    $('#harga_beli_update_form').val(hsl.harga_beli);
                    $('#jumlah_ke_apotik_update_form').val(hsl.jumlah_ke_apotik);
                    $('#jumlah_ke_stok_update_form').val(Math.ceil(hsl.qty));
                    $('#jumlah_hari_update_form').val(hsl.jumlah_hari);
                    $('#departemen_stok_id_update').val(hsl.departemen_stok_id);
                    var kekuatan = hsl.kekuatan;
                    var dosis_minta = hsl.dosis_minta;
                    var qty = hsl.qty;
                    var jumlah_minta = qty * kekuatan / dosis_minta;
                    $('#stok_update_form').val(Math.round(jumlah_minta));
                }

                else{
                    $('#modal-update-cart').trigger('click');
                    $('#cart_id').val(hsl.id);

                    $('#nama_barang_update').val(hsl.nama_barang);
                    $('#signa1_nonracik_update').val(hsl.signa1);
                    $('#signa2_nonracik_update').val(hsl.signa2);
                    $('#qty_nonracik_update').val(hsl.qty);
                    $('#departemen_stok_id_update').val(hsl.departemen_stok_id);
                    $('#harga_jual_nonracik_update').val(hsl.harga_jual);
                    $('#harga_beli_nonracik_update').val(hsl.harga_beli);
                    $('#jumlah_ke_apotik_nonracik_update').val(hsl.jumlah_ke_apotik);
                    $('#jumlah_hari_nonracik_update').val(hsl.jumlah_hari);
                }
            }

            else{
                alert(hsl.message);
            } 
        }
    });
    
});

$(document).on('click','a.cart-delete', function(e) {

    var id = $(this).attr('data-item');
    var conf = confirm('Hapus item ini ? ');
    if(conf){
        $.ajax({
            type : 'POST',
            url : '".\yii\helpers\Url::toRoute(['cart/ajax-update-delete'])."',
            data : {dataItem:id},
            beforeSend: function(){

            },
            success : function(data){

                location.reload();
            }
        });
    }
});


$(document).ready(function(){


    $('.duplicate_next').keydown(function(e){
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key == 13){
            e.preventDefault();
            var qty = $(this).val();
            qty = isNaN(qty) ? 0 : qty;
            $(this).next().val(Math.ceil(qty));
    
        }
    });


    $('input:text').focus(function(){
        $(this).css({'background-color' : '#A9F5E1'});
    });

    $('input:text').blur(function(){
        $(this).css({'background-color' : '#FFFFFF'});
    });

    $('#btn-resep-baru').click(function(){
        
        var conf = confirm('Buat Resep Baru?');

        if(conf){

            $('#signa1').focus();

            $.ajax({
              type : 'post',
              url : '".\yii\helpers\Url::toRoute(['cart/ajax-generate-code'])."',
              success : function(res){
                
                var res = $.parseJSON(res);
                
                $('#kode_transaksi').val(res);

                
              },
            });
        }
    });

    $(this).keydown(function(e){
        var key = e.keyCode;
        switch(key){
            case 112: //F1
                e.preventDefault();
                $('#btn-resep-baru').trigger('click');
            break;

            case 113: //F2
                e.preventDefault();
                $('#btn-simpan-item').trigger('click');
            break;
            case 114: // F3
                e.preventDefault();
                $('#click-racikan').trigger('click');
            break;
            case 115: // F4
                e.preventDefault();
                $('#click-nonracikan').trigger('click');
                $('#nama_barang').focus();
            break;

            case 117: // F6
                e.preventDefault();
                $('#btn-obat-baru').trigger('click');                
            break;

            case 119: // F8
                e.preventDefault();
                $('#signa1').focus();
                $('#nama_barang').focus();
            break;
            case 120: // F9
                e.preventDefault();
                $('#generate_kode').trigger('click');
            break;
            case 121: // F10
                e.preventDefault();
                $('#btn-bayar').trigger('click');
            break;
        }
        
    });


    $('#btn-input-update').click(function(e){
        e.preventDefault();
        var departemen_stok_id = $('#departemen_stok_id_update').val();
        var qty = $('#qty_nonracik_update').val();

        if(departemen_stok_id == ''){
            alert('Data Obat tidak boleh kosong');
            return;
        }

        if(qty == ''){
            alert('Jumlah / Qty tidak boleh kosong');
            return;
        }

        obj = new Object;
        obj.cart_id = $('#cart_id').val();
        obj.departemen_stok_id = departemen_stok_id;
        obj.qty = qty;
        obj.kode_transaksi = $('#kode_transaksi').val();
        obj.harga = $('#harga_jual_nonracik_update').val();
        obj.harga_beli = $('#harga_beli_nonracik_update').val();
        obj.subtotal = eval(obj.harga) * eval(obj.qty);
        obj.jumlah_ke_apotik = $('#jumlah_ke_apotik_nonracik_update').val();
        obj.signa1 = $('#signa1_nonracik_update').val();
        obj.signa2 = $('#signa2_nonracik_update').val();
        obj.jumlah_hari = $('#jumlah_hari_nonracik_update').val();

        $.ajax({
            type : 'POST',
            data : {dataItem:obj},
            url : '".\yii\helpers\Url::toRoute(['cart/ajax-simpan-item-update'])."',

            success : function(data){
                
                location.reload();
            }
        });
    });

    $('#btn-simpan-item-update').on('click',function(){

        var kekuatan = $('#kekuatan_update_form').val();
        var dosis_minta = $('#dosis_minta_update_form').val();
        var jml_racikan = $('#stok_update_form').val();
        var hasil = Math.ceil(eval(jml_racikan) * eval(dosis_minta) / eval(kekuatan));
        var harga_jual = $('#harga_jual_update_form').val();
        var harga_beli = $('#harga_beli_update_form').val();
       

        item = new Object;
        item.cart_id = $('#cart_id').val();
        item.barang_id = $('#barang_id_update_form').val();
        item.kekuatan = kekuatan;
        item.departemen_stok_id = $('#dept_stok_id_update_form').val();
        item.dosis_minta = dosis_minta;
        item.kode_transaksi = $('#kode_transaksi').val();
        item.kode_racikan = $('#kode_racikan_update_form').val();
        item.is_racikan = 1;
        item.qty = $('#qty_update_form').val();
        item.qty_bulat = Math.ceil(item.qty);
        
        if(eval(item.qty) < 1)
            item.subtotal_bulat = item.qty * Math.round(harga_jual);
        else
            item.subtotal_bulat = item.qty_bulat * Math.round(harga_jual);

        item.subtotal = item.qty * harga_jual;
        item.signa1 = $('#signa1_update_form').val();
        item.signa2 = $('#signa2_update_form').val();
        item.jumlah_ke_apotik = $('#jumlah_ke_apotik_update_form').val();
        item.harga = harga_jual;
        item.harga_beli = harga_beli;
        
        // $('#qty_update_form').val(hasil);
        $.ajax({
            type : 'POST',
            url : '".\yii\helpers\Url::toRoute(['cart/ajax-simpan-item-update'])."',
            data : {dataItem:item},
            beforeSend: function(){

                // $('#alert-message').hide();
            },
            success : function(data){

                var hsl = jQuery.parseJSON(data);

                if(hsl.code == '200'){

                    refreshTable(hsl);
                    $('#nama_barang_item').val('');
                    $('#nama_barang_item').focus();
                    $('#kekuatan').val(0);
                    $('#dosis_minta').val(0);
                    $('#qty').val(0);
                    $('#jumlah_ke_apotik').val(0);
                   
                }

                else{
                    alert(hsl.message);
                } 
            }
        });
    });

  
});



$(document).on('keydown','input', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
        var inputs = $(this).closest('#listItems').find(':input:visible');
              
        inputs.eq( inputs.index(this)+ 1 ).focus().select();
        $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        }, 10);


    }
});
";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>


<?php 

\yii\bootstrap\Modal::begin([
    'header' => '<h2>Update Cart Non-Racikan</h2>',
    'toggleButton' => ['label' => '','id'=>'modal-update-cart','style'=>'display:none'],
    
]);

?>

<?= $this->render('_non_racikan_update', [
        'model' => $model,
        'jenis_rawat' => $jenis_rawat
    ]) ?>
<?php

\yii\bootstrap\Modal::end();
?>


<?php 

\yii\bootstrap\Modal::begin([
    'header' => '<h2>Update Cart Racikan</h2>',
    'size' => 'modal-lg',
    'toggleButton' => ['label' => '','id'=>'modal-update-racik-cart','style'=>'display:none'],
    
]);
?>
<?= $this->render('_racikan_update', [
        'model' => $model,
        'jenis_rawat' => $jenis_rawat
    ]) ?>
<?php

\yii\bootstrap\Modal::end();
?>
</div>