<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\web\JsExpression;
use keygenqt\autocompleteAjax\AutocompleteAjax;
use yii\jui\AutoComplete;

use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Penjualan */

$this->title = 'Update Penjualan';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="penjualan-form">
<h3>Data Penjualan <?=$rawat[$jenis_rawat];?></h3>
<div class="row">
    <div class="col-sm-4">
         <?php $form = ActiveForm::begin(); ?>
    <div class="form-group col-xs-12 col-lg-12">
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name"> No Resep </div>

                <div class="profile-info-value ">
<span class="editable" id="kode_penjualan"><?=$penjualan->kode_penjualan;?></span>
                    <?= $form->field($penjualan, 'kode_penjualan')->hiddenInput()->label(false); ?>
                    
                    
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Nama Px </div>

                <div class="profile-info-value">
                    <span class="editable" id="nama_px"><?=$penjualanResep->pasien_nama ?: '';?> <?=$penjualanResep->pasien_nama ? '/' : '';?> <?=$penjualanResep->pasien_id ?: '';?></span>
                </div>
            </div>
         
            <div class="profile-info-row">
                <div class="profile-info-name"> Tgl Resep </div>

                <div class="profile-info-value">
                    <span class="editable" id="tgl_resep"><?=$penjualan->tanggal ? date('d-m-Y',strtotime($penjualan->tanggal)): '';?></span>
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Jns Resep </div>

                <div class="profile-info-value">
                    <span class="editable" id="login"><?=$penjualanResep->pasien_jenis ?: '';?></span>
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Dokter </div>

                <div class="profile-info-value">
                    <span class="editable" id="about"><?=$penjualanResep->dokter_nama ?: '';?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Unit </div>

                <div class="profile-info-value">
                    <span class="editable" id="about"><?=$penjualanResep->unit_nama ?: '';?></span>
                </div>
            </div>

            
        </div>

      
     <?php ActiveForm::end(); ?>
   
</div>

</div>
    <div class="col-sm-12">
   <?php $form = ActiveForm::begin([
         'action' => ['retur-barang-customer/simpan-retur'],
        'method' => 'POST',
   ]); ?>        
    <?= $form->errorSummary($model) ?>
       <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
         
    </div>
<?php endif; ?>
 <?php if (Yii::$app->session->hasFlash('danger')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('danger') ?>
         
    </div>
<?php endif; ?>
<?= $form->field($model, 'penjualan_id')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'customer_id')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'departemen_id')->hiddenInput()->label(false); ?>

 <div class="form-group col-xs-12 col-lg-12">
        <div class="profile-user-info profile-user-info-striped">


            <div class="profile-info-row">
                <div class="profile-info-name"> Tanggal Retur </div>

                <div class="profile-info-value">
                    
                     <input name="tanggal"  type="text" id="tanggal" value="<?=date('Y-m-d');?>"/>
                </div>
            </div>
        </div>

      
   
</div>
<table class="table table-striped table-bordered" id="table-item">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama</th>
                <th style="text-align: center;">Signa 1</th>
                <th style="text-align: center;">Signa 2</th>
                <th style="text-align: center;">Harga</th>
                <th style="text-align: center;">Qty</th>
                
                <th style="text-align: center;">Subtotal</th>
                <th style="text-align: center;" width="15%">Jumlah Retur</th>                
            </tr>
        </thead>
        <tbody>
            <?php

            $ii = 0;
            $jj = 0; 
            $no_racik = 0;
            $no_nonracik = 0;
            $total = 0;
            foreach($penjualan->penjualanItems as $q => $item)
            {   
                
                if($item->is_racikan)
                {
                    $no_racik++;
                    if($ii == 0){
                echo '<tr><td colspan="9" style="text-align:left">Racikan</td></tr>';
                
                    }
                    $qty = $item->qty < 1 ? $item->qty : ceil($item->qty);
                    $subtotal_bulat = round($item->harga) * $qty;
                    $ii++;

                    ?>
                     <tr>
                <td><?=($no_racik);?></td>
                <td><?=$item->stok->barang->kode_barang;?></td>
                <td><?=$item->stok->barang->nama_barang;?></td>
                <td><?=$item->signa1;?></td>
                <td><?=$item->signa2;?></td>
                <td style="text-align: right"><?=\app\helpers\MyHelper::formatRupiah(round($item->harga));?></td>
                <td style="text-align: center;"><?=ceil($item->qty);?></td>
                <td style="text-align: right"><?=\app\helpers\MyHelper::formatRupiah($subtotal_bulat);?></td>
                <td>
            <input type="text" name="jumlah[]"/>
                </td>
            </tr>
                    <?php
                    $total += $subtotal_bulat;
                }

                else{
                    if($jj == 0){
                    
                    echo '<tr><td colspan="9" style="text-align:left">Non-Racikan</td></tr>';
                    
                    }
                    $qty = $item->qty < 1 ? $item->qty : ceil($item->qty);
                    $no_nonracik++;
                    $subtotal_bulat = round($item->harga) * $qty;
                    $jj++;

                    
                    ?>
                     <tr>
                <td><?=($no_nonracik);?></td>
                <td><?=$item->stok->barang->kode_barang;?></td>
                <td><?=$item->stok->barang->nama_barang;?></td>
                <td><?=$item->signa1;?></td>
                <td><?=$item->signa2;?></td>
                <td style="text-align: right"><?=\app\helpers\MyHelper::formatRupiah(round($item->harga));?></td>
                <td style="text-align: center;"><?=$qty;?></td>
                <td style="text-align: right"><?=\app\helpers\MyHelper::formatRupiah($subtotal_bulat);?></td>
                <td>
                    <input type="hidden" name="item_id_<?=$item->id;?>" value="<?=$item->id;?>"/>
                    <input type="text" name="jumlah_<?=$item->id;?>"/>
                </td>
            </tr>
                    <?php
                    $total += $subtotal_bulat;
                }
            ?>

           
            <?php 
            }

            ?>

            <tr>
                <td colspan="7" style="text-align:right"><strong>Total Biaya</strong></td>
                <td style="text-align:right"><strong><?=\app\helpers\MyHelper::formatRupiah($total);?></strong></td>
                <td></td>
                
            </tr>
        </tbody>
    </table>
    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
      <?php ActiveForm::end(); ?>   
    </div><!-- /.col -->


</div>

<?php
$script = "

$(document).ready(function(){

    $('#tanggal').datetextentry();
});

$(document).on('keydown','input', function(e) {

    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    
    if(key == 13) {
        e.preventDefault();
        var inputs = $(this).closest('.penjualan-form').find(':input:visible');
              
        inputs.eq( inputs.index(this)+ 1 ).focus().select();
        $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        }, 10);


    }
});


$(document).on('keydown','.jq-dte-day, .jq-dte-month',function(e){
   
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13){
        e.preventDefault();

        cekResep($('#customer_id').val(),$('#tanggal').val());
    }
});

";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);
// $this->registerJs($script);
?>