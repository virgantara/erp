<?php 
use yii\helpers\Url;
?>
<table class="table table-striped" id="table_billing">
    <thead>
        <tr>
            <th>No</th>
            <th>Trx Code</th>
            <th>No Reg.</th>
            <th>Cust. Name</th>
            <th>Cust. Type</th>
            <th>Issued By</th>
            
            <th>Issued Date</th>

            <th>Status</th>
            <th>Option</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $row = '';
        foreach($result as $i => $obj)
        {
            $obj = (object) $obj;
         $row .= '<tr>';
        $row .= '<td>'.($i+1).'</td>';
        $row .= '<td>'.$obj->kode_trx.'</td>';
        $row .= '<td>'.$obj->custid.'</td>';
        
        $row .= '<td>'.$obj->nama.'</td>';
        $row .= '<td>'.$obj->jenis_customer.'</td>';
        
        $row .= '<td>'.$obj->issued_by.'</td>';
        $row .= '<td>'.$obj->trxdate.'</td>';
        switch ($obj->status_bayar) {
            case 1:
                $label = 'SUDAH BAYAR';
                $st = 'success';
                break;
            case 2:
                $label = 'BON';
                $st = 'warning';
                break;
            default:
                $label = 'BELUM BAYAR';
                $st = 'danger';
                
        }
        
        $state =  '<button type="button" class="btn btn-'.$st.' btn-sm" ><span>'.$label.'</span></button>';

        $row .= '<td>'.$state.'</td>';
        $row .= '<td><a target="_parent" href="'.Url::toRoute(['default/view']).'?id='.$obj->id.'"><button class="btn btn-info btn-sm "><span class="glyphicon glyphicon-eye-open"></span>&nbsp;Lihat Detil</button></a></td>';
        $row .= '</tr>';
        }

        echo $row;
        ?>
    </tbody>
</table>