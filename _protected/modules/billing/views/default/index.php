<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Tagihan Pasien';
?>
<h1><?= Html::encode($this->title) ?></h1>
<div class="billing-default-index">
    <div class="row">
        <div class="col-xs-12">
            <form class='form-horizontal'>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> No RM atau Trx Code:</label>
                    <div class="col-lg-2 col-sm-10">
                        <select id="by_search">
                            <option value="1">No RM</option>
                            <option value="2">Kode Trx</option>
                        </select>
                        <input type="text" name="search" id="search"/>    
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1"></label>
                    <div class="col-lg-2 col-sm-10">
                     <?= Html::button(' <i class="ace-icon fa fa-check bigger-110"></i>Cari', ['class' => 'btn btn-info','id'=>'btn-search','value'=>1]) ?>    
                        
                    </div>
                </div>
               
            </form>
            <iframe width="100%" height="500px" name="list_items" id="list_items" frameBorder="0"></iframe>
 
        </div>
    </div>
</div>


<?php
$script = "

function loadTagihan(search,by){
    var url = '".Url::toRoute(['default/list-tagihan'])."?search='+search+'&by='+by;
    $('#list_items').attr('src',url);
}

$(document).ready(function(){

   

    $('#btn-search').click(function(){

        let search = $('#search').val();
        let by = $('#by_search').val();

        loadTagihan(search,by);
    });

    $('#search').keydown(function(e){
        let key = e.keyCode;

        if(key == 13){
            e.preventDefault();
            let search = $(this).val();
            let by = $('#by_search').val();
            loadTagihan(search,by);    
        }
        
    });
    loadTagihan('','');
});


";
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);


?>