<?php

namespace app\modules\billing;

use app\helpers\MyHelper;
use yii\httpclient\Client;
use yii\helpers\Json;

/**
 * billing module definition class
 */
class Module extends \yii\base\Module
{

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\billing\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function updateTagihanObat($params)
    {
        $result = [];
        try {
            $api_baseurl = \Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);

            $response = $client->post('/obat/tagihan/update', $params)->send();

            if ($response->isOk) {
                $result = $response->data['values'];   
            }
        }
        catch(\Exception $e)
        {
            $result = 'Error: '.$e->getMessage();
        }

        return $result;
    }

    public function getPasien($custid)
    {
        $api_baseurl = \Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);

        $response = $client->get('/pasien/rm', ['key' => $custid])->send();
        
        $out = [];


        
        if ($response->isOk) {
            $result = $response->data['values'];

            if(!empty($result))
            {
                foreach ($result as $d) {
                    $out = [
                        'id' => $d['NoMedrec'],
                        'label'=> $d['NAMA'],
                        'alamat' => $d['ALAMAT'],  
                    ];
                }
            }
        }

        return $out;
    }

    public function updateTagihan($params)
    {
        $result = [];
        try {
            $api_baseurl = \Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            
            $headers = [
                'origin' => $params['origin'],
                'destination' => $params['destination']
            ];
            

             
            $response = $client->post('/tagihan/update', $params,$headers)->send();
            
            if ($response->isOk) {
                $result = $response->data['values'];   
                // $headers = [
                //     'client_id' => $params['origin'],
                // ];
                
                // $response = $client->post('/tagihan/receiveClientMsg', $params,$headers)->send();

                
                // if ($response->isOk) {
                //     $result = $response->data['values'];   
                // }
            }
        }
        catch(\Exception $e)
        {
            $result = 'Error: '.$e->getMessage();
            exit;
        }

        // $result = [];
        // try {
            
        // }

        // catch(\Exception $e)
        // {
        //     $result = 'Error: '.$e->getMessage();
        //     exit;
        // }

        return $result;
    }

    public function updateTagihanFromkasir($params)
    {
        $result = [];
        try {
            $api_baseurl = \Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            
            $headers = [
                'origin' => $params['origin'],
                'destination' => $params['destination']
            ];

            $response = $client->post('/tagihan/update/fromkasir', $params,$headers)->send();
            
            if ($response->isOk) {
                $result = $response->data['values'];   
              
            }
        }
        catch(\Exception $e)
        {
            $result = 'Error: '.$e->getMessage();
            exit;
        }

      
        return $result;
    }

    public function insertTagihan($params)
    {
        $result = [];
        try {
            $api_baseurl = \Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $params['origin'] = 'integra';
            $response = $client->post('/tagihan/insert', $params)->send();

            
            
            if ($response->isOk) {
                $result = $response->data['values'];   
            }
        }
        catch(\Exception $e)
        {
            $result = 'Error: '.$e->getMessage();
        }

        return $result;
    }


    public function getTagihan($id)
    {
        $result = [];
        try {
            $api_baseurl = \Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);

            $response = $client->get('/tagihan/get', ['id'=>$id])->send();
            

            if ($response->isOk) {
                $result = $response->data['values'];   
            }
        }
        
        catch(\Exception $e)
        {
            $result = 'Error: '.$e->getMessage();
        }

        return $result;
    }

    public function getTagihanByTrx($trx)
    {
        $result = [];
        try {
            $api_baseurl = \Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);

            $response = $client->get('/tagihan/get/trx', ['kode_trx'=>$trx])->send();
            

            if ($response->isOk) {
                $result = $response->data['values'];   
            }
        }
        
        catch(\Exception $e)
        {
            $result = 'Error: '.$e->getMessage();
        }

        return $result;
    }

    
    public function listTagihan($search, $by, $limit, $page)
    {
        $result = [];
        try {
            $api_baseurl = \Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);

            $params = [
                'limit' => $limit,
                'page' => $page,
                'search' => $search,
                'by' => $by
            ];

            // print_r($params);exit;
            $response = $client->post('/tagihan/list', $params)->send();
             
            if ($response->isOk) {
                $result = $response->data['values'];   
            }

           
        }

        catch(\Exception $e)
        {
            $result = 'Error: '.$e->getMessage();
        }

        return $result;
    }

}
