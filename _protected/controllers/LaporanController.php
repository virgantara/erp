<?php

namespace app\controllers;

use Yii;
use app\models\Cart;
use app\models\PenjualanItem;
use app\models\PenjualanItemSearch;
use app\models\Penjualan;
use app\models\PenjualanSearch;
use app\models\SalesFaktur;
use app\models\SalesFakturSearch;
use app\models\RequestOrder;
use app\models\RequestOrderSearch;
use app\models\Pasien;
use app\models\SalesMasterBarang;
use app\models\MasterJenisBarang;
use app\models\SalesStokGudang;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\MyHelper;
use yii\httpclient\Client;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * PenjualanController implements the CRUD actions for Penjualan model.
 */
class LaporanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionKeluarMasukBarang()
    {

        $results = [];

        if(!empty($_GET['tanggal_awal']) && !empty($_GET['tanggal_akhir']))
        {
            // $listJenisBarang = \app\models\MasterJenisBarang::find()->where(['perusahaan_id'=>Yii::$app->user->identity->perusahaan_id])->orderBy(['nama'=>SORT_ASC]);
            $sd = date('Y-m-d',strtotime($_GET['tanggal_awal']));
            $ed = date('Y-m-d',strtotime($_GET['tanggal_akhir']));
            
            $is_separated = empty($_GET['export']) ?: 0;
            
            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $params = [
                'departemen_id' => Yii::$app->user->identity->departemen,
                'sd' => $sd,
                'ed' => $ed,
                'barang_id' => !empty($_GET['barang_id']) ? $_GET['barang_id'] : '',
            ];
            // $out = [];
            $response = $client->get('/integra/stok/io', $params)->send();
            $results = [];
            if ($response->isOk) 
            {
                $results = $response->data['values'];
            

            // foreach($listJenisBarang->all() as $jb)
            // {
                // $tmp = [];
                // $listBarang = SalesMasterBarang::find();
                // $listBarang->where([
                //     'jenis_barang_id'=>$jb->id,
                //     'is_hapus'=>0
                // ]);
                // $listBarang->orderBy(['nama_barang'=>SORT_ASC]);
            //     foreach($results as $hasil)
            //     {

            //         $query = \app\models\StokHistory::find();
            //         $query->select(['qty_in','qty_out','']);
            //         $query->andFilterWhere(['between', 'tanggal', $sd, $ed]);
            //         $query->where([
            //             'barang_id'=>$barang->id_barang,
            //             'departemen_id' => Yii::$app->user->identity->departemen
            //         ]);
            //         // $query->andWhere([\app\models\BarangOpname::tableName().'.tahun'=>$tahun.$bulan]);
            //         $query->andWhere(['between','tanggal',$sd,$ed]);
            //         // $query->joinWith(['departemenStok as ds']);
                    
            //         $stok_keluar = $query->sum('qty_out');
            //         $stok_masuk = $query->sum('qty_in');
                    
            //         $tmp[$barang->id_barang] = [
            //             'kode' => $barang->kode_barang,
            //             'nama' => $barang->nama_barang,
            //             'satuan' => $barang->id_satuan,
            //             'stok_masuk' => $stok_masuk,
            //             'stok_keluar' => $stok_keluar,
            //             // 'hb' => \app\helpers\MyHelper::formatRupiah($barang->harga_beli,2,$is_separated),
            //             // // 'hj' => \app\helpers\MyHelper::formatRupiah($m->barang->harga_jual,2),
            //             // 'subtotal' => \app\helpers\MyHelper::formatRupiah($subtotal,2,$is_separated),
            //         ];
            //     }

            //     // $results[$jb->id] = [
            //     //     'id' => $jb->id,
            //     //     'nama' => $jb->nama,
            //     //     'list' => $tmp
            //     // ];
            // }
            
           
            }

            if(!empty($_GET['export']) && empty($_GET['search']))
            {
                return $this->renderPartial('_tabel_keluar_masuk_barang', [
                    'results' => $results,
                    'model' => $model,
                    'export' => 1
                ]); 
            }

        }

        return $this->render('keluar_masuk_barang', [
            'results' => $results,
            'model' => $model,


        ]);
    }

    public function actionOpname()
    {

        $list = [];
        $tahunbulan = '';
        if(!empty($_POST['tanggal']) && !empty($_POST['dept_id']))
        {


            $tanggal = date('d',strtotime($_POST['tanggal']));
            $bulan = date('m',strtotime($_POST['tanggal']));
            $tahun = date('Y',strtotime($_POST['tanggal']));
            $query = \app\models\BarangOpname::find();
            $query->where(['<>','barang.nama_barang','-']);
            $query->andWhere(['ds.departemen_id'=>$_POST['dept_id']]);
            $query->andWhere(['barang.is_hapus'=>0]);
            if(!empty($_POST['jenis_barang_id']))
                $query->andWhere(['barang.jenis_barang_id'=>$_POST['jenis_barang_id']]);
            $query->andWhere([\app\models\BarangOpname::tableName().'.tahun'=>$tahun.$bulan]);
            $query->joinWith(['barang as barang','departemenStok as ds']);
            $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $list = $query->all();

            $date = !empty($_POST['tanggal']) ? $_POST['tanggal'] : date('Y-m-d');
            $dept_id = !empty($_POST['dept_id']) ? $_POST['dept_id'] : 0;
            $tanggal = date('d',strtotime($date));
            $bulan = date('m',strtotime($date));
            $tahun = date('Y',strtotime($date));
            $tahunbulan = $tahun.$bulan;
            
            if(!empty($_POST['export']) && empty($_POST['search']))
            {
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                
                
                // Add column headers
                $sheet->setCellValue('A1', 'No')
                    ->setCellValue('B1', 'Kode Barang')
                    ->setCellValue('C1', 'Nama Barang')
                    ->setCellValue('D1', 'Harga')
                    ->setCellValue('E1', 'Jumlah')
                    ->setCellValue('F1', 'Selisih')
                    ->setCellValue('G1', 'Total')
                    ->setCellValue('H1', 'Keterangan');

                
                //Put each record in a new cell

                $sheet->getColumnDimension('A')->setWidth(5);
                $sheet->getColumnDimension('B')->setWidth(15);
                $sheet->getColumnDimension('C')->setWidth(35);
                $sheet->getColumnDimension('D')->setWidth(20);
                $sheet->getColumnDimension('E')->setWidth(10);
                $sheet->getColumnDimension('F')->setWidth(10);
                $sheet->getColumnDimension('G')->setWidth(30);
                $sheet->getColumnDimension('H')->setWidth(20);
                $i= 0;
                $ii = 2;

                $total = 0;
                $index = 0;

                foreach($list as $key => $m)
                {
                    $index++;
                    $subtotal = $m->stok * $m->barang->harga_beli;
                    $total += $subtotal;
                    
                    $sheet->setCellValue('A'.$ii, $index);
                    $sheet->setCellValue('B'.$ii, $m->barang->kode_barang);
                    $sheet->setCellValue('C'.$ii, $m->barang->nama_barang);
                    $sheet->setCellValue('D'.$ii, $m->barang->harga_beli);
                    $sheet->setCellValue('E'.$ii, $m->stok);
                    $sheet->setCellValue('F'.$ii, $m->selisih);
                    $sheet->setCellValue('G'.$ii, $subtotal);
                    $sheet->setCellValue('H'.$ii, '-');
                    
                    $ii++;
                }       

                $sheet->setCellValue('A'.$ii, '');
                $sheet->setCellValue('B'.$ii, '');
                $sheet->setCellValue('C'.$ii, '');
                $sheet->setCellValue('D'.$ii, '');
                $sheet->setCellValue('E'.$ii, '');
                $sheet->setCellValue('F'.$ii, 'Total');
                $sheet->setCellValue('G'.$ii, $total);
                $sheet->setCellValue('H'.$ii, '');

                // Set worksheet title
                $sheet->setTitle('Laporan Opname');
                
                ob_end_clean();
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="laporan_opname.xlsx"');
                header('Cache-Control: max-age=0');
                
                $writer = new Xlsx($spreadsheet);
                $writer->save('php://output');
                exit;
            }

        }

        return $this->render('opname', [
            'list' => $list,
            'model' => $model,
            'tahunbulan' => $tahunbulan
        ]);
    }

    public function actionStokBulanan()
    {
        $bulans = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        ];
        $results = [];

        if(!empty($_POST['bulan']) && !empty($_POST['dept_id']))
        {
            

            $total = 0;
            
            $sd = date('Y-m-01',strtotime(date('Y').'-'.$_POST['bulan'].'-01')).' 00:00:00';
            $ed = date('Y-m-t',strtotime(date('Y').'-'.$_POST['bulan'].'-01')).' 23:59:59';
            $sdPrev = date('Y-m-d H:i:s',strtotime($sd.' -1 month'));
            $edPrev = date('Y-m-t H:i:s',strtotime($ed.' -1 month'));

            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $params = [
                'dept_id' => $_POST['dept_id'],
                'sd' => $sd,
                'ed' => $ed,
                'sdPrev' => $sdPrev,
                'edPrev' => $edPrev
            ];
            // $out = [];
            $response = $client->get('/integra/stok/bydate', $params)->send();
            $results = [];
            if ($response->isOk) 
            {
                $results = $response->data['values'];

                // foreach($results as $m)
                // {
                //     $m = (object)$m;
                //     $results[] = [
                //         'stok_id' => $m->id,
                //         'kode' => $m->kb,
                //         'nama' => $m->nm,
                //         // 'batch_no' => $m->deptStok->batch_no,
                //         // 'exp_date' => $m->deptStok->exp_date,
                //         'satuan' => $m->sat,
                //         'stok_lalu' => $m->stoklalu,//$m->stok_bulan_lalu,
                //         'masuk' => $m->masuk,
                //         'keluar' => $m->keluar,
                //         'stok_awal' => $m->stokawal,
                //         'hb' => \app\helpers\MyHelper::formatRupiah($m->hb,2),
                //         'hj' => 0,//\app\helpers\MyHelper::formatRupiah($m->barang->harga_jual,2),
                //         'subtotal' => 0,//\app\helpers\MyHelper::formatRupiah($subtotal,2),
                //     ];
                // }
            }

            // $results['total'] = $total;
            if(!empty($_POST['export']) && empty($_POST['search']))
            {
                return $this->renderPartial('_stok_bulanan', [
                    'list' => $results,
                    'model' => $model,
                    'export' => 1
                ]); 
            }

        }

        return $this->render('stok_bulanan', [
            'list' => $results,
            'model' => $model,

        ]);
    }

    public function actionOpnameRekap()
    {

        $results = [];

        if(!empty($_GET['tanggal_awal']) && !empty($_GET['tanggal_akhir']))
        {
            $listJenisBarang = \app\models\MasterJenisBarang::find()->where(['perusahaan_id'=>Yii::$app->user->identity->perusahaan_id])->orderBy(['nama'=>SORT_ASC]);
            $sd = date('Y-m-d',strtotime($_GET['tanggal_awal']));
            $ed = date('Y-m-d',strtotime($_GET['tanggal_akhir']));
            
            $is_separated = empty($_GET['export']) ?: 0;
            foreach($listJenisBarang->all() as $jb)
            {
                $tmp = [];
                $listBarang = SalesMasterBarang::find();
                $listBarang->where([
                    'jenis_barang_id'=>$jb->id,
                    'is_hapus'=>0
                ]);
                $listBarang->orderBy(['nama_barang'=>SORT_ASC]);
                foreach($listBarang->all() as $barang)
                {

                    $query = \app\models\BarangOpname::find();
                    $query->where([
                        'barang_id'=>$barang->id_barang
                    ]);
                    // $query->andWhere([\app\models\BarangOpname::tableName().'.tahun'=>$tahun.$bulan]);
                    $query->andWhere(['between','tanggal',$sd,$ed]);
                    // $query->joinWith(['departemenStok as ds']);
                    
                    $stok_riil = $query->sum('stok_riil');
                    $subtotal = $stok_riil * $barang->harga_beli;
                    $tmp[$barang->id_barang] = [
                        'kode' => $barang->kode_barang,
                        'nama' => $barang->nama_barang,
                        'satuan' => $barang->id_satuan,
                        'stok_riil' => $stok_riil,
                        'hb' => \app\helpers\MyHelper::formatRupiah($barang->harga_beli,2,$is_separated),
                        // 'hj' => \app\helpers\MyHelper::formatRupiah($m->barang->harga_jual,2),
                        'subtotal' => \app\helpers\MyHelper::formatRupiah($subtotal,2,$is_separated),
                    ];
                }

                $results[$jb->id] = [
                    'id' => $jb->id,
                    'nama' => $jb->nama,
                    'list' => $tmp
                ];
            }
            
           
            
            if(!empty($_GET['export']) && empty($_GET['search']))
            {
                return $this->renderPartial('_tabel_rekap', [
                    'list' => $results,
                    'model' => $model,
                    'export' => 1
                ]); 
            }

        }

        return $this->render('opname_rekap', [
            'list' => $results,
            'model' => $model,


        ]);
    }

    public function actionResepPasien()
    {
        $searchModel = new PenjualanSearch();
        // $dataProvider = $searchModel->searchTanggal(Yii::$app->request->queryParams,1);

        $results['items'] = [];

        if(!empty($_GET['search']) && !empty($_GET['customer_id']))
        {
            $model = new Penjualan;
            $params = Yii::$app->request->queryParams;
            $results = \app\helpers\MyHelper::loadHistoryItems(
                $params['customer_id'],
                $params['Penjualan']['tanggal_awal'],
                $params['Penjualan']['tanggal_akhir'],
            );

            // print_r($results);exit;

            return $this->render('resepPasien', [
                'results' => $results,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]); 
        }   

        else if(!empty($_GET['export']))
        {
            $listJenisResep = \app\models\JenisResep::getListJenisReseps();
    
            $params = Yii::$app->request->queryParams;
            $results = \app\helpers\MyHelper::loadHistoryItems(
                $params['customer_id'],
                $params['Penjualan']['tanggal_awal'],
                $params['Penjualan']['tanggal_akhir'],
                0
            );

            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $jenis_rawat = $_GET['jenis_rawat'];
            $out = [];
            $response = $client->get('/pasien/rm', ['key' => $params['customer_id']])->send();
            if ($response->isOk) 
            {
                $result = $response->data['values'];

                if(!empty($result[0]))
                {
                    $spreadsheet = new Spreadsheet();
                    $sheet = $spreadsheet->getActiveSheet();
                    
                    
                    $sheet->setCellValue('A3', 'Nama Px: '.$result[0]['NAMA']);
                    $sheet->setCellValue('A4', 'No RM: '.$result[0]['NoMedrec']);

                    // Add column headers
                    $sheet->setCellValue('A5', 'No')
                        ->setCellValue('B5', 'Tgl')
                        // ->setCellValue('C5', 'Nama Px')
                        // ->setCellValue('D5', 'No RM')
                        ->setCellValue('C5', 'No Resep')
                        ->setCellValue('D5', 'Jenis Resep')
                        ->setCellValue('E5', 'Poli')
                        ->setCellValue('F5', 'Dokter')
                        // ->setCellValue('I5', 'Kode Barang')
                        ->setCellValue('G5', 'Nama Barang')
                        ->setCellValue('H5', 'Qty')
                        ->setCellValue('I5', 'Subtotal')
                        ->setCellValue('J5', 'Total');

                    $sheet->mergeCells('A1:J1')->getStyle('A1:J1')->getAlignment()->setHorizontal('center');
                    $sheet->setCellValue('A1','Laporan Resep Pasien');

                    $sheet->mergeCells('A2:J2')->getStyle('A2:J2')->getAlignment()->setHorizontal('center');
                    $sheet->setCellValue('A2','Tanggal '.$_GET['Penjualan']['tanggal_awal'].' s/d '.$_GET['Penjualan']['tanggal_akhir']);

                    //Put each record in a new cell

                    $sheet->getColumnDimension('A')->setWidth(5);
                    $sheet->getColumnDimension('B')->setWidth(13);
                    $sheet->getColumnDimension('C')->setWidth(18);
                    $sheet->getColumnDimension('D')->setWidth(10);
                    $sheet->getColumnDimension('E')->setWidth(15);
                    $sheet->getColumnDimension('F')->setWidth(15);
                    $sheet->getColumnDimension('G')->setWidth(25);
                    $sheet->getColumnDimension('H')->setWidth(6);
                    $sheet->getColumnDimension('I')->setWidth(15);
                    $sheet->getColumnDimension('J')->setWidth(18);
                    // $sheet->getColumnDimension('K')->setWidth(10);
                    // $sheet->getColumnDimension('L')->setWidth(15);
                    // $sheet->getColumnDimension('M')->setWidth(20);
                    $i= 0;
                    $ii = 6;

                    $total = 0;
                    $index = 0;
                     $total = 0;
                    

                    $class = '';
                    foreach($results as $key => $obj)
                    {

                        $class = $key % 2 == 0 ? 'even' : 'odd';
                        $i++;
                        $model = $obj['data'];
                        $items = $obj['items'];
                        // $subtotal = $model['sb_blt'];
                        $total += round($model->total);
                        $kd = '';$nm = '';$qty = 0; $hb = 0;$hj = 0;
                        if(!empty($items[0])){
                            $kd = $items[0]['kd'];
                            $nm = $items[0]['nm'];
                            $qty = $items[0]['qty'];
                            $hj = $items[0]['hj'];
                            $hb = $items[0]['hb'];
                        }

                        $sub = $qty * $hj; 

                        // $sheet->getRowDimension($ii)->setRowHeight(-1);
                        $sheet->setCellValue('A'.$ii, $key+1);
                        $sheet->setCellValue('B'.$ii, date('d/m/Y',strtotime($model->tanggal)));
                        // $sheet->setCellValue('C'.$ii, $model->nm);
                        // $sheet->setCellValue('D'.$ii, $model->pid);
                        $sheet->setCellValue('C'.$ii, $model->kode_penjualan);
                        $sheet->setCellValue('D'.$ii, $listJenisResep[$model->jrid]);
                        $sheet->setCellValue('E'.$ii, $model->unm);
                        $sheet->setCellValue('F'.$ii, $model->dnm);
                        // $sheet->setCellValue('G'.$ii, $kd);
                        $sheet->setCellValue('G'.$ii, $nm);
                        $sheet->setCellValue('H'.$ii, $qty);
                        // $sheet->getStyle('L'.$ii)->getNumberFormat()->setFormatCode('#.##;[Red]-#.##');
                        $sheet->setCellValue('I'.$ii, round($sub));
                        $sheet->setCellValue('J'.$ii, round($model->total));
                        
                        
                        $ii++;
                        foreach($items as $q => $it)
                        {
                            if($q==0 ) continue;

                            $qty = $it['qty'];
                            $hj = $it['hj'];
                            $sub = $qty * $hj; 
                            // $sheet->setCellValue('I'.$ii, $it['kd']);
                            $sheet->setCellValue('G'.$ii, $it['nm']);
                            $sheet->setCellValue('H'.$ii, $qty);
                            // $sheet->getStyle('L'.$ii)->getNumberFormat()->setFormatCode('#.##;[Red]-#.##');
                            $sheet->setCellValue('I'.$ii, round($sub));
                            // $sheet->setCellValue('M'.$ii, $model->total);
                            $ii++;

                        }
                                        
                    }       

                    // $sheet->getStyle('E1:E'.$ii)->getAlignment()->setWrapText(true);
                    // $sheet->getStyle('F1:F'.$ii)->getAlignment()->setWrapText(true);
                    // $sheet->getStyle('G1:G'.$ii)->getAlignment()->setWrapText(true); 
                    // $sheet->getDefaultRowDimension()->setRowHeight(-1);
                    $sheet->setCellValue('A'.$ii, '');
                    $sheet->setCellValue('B'.$ii, '');
                    $sheet->setCellValue('C'.$ii, '');
                    $sheet->setCellValue('D'.$ii, '');
                    $sheet->setCellValue('E'.$ii, '');
                    $sheet->setCellValue('F'.$ii, '');
                    $sheet->setCellValue('G'.$ii, '');
                    $sheet->setCellValue('H'.$ii, '');
                    $sheet->setCellValue('I'.$ii, 'Total');
                    $sheet->setCellValue('J'.$ii, $total);
                    // $sheet->setCellValue('K'.$ii, '');
                    // $sheet->setCellValue('L'.$ii, 'Total');
                    // $sheet->setCellValue('M'.$ii, $total);

                    // Set worksheet title
                    $sheet->setTitle('Laporan Resep');
                    
                    ob_end_clean();
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="laporan_resep_per_pasien.xlsx"');
                    header('Cache-Control: max-age=0');
                    
                    $writer = new Xlsx($spreadsheet);
                    $writer->save('php://output');
                    exit;
                }
                
            }
            
            

           
        }

        else{
             $model = new Penjualan;
            return $this->render('resepPasien', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                'results' => $results
            ]); 
        }

        // print_r($results);exit;

        
    }


    public function actionResepRekap()
    {
        // $searchModel = new PenjualanSearch();
        // $dataProvider = $searchModel->searchTanggal(Yii::$app->request->queryParams,1);

        $results = [];




        if(!empty($_GET['search']))
        {

            $tipe = $_GET['jenis_rawat'] == 1 ? 2 : 1;
        
            $tanggal_awal = date('Y-m-d',strtotime($_GET['Penjualan']['tanggal_awal']));
            $tanggal_akhir = date('Y-m-d',strtotime($_GET['Penjualan']['tanggal_akhir']));
            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $response = $client->get('/m/unit/list', ['tipe'=>$tipe])->send();
            
            $results = [];
            
            if ($response->isOk) {
                $result = $response->data['values'];
                
                if(!empty($result))
                {
                 
                    foreach ($result as $d) {
                        $label = $d['unit_tipe'] == 2 ? 'Poli '.$d['NamaUnit'] : $d['NamaUnit'];

                        $query = \app\models\PenjualanResep::find()->where([
                            'unit_id'=>$d['KodeUnit']

                        ]);


                        if(!empty($_GET['jenis_resep_id'])){
                            $query = \app\models\PenjualanResep::find()->where([
                                'unit_id'=>$d['KodeUnit'],
                                'jenis_resep_id' => $_GET['jenis_resep_id']
                            ]);                            
                        }

                        $query->joinWith(['penjualan as p']);
                        $query->andWhere(['p.status_penjualan'=>1]);
                        $query->andWhere(['p.is_removed'=>0]);
                        $query->andWhere(['between','p.tanggal',$tanggal_awal,$tanggal_akhir]);
                        $listResep = $query->all();

                        $total = 0;
                        $count = count($listResep);
                        foreach($listResep as $item)
                        {
                            $p = Penjualan::findOne($item->penjualan_id);
                            foreach($p->penjualanItems as $it)
                            {
                                $jml = ($it->jumlah_ke_apotik * round($it->harga));
                                $total += $jml;
                            }
                            
                        }

                        $avg = $total / ($count == 0 ? 1 : $count);

                        $results[] = [
                            'id' => $d['KodeUnit'],
                            'label'=> $label,
                            'count' => $count,
                            'total' => round($total,2) ,
                            'avg' => round($avg,2) 
                        ];
                    }    
                }

                else
                {
                    $results[] = [
                        'id' => 0,
                        'label'=> 'Data tidak ditemukan',
                       
                    ];
                }
                
            }

            
            $model = new Penjualan;
            return $this->render('resep_rekap', [
                // 'searchModel' => $searchModel,
                // 'dataProvider' => $dataProvider,
                'model' => $model,
                'results' => $results
            ]); 
        }   

        else if(!empty($_GET['export']))
        {
             
            $listJenisResep = \app\models\JenisResep::getListJenisReseps();


            
            $tipe = $_GET['jenis_rawat'] == 1 ? 2 : 1;
        
            $tanggal_awal = date('Y-m-d',strtotime($_GET['Penjualan']['tanggal_awal']));
            $tanggal_akhir = date('Y-m-d',strtotime($_GET['Penjualan']['tanggal_akhir']));
            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $response = $client->get('/m/unit/list', ['tipe'=>$tipe])->send();
            
            $results = [];
            
            if ($response->isOk) {
                $result = $response->data['values'];
                
                if(!empty($result))
                {
                 
                    foreach ($result as $d) {
                        $label = $d['unit_tipe'] == 2 ? 'Poli '.$d['NamaUnit'] : $d['NamaUnit'];

                        $query = \app\models\PenjualanResep::find()->where([
                            'unit_id'=>$d['KodeUnit']

                        ]);


                        if(!empty($_GET['jenis_resep_id'])){
                            $query = \app\models\PenjualanResep::find()->where([
                                'unit_id'=>$d['KodeUnit'],
                                'jenis_resep_id' => $_GET['jenis_resep_id']
                            ]);                            
                        }

                        $query->joinWith(['penjualan as p']);
                        $query->andWhere(['p.status_penjualan'=>1]);
                        $query->andWhere(['p.is_removed'=>0]);
                        $query->andWhere(['between','p.tanggal',$tanggal_awal,$tanggal_akhir]);
                        $listResep = $query->all();

                        $total = 0;
                        $count = count($listResep);
                        foreach($listResep as $item)
                        {
                            $total += Penjualan::getTotalSubtotal($item->penjualan);
                        }

                        $avg = $total / ($count == 0 ? 1 : $count);

                        $results[] = [
                            'id' => $d['KodeUnit'],
                            'label'=> $label,
                            'count' => $count,
                            'total' => \app\helpers\MyHelper::formatRupiah($total,2) ,
                            'avg' => \app\helpers\MyHelper::formatRupiah($avg,2) 
                        ];
                    }    
                }

                else
                {
                    $results[] = [
                        'id' => 0,
                        'label'=> 'Data tidak ditemukan',
                       
                    ];
                }
                
            }

            
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            // Add column headers
            $sheet->setCellValue('A3', 'No')
                ->setCellValue('B3', 'Poli')
                ->setCellValue('C3', 'Jumlah Resep')
                ->setCellValue('D3', 'Nominal Resep')
                ->setCellValue('E3', 'Rata-rata Resep');

            $sheet->mergeCells('A1:E1')->getStyle('A1:E1')->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('A1','Laporan Rekapitulasi Nominal Resep '.(!empty($_GET['jenis_resep_id']) ? $listJenisResep[$_GET['jenis_resep_id']] : ''));

            $sheet->mergeCells('A2:E2')->getStyle('A2:E2')->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('A2','Tanggal '.$_GET['Penjualan']['tanggal_awal'].' s/d '.$_GET['Penjualan']['tanggal_akhir']);

            //Put each record in a new cell

            $sheet->getColumnDimension('A')->setWidth(5);
            $sheet->getColumnDimension('B')->setWidth(30);
            $sheet->getColumnDimension('C')->setWidth(15);
            $sheet->getColumnDimension('D')->setWidth(30);
            $sheet->getColumnDimension('E')->setWidth(30);
            $i= 0;
            $ii = 4;

            $total = 0;
            $jml = 0;
            $total_avg = 0;
            foreach($results as $key => $model)
            {
               
                $jml += $model['count'];
                $total += $model['total'];
                $total_avg += $model['avg'];
                $i++;

                $sheet->setCellValue('A'.$ii, ($key+1));
                $sheet->setCellValue('B'.$ii, $model['label']);
                $sheet->setCellValue('C'.$ii, $model['count']);
                $sheet->setCellValue('D'.$ii, $model['total']);
                $sheet->setCellValue('E'.$ii, $model['avg']);
              
                // $objPHPExcel->getActiveSheet()->setCellValue('H'.$ii, $row->subtotal);
                
                $ii++;

                
            }       

            $sheet->setCellValue('A'.$ii, '');
            $sheet->setCellValue('B'.$ii, 'Total');
            $sheet->setCellValue('C'.$ii, $jml);
            $sheet->setCellValue('D'.$ii, $total);
            $sheet->setCellValue('E'.$ii, $total_avg);

            // Set worksheet title
            $sheet->setTitle('Lap Rekap Resep');
            
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="laporan_rekap_resep.xlsx"');
            header('Cache-Control: max-age=0');
            
            $writer = new Xlsx($spreadsheet);
            $writer->save('php://output');
            
            exit;
        }

        else{
            
            $tipe = 2;
        
            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $response = $client->get('/m/unit/list', ['tipe'=>$tipe])->send();
            
            $results = [];
            
            if ($response->isOk) {
                $result = $response->data['values'];
                
                if(!empty($result))
                {
                    foreach ($result as $d) {
                        $label = $d['unit_tipe'] == 2 ? 'Poli '.$d['NamaUnit'] : $d['NamaUnit'];

                        $query = \app\models\PenjualanResep::find()->where([
                            'unit_id'=>$d['KodeUnit']

                        ]);


                        if(!empty($_GET['jenis_resep_id'])){
                            $query = \app\models\PenjualanResep::find()->where([
                                'unit_id'=>$d['KodeUnit'],
                                'jenis_resep_id' => $_GET['jenis_resep_id']
                            ]);                            
                        }

                        $query->joinWith(['penjualan as p']);
                        $query->andWhere(['p.status_penjualan'=>1]);
                        $query->andWhere(['p.is_removed'=>0]);
                        $query->andWhere(['between','p.tanggal',date('Y-m-d'),date('Y-m-d')]);
                        $listResep = $query->all();

                        $total = 0;
                        $count = count($listResep);
                        foreach($listResep as $item)
                        {
                            $total += Penjualan::getTotalSubtotal($item->penjualan);
                        }

                        $avg = $total / ($count == 0 ? 1 : $count);

                        $results[] = [
                            'id' => $d['KodeUnit'],
                            'label'=> $label,
                            'count' => $count,
                            'total' => \app\helpers\MyHelper::formatRupiah($total,2) ,
                            'avg' => \app\helpers\MyHelper::formatRupiah($avg,2) 
                        ];
                    }    
                }

                else
                {
                    $results[] = [
                        'id' => 0,
                        'label'=> 'Data tidak ditemukan',
                       
                    ];
                }
                
            }

            
            $model = new Penjualan;
            return $this->render('resep_rekap', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                'results' => $results
            ]); 
        }

        // print_r($results);exit;

        
    }

    public function actionResep()
    {
        $searchModel = new PenjualanSearch();
        // $dataProvider = $searchModel->searchTanggal(Yii::$app->request->queryParams,1);

        $results = [];
        $listJenisResep = \app\models\JenisResep::getListJenisReseps();
        // $results = $dataProvider;

        if(!empty($_GET['search']))
        {
            $params = Yii::$app->request->queryParams;
            $sd = date('Y-m-d',strtotime($params['Penjualan']['tanggal_awal']));
            $ed = date('Y-m-d',strtotime($params['Penjualan']['tanggal_akhir']));
            $dept_id = Yii::$app->user->identity->departemen;
            $jenis_resep = $_GET['jenis_resep_id'];
            $jenis_rawat = $_GET['jenis_rawat'];
            $unit_id = $_GET['unit_id'];
            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $response = $client->get('/integra/penjualan/rinci', [
                'sd' => $sd,
                'ed'=>$ed,
                'dept_id'=>$dept_id,
                'jid' => $jenis_resep,
                'jr' => $jenis_rawat,
                'uid' => $unit_id
            ])->send();
            if($response->isOk)
            {
                $results = $response->data['values'];
            }
            $model = new Penjualan;
            return $this->render('resep', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                'results' =>$results,
                'listJenisResep' => $listJenisResep,
                'export' => 0
            ]); 
        }   

        else if(!empty($_GET['export']))
        {
            
            // $themeUrl = Yii::$app->view->theme->baseUrl;

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setPath('images/logo-rsud.jpg'); // put your path and image here
            $drawing->setCoordinates('A1');
            $drawing->setWidthAndHeight(60,60);
            $drawing->setResizeProportional(true);
            $drawing->setOffsetX(110);
            // $drawing->getShadow()->setVisible(true);
            // $drawing->getShadow()->setDirection(45);
            $drawing->setWorksheet($sheet);
            // Add column headers
            $sheet->setCellValue('A5', 'No')
                ->setCellValue('B5', 'Tgl')
                ->setCellValue('C5', 'Nama Px')
                ->setCellValue('D5', 'No RM')
                ->setCellValue('E5', 'No Resep')
                ->setCellValue('F5', 'Jenis Resep')
                ->setCellValue('G5', 'Poli')
                ->setCellValue('H5', 'Dokter')
                ->setCellValue('I5', 'Jml ke BPJS')
                ->setCellValue('J5', 'Jml ke Apotek')
                ->setCellValue('K5', 'Total');

            // $sheet->mergeCells('A1:B2');
            $sheet->mergeCells('A1:K1')->getStyle('A1:K1')->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('A1','Laporan Penjualan Resep');

            $sheet->mergeCells('A2:K2')->getStyle('A2:K2')->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('A2','RUMAH SAKIT UMUM DAERAH');

            $sheet->mergeCells('A3:K3')->getStyle('A3:K3')->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('A3','PARE - KABUPATEN KEDIRI');

            $sheet->mergeCells('A4:K4')->getStyle('A4:K4')->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('A4','Tanggal '.$_GET['Penjualan']['tanggal_awal'].' s/d '.$_GET['Penjualan']['tanggal_akhir']);

            //Put each record in a new cell

            $sheet->getColumnDimension('A')->setWidth(5);
            $sheet->getColumnDimension('B')->setWidth(15);
            $sheet->getColumnDimension('C')->setWidth(35);
            $sheet->getColumnDimension('D')->setWidth(10);
            $sheet->getColumnDimension('E')->setWidth(30);
            $sheet->getColumnDimension('F')->setWidth(20);
            $sheet->getColumnDimension('G')->setWidth(30);
            $sheet->getColumnDimension('H')->setWidth(20);
            $sheet->getColumnDimension('I')->setWidth(20);
            $sheet->getColumnDimension('J')->setWidth(20);
            $sheet->getColumnDimension('K')->setWidth(20);
            $i= 0;
            $ii = 6;

            $total = 0;
             $total_ke_apotek = 0;
            $total_ke_bpjs = 0;

            $params = Yii::$app->request->queryParams;
            $sd = date('Y-m-d',strtotime($params['Penjualan']['tanggal_awal']));
            $ed = date('Y-m-d',strtotime($params['Penjualan']['tanggal_akhir']));
            $dept_id = Yii::$app->user->identity->departemen;
            $jenis_resep = $_GET['jenis_resep_id'];
            $jenis_rawat = $_GET['jenis_rawat'];
            $unit_id = $_GET['unit_id'];
            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $response = $client->get('/integra/penjualan/rinci', [
                'sd' => $sd,
                'ed'=>$ed,
                'dept_id'=>$dept_id,
                'jid' => $jenis_resep,
                'jr' => $jenis_rawat,
                'uid' => $unit_id
            ])->send();
            if($response->isOk)
            {
                $results = $response->data['values'];
                foreach($results as $key => $model)
                {
                    $model = (object)$model;
                    $total_ke_bpjs += $model->jb;
                    $total_ke_apotek += $model->ja;
                    $total += $model->sb;
                    $i++;

                    $sheet->setCellValue('A'.$ii, $i);
                    $sheet->setCellValue('B'.$ii, date('d/m/Y',strtotime($model->tgl)));
                    $sheet->setCellValue('C'.$ii, $model->nm);
                    $sheet->setCellValue('D'.$ii, $model->rm);
                    $sheet->setCellValue('E'.$ii, $model->trx);
                    $sheet->setCellValue('F'.$ii, $listJenisResep[$model->jr]);
                    $sheet->setCellValue('G'.$ii, $model->unm);
                    $sheet->setCellValue('H'.$ii, $model->dnm);
                    $sheet->setCellValue('I'.$ii, round($model->jb,2));
                    $sheet->setCellValue('J'.$ii, round($model->ja,2));
                    $sheet->setCellValue('K'.$ii, round($model->sb,2));
                    // $objPHPExcel->getActiveSheet()->setCellValue('H'.$ii, $row->subtotal);
                    $sheet->getStyle('K'.$ii)
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    $sheet->getStyle('I'.$ii)
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    $sheet->getStyle('J'.$ii)
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

                    $ii++;

                    
                }       
            }

            else{
                print_r($response);exit;
            }
            $sheet->setCellValue('A'.$ii, '');
            $sheet->setCellValue('B'.$ii, '');
            $sheet->setCellValue('C'.$ii, '');
            $sheet->setCellValue('D'.$ii, '');
            $sheet->setCellValue('E'.$ii, '');
            $sheet->setCellValue('F'.$ii, '');
            $sheet->setCellValue('G'.$ii, '');
            $sheet->setCellValue('H'.$ii, 'Total');
            $sheet->setCellValue('I'.$ii, round($total_ke_bpjs,2));
            $sheet->setCellValue('J'.$ii, round($total_ke_apotek,2));
            $sheet->setCellValue('K'.$ii, round($total,2));
            $sheet->getStyle('K'.$ii)
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    $sheet->getStyle('I'.$ii)
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    $sheet->getStyle('J'.$ii)
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            // Set worksheet title
            $sheet->setTitle('Laporan Resep');

            ob_end_clean();
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="laporan_resep.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = new Xlsx($spreadsheet);
            $writer->save('php://output');
            
            exit;
        }

        else{
             $model = new Penjualan;
            return $this->render('resep', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                'results' => $results,
                'listJenisResep' => $listJenisResep,
            ]); 
        }

        // print_r($results);exit;

        
    }

    
    public function actionEdTahunan(){

        $results = [];
        if(!empty($_POST['tanggal_awal']) && !empty($_POST['tanggal_akhir']) && !empty($_POST['gudang_id']))
        {


            $tanggal_awal = $_POST['tanggal_awal'];
            $tanggal_akhir = $_POST['tanggal_akhir'];
            $a = $tanggal_awal;
            $b = $tanggal_akhir;

            $i = date("Ym", strtotime($a));
            $list_bulan = [];
            while($i <= date("Ym", strtotime($b))){
                $list_bulan[] = $i;
                if(substr($i, 4, 2) == "12")
                    $i = (date("Y", strtotime($i."01")) + 1)."01";
                else
                    $i++;
            }


            foreach($list_bulan as $bln)
            {
                $y = substr($bln, 0,4);
                $mm = substr($bln,4,2);
                $ds = $y.'-'.$mm.'-01';
                $de = $y.'-'.$mm.'-31';
                $query = SalesStokGudang::find();
                $query->joinWith(['barang as barang']);
                $query->where(['id_gudang'=>$_POST['gudang_id']]);
                $query->andWhere(['barang.is_hapus'=>0]);
                // $query->andWhere(['id_barang'=>$obat->id_barang]);
                $query->andWhere(['between','exp_date',$ds,$de]);
                $query->orderBy(['exp_date'=>SORT_ASC]);
                $list = $query->all();
                
                $total = 0;
                foreach($list as $q => $m)
                {
                    
                    if($m->jumlah > 0)
                    {
                        $subtotal = $m->jumlah * $m->barang->harga_beli;
                        $total += $subtotal;
                        $results[] = [
                            'bulan' => date('M',strtotime($m->exp_date)),
                            'tahun' => $y, 
                            'stok_id' => $m->id_stok,
                            'kode' => $m->barang->kode_barang,
                            'nama' => $m->barang->nama_barang,
                            'satuan' => $obat->id_satuan,
                            'ed' => $m->exp_date,
                            'jumlah' => $m->jumlah,
                            'hb' => \app\helpers\MyHelper::formatRupiah($m->barang->harga_beli,2),
                            'hj' => \app\helpers\MyHelper::formatRupiah($m->barang->harga_jual,2),
                            'subtotal' => \app\helpers\MyHelper::formatRupiah($subtotal,2),
                        ];
                    }
                    
                }
            }

            $results['total'] = $total;

        }

        return $this->render('ed_tahunan', [
            'list' => $results,
            'model' => $model,

        ]);
    }

    public function actionJenisBarang(){

        $results = [];
        $query = \app\models\MasterJenisBarang::find();
        $query->where(['tipe'=>2]);
        
        $list = $query->all();
        

        if(!empty($_POST['tanggal']) && !empty($_POST['dept_id']))
        {


            $tanggal = date('d',strtotime($_POST['tanggal']));
            $bulan = date('m',strtotime($_POST['tanggal']));
            $tahun = date('Y',strtotime($_POST['tanggal']));
            
            // $listKode = ['GDO','APT','APB','DIST'];
            // $listUnit = [];
            // foreach($listKode as $q=>$v){
            //     $listUnit[] = \app\models\Departemen::find()->where(['kode'=>$v])->one();
            // }

            $total = 0;
            foreach($list as $q => $m)
            {

                $query = \app\models\BarangOpname::find();
                $query->where(['<>','barang.nama_barang','-']);
                $query->andWhere(['od.nar_p_non'=>$m->kode]);
                $query->andWhere(['ds.departemen_id'=>$_POST['dept_id']]);
                $query->andWhere(['barang.is_hapus'=>0]);
                $query->andWhere([\app\models\BarangOpname::tableName().'.tahun'=>$tahun.$bulan]);
                $query->joinWith(['barang as barang','departemenStok as ds','barang.obatDetil as od']);
                $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
                $listBarang = $query->all();
                // print_r($listBarang);exit;
                foreach($listBarang as $item)
                {
                    $barang_id = $item->departemenStok->barang_id;


                    $kartuStok = \app\models\KartuStok::find()->where([
                        'barang_id' => $barang_id,
                        'departemen_id' => $_POST['dept_id'],

                    ]);

                    $tanggal_awal = date('Y-m-01',strtotime($_POST['tanggal']));
                    $tanggal_akhir = date('Y-m-t',strtotime($_POST['tanggal']));

                    $kartuStok->andFilterWhere(['between', 'tanggal', $tanggal_awal, $tanggal_akhir]);
                    $qry = $kartuStok->all();
                    $qty_in = 0;
                    $qty_out = 0;
                    foreach($qry as $ks)
                    {
                        $qty_in += $ks->qty_in;
                        $qty_out += $ks->qty_out;
                    }

                    $subtotal = $item->stok_riil * $item->barang->harga_beli;
                    $total += $subtotal;

                    $results[$m->id][] = [
                        'stok_id' => $item->departemenStok->barang_id,
                        'kode' => $item->barang->kode_barang,
                        'nama' => $item->barang->nama_barang,
                        'satuan' => $item->barang->id_satuan,
                        'stok_lalu' => $item->stok_lalu,
                        'masuk' => $qty_in,
                        'keluar' => $qty_out,
                        'stok_riil' => $item->stok_riil,
                        'hb' => \app\helpers\MyHelper::formatRupiah($item->barang->harga_beli,2),
                        'hj' => \app\helpers\MyHelper::formatRupiah($item->barang->harga_jual,2),
                        'subtotal' => \app\helpers\MyHelper::formatRupiah($subtotal,2),
                    ];
                }
                
                
            }


            $results['total'] = $total;
            if(!empty($_POST['export']) && empty($_POST['search']))
            {
                return $this->renderPartial('_tabel_jenis_barang', [
                    'list' => $list,
                    'results' => $results,
                    'model' => $model,
                    'export' => 1
                ]); 
            }

        }


        return $this->render('jenis_obat', [
            'list' => $list,
            'results' => $results,
            'model' => $model,

        ]);
    }

    public function actionOpnameBulanan()
    {

        $results = [];

        if(!empty($_POST['tanggal']) && !empty($_POST['dept_id']))
        {


            // $tanggal = date('d',strtotime($_POST['tanggal']));
            // $bulan = date('m',strtotime($_POST['tanggal']));
            // $tahun = date('Y',strtotime($_POST['tanggal']));
            // $query = \app\models\BarangOpname::find();
            // $query->where(['<>','barang.nama_barang','-']);
            // $query->andWhere(['ds.departemen_id'=>$_POST['dept_id']]);
            // $query->andWhere(['barang.is_hapus'=>0]);
            // if(!empty($_POST['jenis_barang_id']))
            //     $query->andWhere(['barang.jenis_barang_id'=>$_POST['jenis_barang_id']]);
            // $query->andWhere([\app\models\BarangOpname::tableName().'.tahun'=>$tahun.$bulan]);
            // $query->joinWith(['barang as barang','departemenStok as ds']);
            // $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $query = SalesMasterBarang::find()->where([
                'is_hapus'=>0,
                'jenis_barang_id' => $_POST['jenis_barang_id']
            ]);
            $query->orderBy(['nama_barang'=>SORT_ASC])->all();
            $list = $query->all();

            $total = 0;
            $tanggal_awal = date('Y-m-01 00:00:01',strtotime($_POST['tanggal']));
            $tanggal_akhir = date('Y-m-t 23:59:59',strtotime($_POST['tanggal']));

            foreach($list as $q => $m)
            {
                $barang_id = $m->id_barang;


                $kartuStok = \app\models\StokHistory::find()->where([
                    'barang_id' => $barang_id,
                    'departemen_id' => $_POST['dept_id'],

                ]);

                

                $kartuStok->andFilterWhere(['between', 'created_at', $tanggal_awal, $tanggal_akhir]);
                $qry = $kartuStok->all();
                // print_r($qry[0]);exit;
                $stok_awal = $qry[0]->sisa;
                $stok_akhir = $qry[count($qry)-1]->sisa;
                $qty_in = 0;
                $qty_out = 0;
                foreach($qry as $ks)
                {
                    $qty_in += $ks->qty_in;
                    $qty_out += $ks->qty_out;
                }

                $subtotal = $stok_akhir * $m->harga_beli;
                $total += $subtotal;
                $results[] = [
                    'stok_id' => $m->id_barang,
                    'kode' => $m->kode_barang,
                    'nama' => $m->nama_barang,
                    // 'batch_no' => $m->departemenStok->batch_no,
                    // 'exp_date' => $m->departemenStok->exp_date,
                    'satuan' => $m->id_satuan,
                    'stok_awal' => $stok_awal,
                    'masuk' => $qty_in,
                    'keluar' => $qty_out,
                    'stok_akhir' => $stok_akhir,
                    // 'stok_riil' => $m->stok_riil,
                    'hb' => \app\helpers\MyHelper::formatRupiah($m->harga_beli,2),
                    'hj' => \app\helpers\MyHelper::formatRupiah($m->harga_jual,2),
                    'subtotal' => \app\helpers\MyHelper::formatRupiah($subtotal,2),
                ];
            }


            $results['total'] = $total;
            if(!empty($_POST['export']) && empty($_POST['search']))
            {
                return $this->renderPartial('_tabel_opname', [
                    'list' => $results,
                    'model' => $model,
                    'export' => 1
                ]); 
            }

        }

        return $this->render('opname_bulanan', [
            'list' => $results,
            'model' => $model,

        ]);
    }

    public function actionMutasiKeluar()
    {
        $searchModel = new RequestOrderSearch();
        $dataProvider = $searchModel->searchByTanggal(Yii::$app->request->queryParams);


        if(!empty($_GET['search']))
        {
            $model = new RequestOrder;
            return $this->render('mutasi_barang_keluar', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                // 'results' => $dataProvider->getModels(),

            ]); 
        }   

        else if(!empty($_GET['export']))
        {
             
            $params = $_GET;
            $dataProvider = $searchModel->searchByTanggal($params);

            
            
          
            return $this->renderPartial('_tabel_mutasi_keluar', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                // 'results' => $results,
                'export' => 1
            ]); 
        }

        else{
            $model = new RequestOrder;
            return $this->render('mutasi_barang_keluar', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                'results' => $results
            ]); 
        }

        // print_r($results);exit;

        
    }

    public function actionMutasiMasuk()
    {
        $searchModel = new SalesFakturSearch();
        $dataProvider = $searchModel->searchByTanggal(Yii::$app->request->queryParams);


        // print_r(Yii::$app->request->queryParams);exit;

        if(!empty($_GET['search']))
        {
            $model = new SalesFaktur;
            return $this->render('mutasi_barang_masuk', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                // 'results' => $results,

            ]); 
        }   

        else if(!empty($_GET['export']))
        {
             
            $params = $_GET;
            $dataProvider = $searchModel->searchByTanggal($params);

            return $this->renderPartial('_tabel_mutasi_masuk', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                // 'results' => $results,
                'export' => 1
            ]); 
        }

        else{
            $model = new SalesFaktur;
            return $this->render('mutasi_barang_masuk', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                // 'results' => $results
            ]); 
        }

        // print_r($results);exit;

        
    }

    public function actionPenjualanRekap()
    {
        $model = new SalesMasterBarang;
        
        $listDepartment = \app\models\Departemen::find()->all();
            
        $results = [];
        if(!empty($_GET['search']))
        {

            
            $dept_id = Yii::$app->request->queryParams['SalesMasterBarang']['dept_id'];
            $model->dept_id = $dept_id;
            $sd = Yii::$app->request->queryParams['SalesMasterBarang']['tanggal_awal'];
            $ed = Yii::$app->request->queryParams['SalesMasterBarang']['tanggal_akhir'];
            $sd = \app\helpers\MyHelper::dmYtoYmd($sd);
            $ed = \app\helpers\MyHelper::dmYtoYmd($ed);
            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $response = $client->get('/integra/penjualan/rekap', [
                'sd' => $sd,
                'ed' => $ed,
                'dept_id' => $dept_id
            ])->send();

            if ($response->isOk) 
                $results = $response->data['values'];
            
            return $this->render('penjualan_bulanan', [
                'results' => $results,
                'model' => $model,
                'listDepartment' => $listDepartment,
                'export' => 0
            ]); 
        }   

        else if(!empty($_GET['export']))
        {             


            $dept_id = Yii::$app->request->queryParams['SalesMasterBarang']['dept_id'];
            $results = [];
            $sd = Yii::$app->request->queryParams['SalesMasterBarang']['tanggal_awal'];
            $ed = Yii::$app->request->queryParams['SalesMasterBarang']['tanggal_akhir'];
            $sd = \app\helpers\MyHelper::dmYtoYmd($sd);
            $ed = \app\helpers\MyHelper::dmYtoYmd($ed);
            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $response = $client->get('/integra/penjualan/rekap', [
                'sd' => $sd,
                'ed' => $ed,
                'dept_id' => $dept_id
            ])->send();

            if ($response->isOk) 
                $results = $response->data['values'];


            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            
            // // Add column headers
            $sheet->mergeCells('A1:F1');
            

            $sheet->getStyle("A1:F1")->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('A1','Laporan Rekap Penjualan Obat dari '.$tanggal_awal.' hingga '.$tanggal_akhir);
            $sheet
                ->setCellValue('A2', 'No')
                ->setCellValue('B2', 'Kode Barang')
                ->setCellValue('C2', 'Nama Barang')
                ->setCellValue('D2', 'Qty')
                ->setCellValue('E2', 'HJ')
                ->setCellValue('F2', 'Subtotal');

            $sheet->getColumnDimension('A')->setWidth(5);
            $sheet->getColumnDimension('B')->setWidth(15);
            $sheet->getColumnDimension('C')->setWidth(45);
            $sheet->getColumnDimension('D')->setWidth(10);
            $sheet->getColumnDimension('E')->setWidth(20);
            $sheet->getColumnDimension('F')->setWidth(20);

            //Put each record in a new cell

            $i= 1;
            $ii = 3;

            $total = 0;

            foreach($results as $key => $model)
            {
                $model = (object)$model;
                $qty = $model->qty;
                
                $subtotal = $qty * $model->hj;
                $total += $subtotal;
                
                $sheet->setCellValue('A'.$ii, $i);
                $sheet->setCellValue('B'.$ii, $model->kd);
                $sheet->setCellValue('C'.$ii, $model->nm);
                $sheet->setCellValue('D'.$ii, $qty);
                $sheet->setCellValue('E'.$ii, round($model->hj,2));
                $sheet->setCellValue('F'.$ii, round($subtotal,2));
                
                // $objPHPExcel->getActiveSheet()->setCellValue('H'.$ii, $row->subtotal);
                $i++;
                $ii++;
            }       

            $sheet->setCellValue('A'.$ii, '');
            $sheet->setCellValue('B'.$ii, '');
            $sheet->setCellValue('C'.$ii, '');
            $sheet->setCellValue('D'.$ii, '');
            $sheet->setCellValue('E'.$ii, 'Total');
            $sheet->setCellValue('F'.$ii, round($total,2));

            // Set worksheet title
            $sheet->setTitle('Laporan Rekap Penjualan');
            
            ob_end_clean();
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="laporan_rekap_penjualan_'.$dept_id.'_'.$sd.' - '.$ed.'.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = new Xlsx($spreadsheet);
            $writer->save('php://output');
            exit;
        }

        else{
            $model = new SalesMasterBarang;

            return $this->render('penjualan_bulanan', [
                'model' => $model,
                'listDepartment' => $listDepartment,
                'results' => $results
            ]); 
        }

        // print_r($results);exit;

        
    }

    /**
     * Lists all Penjualan models.
     * @return mixed
     */
    public function actionPenjualan()
    {
        $searchModel = new PenjualanItemSearch();
        $dataProvider = $searchModel->searchTanggalDataProvider(Yii::$app->request->queryParams,1);

        if(!empty($_GET['search']))
        {
            $model = new PenjualanItem;
            return $this->render('penjualan', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                'results' => $results,
                'export' => 0
            ]); 
        }   

        else if(!empty($_GET['export']))
        {             
            $sd = Yii::$app->request->queryParams['PenjualanItem']['tanggal_awal'];
            $ed = Yii::$app->request->queryParams['PenjualanItem']['tanggal_akhir'];

            $selisih = \app\helpers\MyHelper::getSelisihHari($sd, $ed);

            if($selisih > 31)
            {
                Yii::$app->session->setFlash('error', "Selisih hari tidak boleh melebihi 31 hari");
                $model = new PenjualanItem;
                return $this->render('penjualan', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'results' => $results,
                    'export' => 0
                ]); 
            }

            $tanggal_awal = date('d/m/Y',strtotime(Yii::$app->request->queryParams['PenjualanItem']['tanggal_awal']));
            $tanggal_akhir = date('d/m/Y',strtotime(Yii::$app->request->queryParams['PenjualanItem']['tanggal_akhir']));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            
            // // Add column headers
            $sheet->mergeCells('A1:I1');
            

            $sheet->getStyle("A1:I1")->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('A1','Laporan Penjualan Obat dari '.$tanggal_awal.' hingga '.$tanggal_akhir);
            $sheet
                ->setCellValue('A2', 'No')
                ->setCellValue('B2', 'Tgl')
                ->setCellValue('C2', 'Kode Trx')
                ->setCellValue('D2', 'Kode')
                ->setCellValue('E2', 'Nama')
                ->setCellValue('F2', 'Qty')
                ->setCellValue('G2', 'HB')
                ->setCellValue('H2', 'HJ')
                ->setCellValue('I2', 'Laba');

            $sheet->getColumnDimension('A')->setWidth(5);
            $sheet->getColumnDimension('B')->setWidth(15);
            $sheet->getColumnDimension('C')->setWidth(20);
            $sheet->getColumnDimension('D')->setWidth(15);
            $sheet->getColumnDimension('E')->setWidth(30);
            $sheet->getColumnDimension('F')->setWidth(8);
            $sheet->getColumnDimension('G')->setWidth(20);
            $sheet->getColumnDimension('H')->setWidth(20);
            $sheet->getColumnDimension('I')->setWidth(20);

            //Put each record in a new cell

            $i= 1;
            $ii = 3;

            $total = 0;
            $dataProvider = $searchModel->searchTanggal(Yii::$app->request->queryParams,1);


            // $rowTotal = count($dataProvider);
            // $itemPerFile = 1000;
            // $totalFiles = ceil($rowTotal / $itemPerFile);



            // for($index = 1;$index <= $totalFiles;$index++)
            // {

            // }

            foreach($dataProvider as $row)
            {
                  $laba = ($row->harga - $row->harga_beli) * $row->qty;
                $total += $laba;
                
                $sheet->setCellValue('A'.$ii, $i);
                $sheet->setCellValue('B'.$ii, date('d/m/Y',strtotime($row->penjualan->tanggal)));
                $sheet->setCellValue('C'.$ii, $row->penjualan->kode_penjualan);
                $sheet->setCellValue('D'.$ii, $row->stok->barang->kode_barang);
                $sheet->setCellValue('E'.$ii, $row->stok->barang->nama_barang);
                $sheet->setCellValue('F'.$ii, $row->qty);
                $sheet->setCellValue('G'.$ii, round($row->harga_beli,2));
                $sheet->setCellValue('H'.$ii, round($row->harga,2));
                $sheet->setCellValue('I'.$ii, round($laba,2));
                // $objPHPExcel->getActiveSheet()->setCellValue('H'.$ii, $row->subtotal);
                $i++;
                $ii++;
            }       

            $sheet->setCellValue('A'.$ii, '');
            $sheet->setCellValue('B'.$ii, '');
            $sheet->setCellValue('C'.$ii, '');
            $sheet->setCellValue('D'.$ii, '');
            $sheet->setCellValue('E'.$ii, '');
            $sheet->setCellValue('F'.$ii, '');
            $sheet->setCellValue('G'.$ii, '');
            $sheet->setCellValue('H'.$ii, 'Total Laba');
            $sheet->setCellValue('I'.$ii, round($total,2));

            // Set worksheet title
            $sheet->setTitle('Laporan Penjualan');
            
            ob_end_clean();
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="laporan_penjualan.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = new Xlsx($spreadsheet);
            $writer->save('php://output');
            exit;
        }

        else{
             $model = new PenjualanItem;
            return $this->render('penjualan', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                'results' => $results
            ]); 
        }

        // print_r($results);exit;

        
    }

    /**
     * Displays a single Penjualan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Penjualan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Penjualan();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Penjualan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Penjualan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Penjualan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Penjualan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Penjualan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
