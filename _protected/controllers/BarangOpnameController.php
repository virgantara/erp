<?php

namespace app\controllers;

use Yii;
use app\models\BarangOpname;
use app\models\BarangOpnameSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\DepartemenStok;
use app\models\KartuStok;
use app\models\StokHistory;
use app\models\DepartemenStokHistory;
/**
 * BarangOpnameController implements the CRUD actions for BarangOpname model.
 */
class BarangOpnameController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BarangOpname models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BarangOpnameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BarangOpname model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionList(){

        $list = [];
        if(!empty($_POST['tanggal']) && !empty($_POST['btn-cari']))
        {

            $tanggal = date('d',strtotime($_POST['tanggal']));
            $bulan = date('m',strtotime($_POST['tanggal']));
            $tahun = date('Y',strtotime($_POST['tanggal']));

            $query = DepartemenStok::find();
            $query->where(['<>','barang.nama_barang','-']);
            $query->andWhere(['departemen_id'=>$_POST['dept_id']]);
            // $query->andWhere(['departemen_id'=>Yii::$app->user->identity->departemen]);
            // $query->andWhere(['tahun'=>$tahun.$bulan]);
            $query->andWhere(['barang.is_hapus'=>0]);
            $query->joinWith(['barang as barang']);
            $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $list = $query->all();

        }

        return $this->render('create', [
            'list' => $list,
            'model' => $model,

        ]);
    }

  

    /**
     * Creates a new BarangOpname model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        $list = [];

        // print_r($_POST);exit;

        if(!empty($_GET['cari']))
        {
            $tgl = date('Y-m-d',strtotime($_GET['tanggal']));
            $tanggal = date('d',strtotime($_GET['tanggal']));
            $bulan = date('m',strtotime($_GET['tanggal']));
            $tahun = date('Y',strtotime($_GET['tanggal']));

            $query = DepartemenStok::find();
            $query->where(['<>','barang.nama_barang','-']);
            $query->andWhere(['departemen_id'=>$_GET['dept_id']]);
            $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id']]);
            // $query->andWhere(['barang.id_barang'=>514]);
            $query->andWhere(['barang.is_hapus'=>0]);
            $query->joinWith(['barang as barang']);
            $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $list = $query->all();
        }

        if(!empty($_POST['simpan']))
        {

            $tanggal = date('d',strtotime($_POST['tanggal_pilih']));
            $bulan = date('m',strtotime($_POST['tanggal_pilih']));
            $tahun = date('Y',strtotime($_POST['tanggal_pilih']));
            $tgl = date('Y-m-d',strtotime($_POST['tanggal_pilih']));


            $query = DepartemenStok::find();
            $query->where(['<>','barang.nama_barang','-']);
            $query->andWhere(['departemen_id'=>$_POST['dept_id_pilih']]);
            // $query->andWhere(['tanggal'=>$tahun.$bulan]);
             // $query->andWhere(['barang.id_barang'=>514]);
            $query->andWhere(['barang.jenis_barang_id'=>$_POST['jenis_barang_id']]);
            $query->andWhere(['barang.is_hapus'=>0]);
            $query->joinWith(['barang as barang']);
            $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $list = $query->all();

            // $date = date('Y-m-d',strtotime($_POST['tanggal_pilih']));
          
            $transaction = \Yii::$app->db->beginTransaction();
            
            try 
            {

                foreach($list as $m)
                {
                    $stok_riil = $_POST['stok_riil_'.$m->id] ?: 0;
                    $batch_no = $_POST['batch_no_'.$m->id] ?: '';
                    $exp_date = $_POST['exp_date_'.$m->id] ?: date('Y-m-d');
                    $stok = $m->stok;
                    $model = BarangOpname::find()->where([
                        'departemen_stok_id' => $m->id,
                        'bulan' => $bulan,
                        'tahun' => $tahun.$bulan
                    ])->one();


                    if(empty($model))
                        $model = new BarangOpname;

                    $model->barang_id = $m->barang_id;
                    $model->perusahaan_id = Yii::$app->user->identity->perusahaan_id;
                    $model->departemen_stok_id = $m->id;
                    $model->selisih = $m->stok - $stok_riil;
                    $model->stok = $stok_riil;
                    $model->stok_riil = $stok_riil;
                    $model->stok_lalu = $stok;
                    
                    $model->bulan = $bulan;
                    $model->tahun = $tahun.$bulan;
                    $model->tanggal = $tgl;
                    
                    $mh = DepartemenStokHistory::find()->where([
                        'dept_stok_id' => $m->id,
                        'tanggal' => $tgl
                    ])->one();

                    $mh = new DepartemenStokHistory;
                    $mh->dept_stok_id = $m->id;
                    $mh->attributes = $m->attributes;
                    $mh->created_at = date('Y-m-d H:i:s');
                    $mh->updated_at = date('Y-m-d H:i:s');
                    $mh->tanggal = $tgl;
                    if(!$mh->save())
                    {
                        $errors .= \app\helpers\MyHelper::logError($mh);
                        $model->addError('barang_id',$errors);
                    }

                    if($model->validate())
                    {
                        $model->save();
                    }
                    else{
                        
                        $errors = \app\helpers\MyHelper::logError($model);
                        // print_r($errors);exit;
                        $model->addError('barang_id',$errors);
                    }
                    
                }
                Yii::$app->session->setFlash('success', "Data tersimpan");
                $transaction->commit();
                return $this->redirect(['create']);
            } catch (\Exception $e) {
                $transaction->rollBack();
                return $this->render('create', [
                    'list' => $list,
                    'model' => $model,
                ]);
                // throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                return $this->render('create', [
                    'list' => $list,
                    'model' => $model,
                ]);
                // throw $e;
            }
        }

        else if(!empty($_POST['validate']))
        {

            $tgl = date('Y-m-d',strtotime($_POST['tanggal_pilih']));
            $tanggal = date('d',strtotime($_POST['tanggal_pilih']));
            $bulan = date('m',strtotime($_POST['tanggal_pilih']));
            $tahun = date('Y',strtotime($_POST['tanggal_pilih']));
            
            $dept_id = $_POST['dept_id_pilih'];

            $query = DepartemenStok::find();
            $query->where(['<>','barang.nama_barang','-']);
            // $query->andWhere(['tanggal'=>$tanggal]);
            $query->andWhere(['departemen_id'=>$_POST['dept_id_pilih']]);
            $query->andWhere(['barang.jenis_barang_id'=>$_POST['jenis_barang_id']]);
            $query->andWhere(['barang.is_hapus'=>0]);
            $query->joinWith(['barang as barang']);
             // $query->andWhere(['barang.id_barang'=>514]);
            $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $list = $query->all();

            // $date = date('Y-m-d',strtotime($_POST['tanggal_pilih']));

            $transaction = \Yii::$app->db->beginTransaction();
            $errors = '';
            try 
            {
                
                foreach($list as $m)
                {

                    $stok_riil = $_POST['stok_riil_'.$m->id] ?: 0;
                    $batch_no = $_POST['batch_no_'.$m->id] ?: '';
                    $exp_date = $_POST['exp_date_'.$m->id] ?: date('Y-m-d');
                    $stok = $m->stok;
                   
                    // $m->stok_bulan_lalu = $stok;
                    $m->qty = $stok_riil - $stokSblmValidasi;
                    $m->batch_no = $batch_no;
                    $m->exp_date = $exp_date;
                    if(!$m->save())
                    {
                        print_r($m->getErrors());exit;
                    }
                    

                    // if(date('Y-m-d') == date('Y-m-01'))
                    // {



                        $ks = new KartuStok;
                        $ks->barang_id = $m->barang_id;
                        $ks->qty_in = $stok_riil - $stokSblmValidasi;
                        $ks->kode_transaksi = 'STOKOPNAME_'.$tahun.$bulan.'_'.$m->id;
                        $ks->tanggal = date('Y-m-d');
                        $ks->departemen_id = $dept_id;
                        
                        $ks->stok_id = $m->id;
                        $ks->keterangan = 'Stok Opname '.$m->barang->nama_barang.' '.$tahun.' bulan '.$bulan;
                        $ks->created_at = date('Y-m-d H:i:s');//date('Y-m-01').' 00:00:01';
                        $ks->updated_at = date('Y-m-d H:i:s');//date('Y-m-01').' 00:00:01';
                        // print_r($ks->qty_in);exit;

                        $params = [
                            'barang_id' => $m->barang_id,
                            'departemen_id' => $dept_id
                        ];

                        // $prevStok = StokHistory::getPrevStok($params);

                        $sh = new StokHistory;
                        $sh->barang_id = $ks->barang_id;
                        $sh->kode_transaksi = $ks->kode_transaksi;
                        $sh->departemen_id = $ks->departemen_id;
                        $sh->qty_in = $ks->qty_in ?: 0;
                        $sh->qty_out = $ks->qty_out ?: 0;

                        // if(!empty($prevStok[1]))
                        // {
                        //     $sh->sisa = $prevStok->sisa - $sh->qty_out;
                        // }

                        $sh->sisa = $stok_riil - $stokSblmValidasi;
                        $sh->tanggal = $ks->tanggal;
                        $sh->keterangan = $ks->keterangan;
                        $sh->created_at = $ks->created_at;
                        $sh->updated_at = $ks->updated_at;
                        // $sh->sisa = $m->sisa;
                        $sh->save();

                        if($ks->validate())
                            $ks->save();
                        else{
                            
                            foreach($ks->getErrors() as $attribute){
                                foreach($attribute as $error){
                                    $errors .= $error.' ';
                                }
                            }
                                
                            // print_r($errors);exit;             
                        }

                        // switch($dept_id)
                        // {
                        //     case 1 :
                        //     case 2 :

                        //         $listStokSebelumValidasi = KartuStok::getListPenjualanSebelumValidasi($m->barang_id, $dept_id, $sd, $ed);

                        //         $s = ceil($stok_riil);
                        //         foreach($listStokSebelumValidasi as $q => $ls)
                        //         {
                        //             $s = $s - $ls->qty_out;
                        //             $ls->sisa = $s;
                        //             $ls->save();  
                        //         }
                        //     break;
                        // }
                    // }

                    
                }
                Yii::$app->session->setFlash('success', "Data tersimpan");
                $transaction->commit();
                return $this->redirect(['create']);
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                
                throw $e;
            }
        }
        // print_r(count($_POST['stok_riil']));exit;

        return $this->render('create', [
            'list' => $list,
            'model' => $model,

        ]);
    }

    /**
     * Updates an existing BarangOpname model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BarangOpname model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BarangOpname model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BarangOpname the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BarangOpname::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
