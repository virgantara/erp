<?php

namespace app\controllers;

use Yii;
use app\models\ReturBarangCustomer;
use app\models\ReturBarangCustomerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ReturBarangCustomerItem;
use app\models\ReturBarangCustomerItemSearch;
use yii\base\UserException;
use yii\httpclient\Client;
use app\models\PenjualanResep;
use app\models\PenjualanSearch;
use app\models\Penjualan;
use app\models\PenjualanItem;
use app\models\DepartemenStok;
use app\models\Departemen;

/**
 * ReturBarangCustomerController implements the CRUD actions for ReturBarangCustomer model.
 */
class ReturBarangCustomerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAjaxAddItem()
    {
        if (Yii::$app->request->isAjax) 
        {

            $errors = '';
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try 
            {
                $dataPost = $_POST['dataPost'];
                
                $model = ReturBarangCustomerItem::find()->where([
                    'uuid'=>$dataPost['uuid'],
                    'barang_id' => $dataPost['barang_id']
                ])->one();

                if(!empty($model))
                {
                    $result = [
                        'code' => 500,
                        'short' => 'danger',
                        'message' => 'Item sudah ada di retur ini',
                    ];
                    echo \yii\helpers\Json::encode($result);        
                    die();
                }

                $model = new ReturBarangCustomerItem();
                $model->attributes = $dataPost;
                $model->uuid = $dataPost['uuid'];

                // $model->tanggal = $_POST['tanggal'];

                if(!$model->save())
                {

                    $errors .= \app\helpers\MyHelper::logError($model);
                    
                    $result = [
                        'code' => 500,
                        'short' => 'danger',
                        'message' => $errors,
                    ];
                    echo \yii\helpers\Json::encode($result);        
                    die();
                }

                $transaction->commit();
                $query = ReturBarangCustomerItem::find()->where(['uuid'=>$dataPost['uuid']]);
                $items = [];
                foreach($query->all() as $item)
                {
                    $items[] = [
                        'bid' => $item->barang_id,
                        'kd_brg' => $item->barang->kode_barang,
                        'nm_brg' => $item->barang->nama_barang,
                        'hb' => $item->hb,
                        'hj'=>$item->hj,
                        'id'=> $item->id
                    ];
                }

                $result = [
                    'code' => 200,
                    'short' => 'success',
                    'message' => 'Item disimpan',
                    'items' => $items
                ];
                echo \yii\helpers\Json::encode($result);

            } catch (\UserException $e) {

                // $model->addError('id',$errors);
                // $result = [
                //     'code' => 500,
                //     'short' => 'danger',
                //     'message' => $errors
                // ];
                // echo \yii\helpers\Json::encode($result);
                $transaction->rollBack();
                
                throw $e;
            } 


        }
    }

    public function actionUnit()
    {
        $model = new ReturBarangCustomer();
        
        if($model->load(Yii::$app->request->post()))
        {
            $departemen_pemberi=Yii::$app->user->identity->departemen;
            $model->departemen_penerima_id = $_POST['ReturBarangCustomer']['departemen_penerima_id'];
            $model->uuid = $_POST['ReturBarangCustomer']['uuid'];
            $model->tanggal = $_POST['tanggal'];
            $query = ReturBarangCustomerItem::find()->where(['uuid'=>$model->uuid]);
            $departemenPenerima = Departemen::findOne($model->departemen_penerima_id);
            $departemenPemberi = Departemen::findOne($departemen_pemberi);            
            $errors = '';
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();

           
            try 
            {

                if($model->save())
                {
                    foreach($query->all() as $item)
                    {
                        $qty = $_POST['qty_'.$item->id];
                        $item->qty = $qty;
                        $item->retur_id = $model->id;
                        $item->save();    

                        DepartemenStok::minusStok($item->barang_id, $departemen_pemberi, $qty);
                        $params = [
                            'barang_id' => $item->barang_id,
                            'status' => 0,
                            'kode_transaksi' => 'RETUR_BARANG_'.$item->uuid,
                            'qty' => $qty,
                            'tanggal' => $model->tanggal,
                            'departemen_id' => $departemen_pemberi,
                            'stok_id' => 0,
                            'keterangan' => 'RETUR ke UNIT: '.$departemenPenerima->nama.' BARANG: '.$item->barang->nama_barang.' - '.$item->barang->kode_barang,
                        ];
                        
                        \app\models\KartuStok::createKartuStok($params);

                        DepartemenStok::addStok($item->barang_id, $model->departemen_penerima_id, $qty);

                        $params = [
                            'barang_id' => $item->barang_id,
                            'status' => 1,
                            'kode_transaksi' => 'RETUR_BARANG_DARI_'.$departemenPemberi->nama.' '.$model->id,
                            'qty' => $qty,
                            'tanggal' => $model->tanggal,
                            'departemen_id' => $model->departemen_penerima_id,
                            'stok_id' => 0,
                            'keterangan' => 'RETUR dari UNIT: '.$departemenPemberi->nama.' BARANG: '.$item->barang->nama_barang.' - '.$item->barang->kode_barang,
                        ];
                        
                        
                        \app\models\KartuStok::createKartuStok($params);
                        
                    }    

                    Yii::$app->session->setFlash('success', "Data tersimpan");
                }

                else
                {
                    $errors .= \app\helpers\MyHelper::logError($model);
                    

                    throw new \UserException;
                    
                }   

                $transaction->commit();

            } catch (\UserException $e) {
                Yii::$app->session->setFlash('danger', $errors);
                $transaction->rollBack();
                
                throw $e;
            } 
        }
        
        $model->uuid = \app\helpers\MyHelper::gen_uuid();
        return $this->render('unit', [
            'model' => $model,
            // 'jenis_rawat' => $jenis_rawat
        ]);
    }

    public function actionTakBertuan()
    {
        $model = new ReturBarangCustomer();
        


        if($model->load(Yii::$app->request->post()))
        {
            $model->departemen_penerima_id = Yii::$app->user->identity->departemen;
            $model->uuid = $_POST['ReturBarangCustomer']['uuid'];
            $model->unit_id = $_POST['unit_id'];
            $model->tanggal = $_POST['tanggal'];
            $model->unit_nama = $_POST['unit_nama'];
            $query = ReturBarangCustomerItem::find()->where(['uuid'=>$model->uuid]);
            
            $errors = '';
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try 
            {

                if($model->save())
                {
                    foreach($query->all() as $item)
                    {
                        $qty = $_POST['qty_'.$item->id];
                        $item->qty = $qty;
                        $item->retur_id = $model->id;
                        $item->save();    

                        DepartemenStok::addStok($item->barang_id, $model->departemen_penerima_id, $qty);

                        $params = [
                            'barang_id' => $item->barang_id,
                            'status' => 1,
                            'kode_transaksi' => 'RETUR_BARANG_'.$item->uuid,
                            'qty' => $qty,
                            'tanggal' => $model->tanggal,
                            'departemen_id' => $model->departemen_penerima_id,
                            'stok_id' => 0,
                            'keterangan' => 'RETUR dari UNIT: '.$model->unit_nama.' BARANG: '.$item->barang->nama_barang.' - '.$item->barang->kode_barang,
                        ];
                      
                        \app\models\KartuStok::createKartuStok($params);
                      
                    }    
                }

                else
                {
                    $errors .= \app\helpers\MyHelper::logError($model);
                    
                    $result = [
                        'code' => 500,
                        'short' => 'danger',
                        'message' => $errors,
                    ];
                    echo \yii\helpers\Json::encode($result);        
                    die();
                }   

                $transaction->commit();
            } catch (\UserException $e) {

                $transaction->rollBack();
                
                throw $e;
            } 
        }
        
        $model->uuid = \app\helpers\MyHelper::gen_uuid();
        return $this->render('tak_bertuan', [
            'model' => $model,
            // 'jenis_rawat' => $jenis_rawat
        ]);
    }

    public function actionUpdateResep($id)
    {
        $model = new ReturBarangCustomer();
        
        $penjualan = Penjualan::findOne($id);
        $penjualanResep = $penjualan->penjualanResep;
        $model->penjualan_id = $penjualan->id;
        $model->customer_id = $penjualanResep->pasien_id;
        $model->departemen_id = $penjualan->departemen_id;
        
        
        return $this->render('create', [
            'model' => $model,
            'penjualan' => $penjualan,
            'penjualanResep'=>$penjualanResep
            // 'jenis_rawat' => $jenis_rawat
        ]);
    }


    public function actionCreateTanpaResep()
    {
        $model = new ReturBarangCustomer();
        $penjualan = new Penjualan;
        $penjualanResep = new PenjualanResep;
        $dataProvider = null;
        $pasien = [];
        if($model->load($_GET)){
            $searchModel = new PenjualanSearch;
            $searchModel->customer_id = $model->customer_id;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $api_baseurl = Yii::$app->params['api_baseurl'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $customer_id = $_GET['customer_id'];
            $response = $client->get('/pasien/rm', ['key' => $model->customer_id])->send();
            
            $out = [];

            
            if ($response->isOk) {
                $result = $response->data['values'];

                if(!empty($result))
                {
                    $d = $result[0];    
                    $pasien = [
                        'id' => $d['NoMedrec'],
                        'label'=> $d['NAMA'].' '.$d['NoMedrec'].' '.$d['NamaUnit'].' '.date('d/m/Y',strtotime($d['TGLDAFTAR'])),
                        'nodaftar'=> $d['NODAFTAR'],
                        'alamat' => $d['ALAMAT'],
                        'jk' => $d['JENSKEL'],
                        'namapx' => $d['NAMA'],
                        'jenispx'=> $d['KodeGol'],
                        'namagol' => $d['NamaGol'],
                        'tgldaftar' => $d['TGLDAFTAR'],
                        'jamdaftar' => $d['JamDaftar'],
                        'kodeunit' => $d['KodeUnit'],
                        'id_rawat_inap' => !empty($d['id_rawat_inap']) ? $d['id_rawat_inap'] : '',
                        'namaunit' => $d['unit_tipe'] == 2 ? 'Poli '.$d['NamaUnit'] : $d['NamaUnit'],
                        'id_dokter' => !empty($d['id_dokter']) ? $d['id_dokter'] : '',
                        'nama_dokter' => !empty($d['nama_dokter']) ? $d['nama_dokter'] : '', 
                    ];
                
                }
            }

            // $model->penjualan_id = $penjualan->id;
            // $model->customer_id = $penjualanResep->pasien_id;
            // $model->departemen_id = $penjualan->departemen_id;
        }
        
        return $this->render('tanpa_resep', [
            'model' => $model,
            'pasien'=>$pasien,
            'penjualan' => $penjualan,
            'penjualanResep'=>$penjualanResep,
            'dataProvider' => $dataProvider
            // 'jenis_rawat' => $jenis_rawat
        ]);
    }


    public function actionSimpanRetur()
    {
        if(!empty($_POST['ReturBarangCustomer']['penjualan_id']))
        {

            $errors = '';
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try 
            {


                $penjualan = Penjualan::findOne($_POST['ReturBarangCustomer']['penjualan_id']);
                $model = new ReturBarangCustomer();
                $penjualanResep = $penjualan->penjualanResep;
                $model->departemen_penerima_id = Yii::$app->user->identity->departemen;
                $model->tanggal = $_POST['tanggal'];

                if ($model->load(Yii::$app->request->post())) {
                    $model->save();

                    $total = 0;
                    foreach($penjualan->penjualanItems as $item)
                    {

                        $ds = $item->stok;
                        $jml = !empty($_POST['jumlah_'.$item->id]) ? $_POST['jumlah_'.$item->id] : 0;
                               
                        if($jml == 0) continue;

                        $sisa = $item->qty - $jml;
                        $sisa_bulat = $item->qty - $jml;

                        $item->qty = $sisa;
                        $item->qty_bulat = $sisa_bulat;
                        $item->save();

                        

                        $qty = $item->qty < 1 ? $item->qty : ceil($item->qty);
                        $total += $qty * round($item->harga);

                         
                        $m = new ReturBarangCustomerItem;
                        $m->retur_id = $model->id;
                        $m->barang_id = $item->stok->barang_id;
                        $m->qty = $jml;
                        $m->hb = $item->harga_beli;
                        $m->hj = $item->harga;

                        if($m->validate()){
                            $m->save();
                            DepartemenStok::addStok($m->barang_id, $model->departemen_penerima_id, $jml);
                             $params = [
                                'barang_id' => $m->barang_id,
                                'status' => 1,
                                'kode_transaksi' => 'RETUR_BARANG_PX_'.$model->id.$m->id,
                                'qty' => $jml,
                                'tanggal' => $model->tanggal,
                                'departemen_id' => $model->departemen_penerima_id,
                                'stok_id' => $item->stok_id,
                                'keterangan' => 'RETUR BARANG PASIEN KODE RESEP:'.$penjualan->kode_penjualan,
                            ];
                            
                            \app\models\KartuStok::createKartuStok($params);
                
                        }

                        else{
                            $errors = \app\helpers\MyHelper::logError($m);

                            return $this->redirect(['create']);
                
                            
                        }

                    }


                    $billingModule = \Yii::$app->getModule('billing');
                    
                    $params = [
                        'nama' => $penjualanResep->pasien_nama,
                        'kode_trx' => $penjualan->kode_penjualan,
                        'trx_date' => date('Ymdhis'),
                        'jenis_tagihan' => 'OBAT',
                        'person_in_charge' => $penjualanResep->dokter_nama,
                        'custid' => $penjualan->customer_id,
                        'issued_by' => Yii::$app->user->identity->departemenNama,
                        'keterangan' => 'Tagihan Resep : '.$penjualan->kode_penjualan,
                        'nilai' => $total,
                        'origin' => 'integra',
                        'jenis_customer' => $penjualanResep->pasien_jenis,
                        'status_bayar' => $penjualan->status_penjualan
                    ];

                    $hasil= $billingModule->updateTagihan($params);
                    if($penjualanResep->jenis_rawat == '2'){
                        $params = [
                            'id_rawat_inap' => $penjualanResep->rawat_inap_id,
                            'id_dokter' => $penjualanResep->dokter_id,
                            'kode_alkes' => 'OBAT',
                            'nilai' => $total,
                            'keterangan' => $penjualan->kode_penjualan
                        ];    

                        if(empty($params['id_rawat_inap'])){
                            throw new \Exception("ID Rawat Inap Kosong");
                            
                        }

                        $log = \app\helpers\MyHelper::ajaxSyncObatInap($params);

                    
                    }
                    
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Data tersimpan");
                    return $this->redirect(['create']);
                }


            } catch (\UserException $e) {

                $model->addError('id',$errors);
                
                Yii::$app->session->setFlash('danger', $errors);
                $transaction->rollBack();
                return $this->redirect(['create']);
            } catch (\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('danger', $errors);
                return $this->redirect(['create']);
            }

        }

    }

 

    /**
     * Lists all ReturBarangCustomer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReturBarangCustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ReturBarangCustomer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $returItem = new ReturBarangCustomerItemSearch;
        $returItem->retur_id = $id;
        $dataProvider = $returItem->search(Yii::$app->request->queryParams);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'searchModel'=>$returItem
        ]);
    }

    /**
     * Creates a new ReturBarangCustomer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReturBarangCustomer();
        $penjualan = new Penjualan;
        $penjualanResep = new PenjualanResep;
        if($penjualan->load(Yii::$app->request->post())){

            $penjualan = Penjualan::find()->where(['kode_penjualan'=>$penjualan->kode_penjualan])->one();
            $penjualanResep = $penjualan->penjualanResep;
            $model->penjualan_id = $penjualan->id;
            $model->customer_id = $penjualanResep->pasien_id;
            $model->departemen_id = $penjualan->departemen_id;
        }
        
        return $this->render('create', [
            'model' => $model,
            'penjualan' => $penjualan,
            'penjualanResep'=>$penjualanResep
            // 'jenis_rawat' => $jenis_rawat
        ]);
    }

    /**
     * Updates an existing ReturBarangCustomer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $penjualan = $model->penjualan;
        $penjualanResep = $penjualan->penjualanResep;
        
        return $this->render('update', [
            'model' => $model,
            'penjualan' => $penjualan,
            'penjualanResep'=>$penjualanResep
            // 'jenis_rawat' => $jenis_rawat
        ]);
    }

    /**
     * Deletes an existing ReturBarangCustomer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReturBarangCustomer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReturBarangCustomer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReturBarangCustomer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
