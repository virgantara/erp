<?php

namespace app\controllers;

use Yii;
use app\models\DepartemenStok;
use app\models\DepartemenStokSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use app\models\DepartemenStokHistory;
use app\models\BarangOpname;
use app\models\KartuStok;
use app\models\StokHistory;
use app\models\SalesMasterBarang;
use yii\httpclient\Client;


// use yii\base\Exception;
/**
 * PerusahaanSubStokController implements the CRUD actions for PerusahaanSubStok model.
 */
class DepartemenStokController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'restore' => ['POST'],
                ],
            ],
        ];
    }

    public function actionFixStokUnit($unit)
    {
        
        $barang = SalesMasterBarang::find()->all();

        foreach($barang as $b)
        {
            $ks = StokHistory::find()->where([
                'departemen_id' => $unit,
                'barang_id' => $b->id_barang
            ]);

            $ks->orderBy(['id'=>SORT_DESC]);
            $ks = $ks->one();

            $stok = DepartemenStok::find()->where([
                'departemen_id' => $unit,
                'barang_id' => $b->id_barang
            ])->one();

            if(!empty($ks))
            {
                echo $b->id_barang.' '.$b->kode_barang.' '.$b->nama_barang.' : ';
                echo 'Sisa: '.$ks->sisa;
                echo ' x Stok: ';
                echo !empty($stok) ? $stok->stok : '';
                echo '<br>';

                if(!empty($stok))
                {
                    $stok->stok = $ks->sisa;
                    $stok->save(false,['stok']);
                }

                   
                
                    
            }

        }

        die();
    }

    public function actionSyncObatMaster()
    {
       
        $transaction = \Yii::$app->db->beginTransaction();
        $errors = '';
        try 
        {
            $listDept = \app\models\Departemen::find()->all();
            foreach($listDept as $dept)
            {
                $query = DepartemenStok::find()->where([
                    'departemen_id' => $dept->id
                ]);

                $listBarang = $query->all();
                
                $list = [];

                foreach($listBarang as $item)
                    $list[] = $item->barang_id;

                // $query = new \yii\db\Query;
                $query = \app\models\SalesMasterBarang::find()
                    ->where(['NOT IN','id_barang',$list])
                    ->orderBy('nama_barang');
                
                $data = $query->all();
                $out = [];
               
                foreach ($data as $d) 
                {
                    if($d->nama_barang == '-') continue;

                    $model = new DepartemenStok();
                    $model->scenario = 'insert';
                    $model->tanggal = date('Y-m-d');
                    $model->stok = 0;
                    $model->stok_minimal = 50;
                    $model->stok_maksimal = 1000;
                    $model->barang_id = $d->id_barang;
                    $model->departemen_id = $dept->id;
                    if(!$model->save())
                    {
                        $errors = \app\helpers\MyHelper::logError($model);
                        throw new \Exception($errors,1);
                        
                    }
                }

                   
               
            }

            $transaction->commit();
        } catch (\Exception $e) {
            // print_r($e);
            $transaction->rollBack();
            print_r($errors);
            // exit;
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            print_r($errors);
            // exit;    
            throw $e;
        }
    }

    public function actionAjaxBarangNotInDepartemen($term = null, $dept_id) {

        if (Yii::$app->request->isAjax) {
            
            // $query = DepartemenStok::find()->where([
            //     'departemen_id' => $dept_id
            // ]);

            // $listBarang = $query->all();
            
            // $list = [];

            // foreach($listBarang as $item)
            //     $list[] = $item->barang_id;

            $query = new \yii\db\Query;
            


            $query->select('b.kode_barang, b.id_barang, b.nama_barang, b.id_satuan as satuan, b.harga_jual, b.harga_beli')
                ->from('erp_sales_master_barang b')
                // ->join('erp_departemen_stok ds',)
                ->where(['LIKE','nama_barang',$term])//'(nama_barang LIKE "%' . $term .'%" OR kode_barang LIKE "%' . $term .'%")')
                // ->andWhere(['NOT IN','id_barang',$list])
                ->orderBy('nama_barang')
                // ->groupBy(['id_barang'])
                ->limit(10);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out = [];
            foreach ($data as $d) {
                if($d['nama_barang'] == '-') continue;
                $out[] = [
                    // 'id_stok' => $d['id'],
                    'id' => $d['id_barang'],
                    'kode' => $d['kode_barang'],
                    'nama' => $d['nama_barang'],
                    // 'dept_stok_id' => $d['id'],
                    'satuan' => $d['satuan'],
                    // 'stok' => $d['stok'],
                    // 'kekuatan' => $d['kekuatan'],
                    'harga_jual' => $d['harga_jual'],
                    'harga_beli' => $d['harga_beli'],
                    'label'=> $d['nama_barang'].' - '.$d['kode_barang']
                ];
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
            \Yii::$app->response->data = $out;
            \Yii::$app->end();
        }
    }

    public function actionJualItem()
    {
        $list = DepartemenStok::find()->all();

        foreach($list as $m)
        {
            $listro = \app\models\RequestOrderItem::find()->where(['stok_id'=>$m->id])->all();

            foreach($listro as $r)
            {
                $r->barang_id = $m->barang_id;
                $r->departemen_id = $m->departemen_id;
                $r->save();
            }
        }
    }

    public function actionRoStok()
    {
        $list = \app\models\RequestOrder::find()->where(['is_hapus'=>2])->all();

        foreach($list as $m)
        {
            $dept_id = $m->departemen_id;
            
            foreach($m->requestOrderItems as $r)
            {

                
                $r->barang_id = $r->item_id;
                $r->departemen_id = $dept_id;
                $r->save();
            }
        }


    }

    public function actionListStok($kode)
    {
        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);

        $dept_id = Yii::$app->user->identity->departemen;

        $response = $client->get('/integra/unit/liststok', [
            'dept_id'=>$dept_id,
            'kode' => $kode
        ])->send();

        $result = [];
        // print_r($response);exit;
        if ($response->isOk) {
            $result = $response->data['values'];  

        }

        return $this->render('list_stok', [
            'result' => $result,
        ]);
    }

    public function actionRemoveDuplicate()
    {
        if(!empty($_POST['simpan']))
        {
            $dept_id = $_POST['dept_id_pilih'];
            
            $query = DepartemenStok::find()->alias('ds');
            $query->where(['<>','barang.nama_barang','-']);
            $query->andWhere(['ds.departemen_id'=>$dept_id]);
            // $query->andWhere(['departemen_id'=>Yii::$app->user->identity->departemen]);
            // $query->andWhere(['tahun'=>$tahun.$bulan]);
            // $query->andWhere(['barang.jenis_barang_id'=>$_POST['jenis_barang_id_pilih']]);
            // $query->andWhere(['barang.id_barang'=>288]);
            $query->andWhere(['barang.is_hapus'=>0]);
            $query->joinWith(['barang as barang']);
            $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $list = $query->all();   
            $transaction = \Yii::$app->db->beginTransaction();
            $errors = '';
            try 
            {
                
                $listItem = [];
                foreach($list as $item)
                {
                    if(!in_array($item->barang_id, $listItem))
                    {
                        $listItem[] = $item->barang_id;

                    }

                    else{
                        $item->delete();
                    }
                }

                $this->redirect(['departemen-stok/repair']);
                $transaction->commit();
            } catch (\Exception $e) {
                print_r($e);
                $transaction->rollBack();
                print_r($errors);
                // exit;
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                print_r($errors);
                // exit;    
                throw $e;
            }
        }
    }

    public function actionRepair()
    {
        $list = [];

        if(!empty($_GET['cari']))
        {
            $dept_id = $_GET['dept_id'];
            
            $query = DepartemenStok::find()->alias('ds');
            $query->where(['<>','barang.nama_barang','-']);
            $query->andWhere(['ds.departemen_id'=>$dept_id]);
            // $query->andWhere(['departemen_id'=>Yii::$app->user->identity->departemen]);
            // $query->andWhere(['tahun'=>$tahun.$bulan]);
            $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id']]);
            // $query->andWhere(['barang.id_barang'=>288]);
            $query->andWhere(['barang.is_hapus'=>0]);
            $query->joinWith(['barang as barang']);
            $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $list = $query->all();   
        }



        return $this->render('repair', [
            'list' => $list,
            'model' => $model,
        ]);
    }

    public function actionAjaxRemoveUnitStok()
    {
        if (Yii::$app->request->isAjax) 
        {

            $transaction = \Yii::$app->db->beginTransaction();
            try 
            {

                $id = $_POST['dataPost'];
                $model = DepartemenStok::findOne($id);

                $model->delete();

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                
                throw $e;
            }

        }   
    }


    public function actionAjaxRemoveStok()
    {
        if (Yii::$app->request->isAjax) 
        {

            $transaction = \Yii::$app->db->beginTransaction();
            try 
            {

                $id = $_POST['dataPost'];
                $model = BarangOpname::findOne($id);

                $model->delete();

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                
                throw $e;
            }

        }   
    }

    public function actionAjaxMerge()
    {
        if (Yii::$app->request->isAjax) 
        {

            $transaction = \Yii::$app->db->beginTransaction();
            try 
            {

                $id = $_POST['dataPost'];
                $model = BarangOpname::findOne($id);

                $query = BarangOpname::find();
                $query->joinWith(['departemenStok as ds']);
                $query->where([
                    'barang_id'=>$model->barang_id,
                    'ds.departemen_id' => $model->departemen_id
                ]);
                $total_stok = $query->sum('stok');

                $model->stok = $total_stok;
                $model->save();

                $query = BarangOpname::find();
                $query->joinWith(['departemenStok as ds']);
                $query->where([
                    'barang_id'=>$model->barang_id,
                    'ds.departemen_id' => $model->departemen_id
                ]);                                
                $listBarang = $query->all();
                $model->stok = $total_stok;

                foreach($listBarang as $ds)
                {
                    if($ds->id != $id)
                        $ds->delete();

                }

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                
                throw $e;
            }

        }   
    }

    public function actionFixStokAwalBulan(){
        if(!empty($_GET['simpan']))
        {
            $dept_id = $_GET['dept_id_pilih'];
            $tgl = $_GET['tahun_pilih'].'-'.$_GET['bulan_pilih'].'-01';
            $prevMonth = date('Y-m',strtotime('-1 month',strtotime($tgl)));
            $tahunbulan = $prevMonth;

            $query = \app\models\DepartemenStok::find();
            $query->where(['<>','barang.nama_barang','-']);
            $query->andWhere(['departemen_id'=>$dept_id]);
            $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id_pilih']]);
            // $query->andWhere(['departemen_id'=>Yii::$app->user->identity->departemen]);
            $query->andWhere(['barang.is_hapus'=>0]);
            $query->joinWith(['barang as barang']);
            $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $listBarang = $query->all();

            $counter = 0;
            $nowMonth = $_GET['tahun_pilih'].'-'.$_GET['bulan_pilih'];
            // $prevMonth = date('Y-m-d',strtotime('-1 month',strtotime($tgl)));
            $transaction = \Yii::$app->db->beginTransaction();
            $errors = '';
            try 
            {
                foreach($listBarang as $q => $b)
                {

                   
                    // $query = \app\models\StokHistory::find()->alias('bo');
                    // $query->where(['<>','barang.nama_barang','-']);
                    // $query->andWhere(['bo.departemen_id'=>$dept_id]);
                    // $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id_pilih']]);
                    // // $query->andWhere(['created_at'=>$tahunbulan]);
                    // $query->andFilterWhere(['between', 'bo.created_at', $nowMonth.'-01 00:00:00', $nowMonth.'-31 23:59:59']);
                    // $query->andWhere(['barang.is_hapus'=>0,'bo.barang_id'=>$b->barang_id]);
                    // $query->joinWith(['barang as barang']);
                    // // $query->limit(1)
                    // $query->orderBy(['bo.created_at'=>SORT_ASC]);
                    // // $tgl = $_GET['tahun'].'-'.$_GET['bulan'].'-01';
                    // // $nextMonth = date('m',strtotime('+1 month',strtotime($tgl)));
                    // // $sd = $_GET['tahun'].'-'.$_GET['bulan'].'-01';
                    // // $ed = date('Y-m-t',strtotime($sd));
                    // // print_r($sd);exit;
                    // $list = $query->all();

                    // if(count($list) == 0) continue;
                
                    // $counter ++;

                    $stok_awal_bulan = \app\models\StokHistory::getLastMonthStok($b->barang_id, $dept_id,$prevMonth);
                
                   
                    $ksawal = new StokHistory;
                    $ksawal->barang_id = $b->barang_id;
                    $ksawal->departemen_id = $dept_id;
                    $ksawal->keterangan = 'STOK AWAL: '.$b->barang->nama_barang;
                    $ksawal->qty_in = $stok_awal_bulan->sisa;
                    $ksawal->qty_out = 0;
                    $ksawal->sisa = $stok_awal_bulan->sisa;    
                    $ksawal->created_at = $nowMonth.'-01 00:00:01';
                    $ksawal->updated_at = $nowMonth.'-01 00:00:01';
                    // print_r($stok_awal_bulan);exit;
                    // $nextMonth = date('m',strtotime('+1 month',strtotime($tgl)));
                    $ksawal->tanggal = $nowMonth.'-01';//$_POST['tahun_pilih'].'-'.$nextMonth.'-'.date('01');
                    $ksawal->save();       

                    if(!$ksawal->save())
                    {
                        $errors .= \app\helpers\MyHelper::logError($ksawal);
                        print_r($errors);exit;
                        throw new \Exception;
                        
                    }
                }
                $this->redirect(['departemen-stok/merge-stok','dept_id'=>$dept_id,'jenis_barang_id'=>$_GET['jenis_barang_id_pilih'],'bulan'=>$_GET['bulan_pilih'],'tahun'=>$_GET['tahun_pilih']]);

                    
                // }

                $transaction->commit();
            } catch (\Exception $e) {
                // print_r($e);
                $transaction->rollBack();
                print_r($errors);
                // exit;
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                print_r($errors);
                // exit;    
                throw $e;
            }
        }
    }

    public function actionFixKartustok()
    {

        if(!empty($_GET['simpan']))
        {
            $dept_id = $_GET['dept_id_pilih'];
            $tgl = $_GET['tahun_pilih'].'-'.$_GET['bulan_pilih'].'-01';
            $prevMonth = date('Ym',strtotime('-1 month',strtotime($tgl)));
            $tahunbulan = $prevMonth;

            

            $stokAwalBulan = $query->one();

            $query = KartuStok::find()->alias('bo');
            $query->where(['<>','barang.nama_barang','-']);
            $query->andWhere(['ds.departemen_id'=>$dept_id]);
            $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id_pilih']]);
            // $query->andWhere(['created_at'=>$tahunbulan]);
            $query->andFilterWhere(['between', 'bo.created_at', '2019-11-01 00:00:00', '2019-11-30 23:59:59']);
            $query->andWhere(['barang.is_hapus'=>0]);
            $query->joinWith(['barang as barang','departemenStok as ds']);
            
            $query->orderBy(['bo.created_at'=>SORT_ASC]);
            // $query->limit(1);
            // $query = BarangOpname::find()->alias('bo');
            // $query->where(['<>','barang.nama_barang','-']);
            // $query->andWhere(['ds.departemen_id'=>$dept_id]);
            // // $query->andWhere(['departemen_id'=>Yii::$app->user->identity->departemen]);
            // // $query->andWhere(['tahun'=>$tahun.$bulan]);
            // $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id_pilih']]);
            // // $query->andWhere(['barang.id_barang'=>288]);
            // $query->andWhere(['bo.tahun'=>$tahunbulan]);
            // $query->andWhere(['barang.is_hapus'=>0]);
            // $query->joinWith(['barang as barang','departemenStok as ds']);
            // $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            $list = $query->all();
            $transaction = \Yii::$app->db->beginTransaction();
            $errors = '';
            try 
            {
                $sd = $_GET['tahun_pilih'].'-'.$_GET['bulan_pilih'].'-01';
                $ed = date('Y-m-t',strtotime($sd));
                    
                foreach($list as $b)
                {
                    

                    $ksawal = new StokHistory;
                    $ksawal->barang_id = $b->barang_id;
                    $ksawal->departemen_id = $dept_id;
                    $ksawal->keterangan = 'STOK AWAL: '.$b->barang->nama_barang;
                    $ksawal->qty_in = $b->stok;
                    $ksawal->qty_out = 0;
                    $ksawal->sisa = $b->stok;    
                    $ksawal->created_at = $sd.' 00:00:01';
                    $ksawal->updated_at = $sd.' 00:00:01';

                    // $nextMonth = date('m',strtotime('+1 month',strtotime($tgl)));
                    $ksawal->tanggal = $tgl;//$_POST['tahun_pilih'].'-'.$nextMonth.'-'.date('01');
                    $ksawal->save();       

                    if(!$ksawal->save())
                    {
                        $errors .= \app\helpers\MyHelper::logError($ksawal);
                        print_r($errors);exit;
                        throw new \Exception;
                        
                    }


                    $query = KartuStok::find()->where([
                        'barang_id' => $b->barang_id,
                        'departemen_id' => $dept_id
                    ]);
                    $query->andFilterWhere(['between', 'tanggal', $sd, $ed]);
                    
                    $query->orderBy(['id'=>SORT_ASC]);

                    $listKartuStok = $query->all();

                    $sisa = $b->stok;
                    foreach($listKartuStok as $ks)
                    {
                        $qty_in = $ks->qty_in;//$_POST['qtyin_'.$b->id.'_'.$ks->id];
                        $qty_out = $ks->qty_out;//$_POST['qtyout_'.$b->id.'_'.$ks->id];
                        $sisa += $qty_in;//$_POST['sisa_'.$b->id.'_'.$ks->id];
                        $sisa -= $qty_out;
                        $k = new StokHistory;
                        $k->created_at = $ks->created_at;
                        $k->updated_at = $ks->updated_at;
                        // $k->id = $ks->id;
                        $k->barang_id = $b->barang_id;
                        $k->departemen_id = $dept_id;
                        $k->keterangan = $ks->keterangan;
                        $k->qty_in = $ks->qty_in;
                        $k->qty_out = $ks->qty_out;
                        $k->sisa = $sisa;    
                        $k->tanggal = $ks->tanggal;
                        $k->save();

                        if($b->barang_id == 288)
                        {
                            // print_r($_POST);exit;
                        }

                        if(!$k->save())
                        {
                            $errors .= \app\helpers\MyHelper::logError($k);
                            throw new \Exception;
                            
                        }

                        // else{
                        //     $ks->delete();
                        // }
                        // $ks->delete();
                    }

                    $this->redirect(['departemen-stok/merge-stok','dept_id'=>$dept_id,'jenis_barang_id'=>$_GET['jenis_barang_id_pilih'],'bulan'=>$_GET['bulan_pilih'],'tahun'=>$_GET['tahun_pilih']]);
                    // $ksawal = new StokHistory;
                    // $ksawal->barang_id = $b->barang_id;
                    // $ksawal->departemen_id = $dept_id;
                    // $ksawal->keterangan = 'STOK AWAL: '.$b->barang->nama_barang;
                    // $ksawal->qty_in = $b->stok;
                    // $ksawal->qty_out = 0;
                    // $ksawal->sisa = $b->stok;    
                    // $ksawal->created_at = $sd.' 00:00:01';
                    // $ksawal->updated_at = $sd.' 00:00:01';
                    // $tgl = $_POST['tahun_pilih'].'-'.$_POST['bulan_pilih'].'-01';
                    // // $nextMonth = date('m',strtotime('+1 month',strtotime($tgl)));
                    // $ksawal->tanggal = $tgl;//$_POST['tahun_pilih'].'-'.$nextMonth.'-'.date('01');
                    // $ksawal->save();       
        
                    //     // if($b->barang_id == 5)
                    //     // {
                    //     //     print_r($k->attributes);exit;
                    //     // }

                    // if(!$ksawal->save())
                    // {
                    //     $errors .= \app\helpers\MyHelper::logError($ksawal);
                    //     print_r($errors);exit;
                    //     throw new Exception;
                        
                    // }

                    
                }

                $transaction->commit();
            } catch (\Exception $e) {
                // print_r($e);
                $transaction->rollBack();
                print_r($errors);
                // exit;
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                print_r($errors);
                // exit;    
                throw $e;
            }
        }
    }

    public function actionMergeStok()
    {
        $list = [];
        $listKartuStok = [];
        $prevMonth = '';
        $nowMonth = '';
        if(!empty($_GET['cari']))
        {
            $dept_id = $_GET['dept_id'];
            $tgl = $_GET['tahun'].'-'.$_GET['bulan'].'-01';
            $nowMonth = $_GET['tahun'].'-'.$_GET['bulan'];
            // $prevMonth = date('Y-m-d',strtotime('-1 month',strtotime($tgl)));
            $prevMonth = date('Y-m',strtotime('-1 month',strtotime($tgl)));
            $tahunbulan = $prevMonth;
           
            // $query = BarangOpname::find()->alias('bo');
            // $query->where(['<>','barang.nama_barang','-']);
            // $query->andWhere(['ds.departemen_id'=>$dept_id]);
            // // $query->andWhere(['departemen_id'=>Yii::$app->user->identity->departemen]);
            // // $query->andWhere(['tahun'=>$tahun.$bulan]);
            // $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id']]);
            // // $query->andWhere(['barang.id_barang'=>288]);
            // $query->andWhere(['bo.tahun'=>$tahunbulan]);
            // $query->andWhere(['barang.is_hapus'=>0]);
            // $query->joinWith(['barang as barang','departemenStok as ds']);
            // $query->orderBy(['barang.nama_barang'=>SORT_ASC]);
            // $query = StokHistory::find()->alias('bo');
            // $query->where(['<>','barang.nama_barang','-']);
            // $query->andWhere(['bo.departemen_id'=>$dept_id]);
            // $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id']]);
            // // $query->andWhere(['created_at'=>$tahunbulan]);
            // $query->andFilterWhere(['between', 'bo.created_at', $nowMonth.'-01 00:00:00', $nowMonth.'-31 23:59:59']);
            // $query->andWhere(['barang.is_hapus'=>0]);
            // $query->joinWith(['barang as barang']);
            // // $query->limit(1)
            // $query->orderBy(['bo.created_at'=>SORT_ASC]);
            // // $tgl = $_GET['tahun'].'-'.$_GET['bulan'].'-01';
            // // $nextMonth = date('m',strtotime('+1 month',strtotime($tgl)));
            // $sd = $_GET['tahun'].'-'.$_GET['bulan'].'-01';
            // $ed = date('Y-m-t',strtotime($sd));
            // // print_r($sd);exit;
            // $list = $query->all();
            // print($list);exit;


            // foreach($list as $b)
                
            //     $query = KartuStok::find()->where([
            //         'barang_id' => $b->barang_id,
            //         'departemen_id' => $dept_id
            //     ]);
            //     $query->andFilterWhere(['between', 'tanggal', $sd, $ed]);
            //     // $query->orderBy(['tanggal'=>SORT_ASC]);
                
            //     $query->orderBy(['id'=>SORT_ASC]);
            //     $listKartuStok[$b->barang_id] = $query->all();
            // }
        }



        return $this->render('fix_stok', [
            'list' => $list,
            'prevMonth'=>$prevMonth,
            'nowMonth' => $nowMonth,
            'model' => $model,
            'listKartuStok' => $listKartuStok
        ]);
    }

    public function actionInputStokAwalBulan()
    {
        $models = DepartemenStok::find()->all();
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $errors = '';
        try 
        {
            $listBulan = ['04','07','10','01'];

            $bln = date('m');
            if(!in_array($bln, $listBulan))
            {
                foreach($models as $m)
                {
                    $m->stok_awal = $m->stok;
                    if(!$m->save())
                    {
                        $errors .= \app\helpers\MyHelper::logError($m);
                    }

                    $mh = new DepartemenStokHistory;
                    $mh->dept_stok_id = $m->id;
                    $mh->attributes = $m->attributes;
                    $mh->created_at = null;
                    $mh->updated_at = null;
                    $mh->tanggal = date('Y-m-d',strtotime('-1 day',strtotime(date('Y-m-d'))));
                    if(!$mh->save())
                    {
                        $errors .= \app\helpers\MyHelper::logError($mh);
                    }

                    $params = [
                        'barang_id' => $m->barang_id,
                        'status' => 1,
                        'kode_transaksi' => 'STOKAWAL_'.$m->id,
                        'qty' => $m->stok,
                        'tanggal' => date('Y-m-d'),
                        'departemen_id' => $m->departemen_id,
                        'stok_id' => $m->id,
                        'keterangan' => 'Stok Awal '.$m->namaBarang.' '.date('Y').' bulan '.date('m'),
                    ];
                    
                    \app\models\KartuStok::createKartuStok($params);
                    
                }
            }

            

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            print_r($errors);
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            print_r($errors);
            throw $e;
        }
    }



    public function actionAjaxDepartemenBarang() {

        if (Yii::$app->request->isAjax) {
            $query = new \yii\db\Query;
            $barang_id = $_POST['barang_id'];
            $query->select('ds.id, (SELECT sisa FROM erp_stok_history WHERE departemen_id = ds.departemen_id AND barang_id = '.$barang_id.' ORDER BY id DESC LIMIT 1) as stok, od.kekuatan, ds.barang_id, ds.departemen_id, b.harga_beli, b.harga_jual, b.nama_barang as nm,b.kode_barang as kd')
                ->from('erp_departemen_stok ds')
                ->join('JOIN','erp_sales_master_barang b','ds.barang_id=b.id_barang')
                ->join('LEFT JOIN','erp_obat_detil od','ds.barang_id=od.barang_id')
                ->where(['ds.departemen_id'=>Yii::$app->user->identity->departemen,'ds.barang_id' => $barang_id,'ds.is_hapus'=>0])
                ->orderBy(['exp_date'=>SORT_ASC])
                ->limit(1);
            $command = $query->createCommand();
            $data = $command->queryOne();
            $out = [
                'barang_id' => $data['barang_id'],
                'departemen_id' => $data['departemen_id'],
                'dept_stok_id' => $data['id'],
                'stok' => $data['stok'],
                'hb' => $data['harga_beli'],
                'nm' => $data['nm'],
                'kd' => $data['kd'],
                'hj' => $data['harga_jual'],
                'kekuatan' => $data['kekuatan']
            ];

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
            \Yii::$app->response->data = $out;
            \Yii::$app->end();
        }
    }

    public function actionAjaxBarangDepartemen($term = null, $dept_id) {

        if (Yii::$app->request->isAjax) {
            $query = new \yii\db\Query;
        
            $query->select('b.kode_barang , ds.id, b.id_barang, b.nama_barang, b.id_satuan as satuan, (SELECT sisa FROM erp_stok_history WHERE departemen_id = ds.departemen_id AND barang_id = b.id_barang ORDER BY id DESC LIMIT 1) as stok, b.harga_jual, b.harga_beli')
                ->from('erp_departemen_stok ds')
                ->join('JOIN','erp_sales_master_barang b','b.id_barang=ds.barang_id')
                // ->join('JOIN','erp_obat_detil od','b.id_barang=od.barang_id')
                // ->join('JOIN','erp_departemen_user du','du.departemen_id=ds.departemen_id')
                ->where([
                    // 'du.user_id'=>Yii::$app->user->identity->id,
                    'ds.departemen_id'=>$dept_id,
                    'b.is_hapus'=>0,
                    // 'ds.is_hapus'=>0
                ])
                ->andWhere('(nama_barang LIKE "%' . $term .'%" OR kode_barang LIKE "%' . $term .'%")')
                // ->andWhere(['>','stok',0])
                ->orderBy('nama_barang')
                // ->groupBy(['id_barang'])
                ->limit(10);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out = [];
            foreach ($data as $d) {
                if($d['nama_barang'] == '-') continue;
                $out[] = [
                    'id_stok' => $d['id'],
                    'id' => $d['id_barang'],
                    'kode' => $d['kode_barang'],
                    'nama' => $d['nama_barang'],
                    // 'dept_stok_id' => $d['id'],
                    'satuan' => $d['satuan'],
                    'stok' => $d['stok'],
                    // 'kekuatan' => $d['kekuatan'],
                    'harga_jual' => $d['harga_jual'],
                    'harga_beli' => $d['harga_beli'],
                    'label'=> $d['nama_barang'].' - '.$d['kode_barang']
                ];
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
            \Yii::$app->response->data = $out;
            \Yii::$app->end();
        }
    }

    public function actionAjaxBarangJual($term = null) {

        if (Yii::$app->request->isAjax) {
            $query = new \yii\db\Query;
        
            $query->select('b.kode_barang , b.id_barang, b.nama_barang, b.id_satuan as satuan, (SELECT sisa FROM erp_stok_history WHERE departemen_id = ds.departemen_id AND barang_id = b.id_barang ORDER BY id DESC LIMIT 1) as stok, b.harga_jual, b.harga_beli, ds.departemen_id')
                ->from('erp_departemen_stok ds')
                ->join('JOIN','erp_sales_master_barang b','b.id_barang=ds.barang_id')
                // ->join('JOIN','erp_obat_detil od','b.id_barang=od.barang_id')
                ->join('JOIN','erp_departemen_user du','du.departemen_id=ds.departemen_id')
                ->where([
                    'du.user_id'=>Yii::$app->user->identity->id,
                    'ds.departemen_id'=>Yii::$app->user->identity->departemen,
                    'b.is_hapus'=>0,
                    // 'ds.is_hapus'=>0
                ])
                ->andWhere('(nama_barang LIKE "%' . $term .'%" OR kode_barang LIKE "%' . $term .'%")')
                // ->andWhere(['>','stok',0])
                ->orderBy('nama_barang')
                // ->groupBy(['id_barang'])
                ->limit(10);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out = [];
            foreach ($data as $d) {
                if($d['nama_barang'] == '-') continue;
                $out[] = [
                    'id' => $d['id_barang'],
                    'kode' => $d['kode_barang'],
                    'nama' => $d['nama_barang'],
                    'departemen_id' => $d['departemen_id'],
                    'satuan' => $d['satuan'],
                    'stok' => $d['stok'],
                    // 'kekuatan' => $d['kekuatan'],
                    'harga_jual' => $d['harga_jual'],
                    'harga_beli' => $d['harga_beli'],
                    'label'=> $d['nama_barang'].' - '.$d['kode_barang']
                ];
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
            \Yii::$app->response->data = $out;
            \Yii::$app->end();
        }
    }

    public function actionAjaxBarang($term = null) {

        if (Yii::$app->request->isAjax) {
            $query = new \yii\db\Query;
        
            $query->select('b.kode_barang , b.id_barang, b.nama_barang, b.id_satuan as satuan, (SELECT sisa FROM erp_stok_history WHERE departemen_id = ds.departemen_id AND barang_id = b.id_barang ORDER BY id DESC LIMIT 1) as stok, b.harga_jual, b.harga_beli')
                ->from('erp_departemen_stok ds')
                ->join('JOIN','erp_sales_master_barang b','b.id_barang=ds.barang_id')
                // ->join('JOIN','erp_obat_detil od','b.id_barang=od.barang_id')
                ->join('JOIN','erp_departemen_user du','du.departemen_id=ds.departemen_id')
                ->where([
                    'du.user_id'=>Yii::$app->user->identity->id,
                    'ds.departemen_id'=>Yii::$app->user->identity->departemen,
                    'b.is_hapus'=>0,
                    // 'ds.is_hapus'=>0
                ])
                ->andWhere('(nama_barang LIKE "%' . $term .'%" OR kode_barang LIKE "%' . $term .'%")')
                // ->andWhere(['>','stok',0])
                ->orderBy('nama_barang')
                // ->groupBy(['id_barang'])
                ->limit(10);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out = [];
            foreach ($data as $d) {
                if($d['nama_barang'] == '-') continue;
                $out[] = [
                    'id' => $d['id_barang'],
                    'kode' => $d['kode_barang'],
                    'nama' => $d['nama_barang'],
                    // 'dept_stok_id' => $d['id'],
                    'satuan' => $d['satuan'],
                    'stok' => $d['stok'],
                    // 'kekuatan' => $d['kekuatan'],
                    'harga_jual' => $d['harga_jual'],
                    'harga_beli' => $d['harga_beli'],
                    'label'=> $d['nama_barang'].' - '.$d['kode_barang'].' - Stok: '.$d['stok']
                ];
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
            \Yii::$app->response->data = $out;
            \Yii::$app->end();
        }
    }

    public function actionAjaxStokBarang($q = null) {

        $query = new \yii\db\Query;
    
        $query->select('b.kode_barang , b.id_barang, b.nama_barang, b.id_satuan as satuan, ds.id, (SELECT sisa FROM erp_stok_history WHERE departemen_id = ds.departemen_id AND barang_id = b.id_barang ORDER BY id DESC LIMIT 1) as stok')
            ->from('erp_departemen_stok ds')
            ->join('JOIN','erp_sales_master_barang b','b.id_barang=ds.barang_id')
            ->join('JOIN','erp_departemen_user du','du.departemen_id=ds.departemen_id')
            ->where(['du.user_id'=>Yii::$app->user->identity->id])
            ->andWhere('(nama_barang LIKE "%' . $q .'%" OR kode_barang LIKE "%' . $q .'%")')
            ->orderBy('nama_barang')
            // ->groupBy(['kode'])
            ->limit(20);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $out[] = [
                'id' => $d['id_barang'],
                'kode' => $d['kode_barang'],
                'nama' => $d['nama_barang'],
                'dept_stok_id' => $d['id'],
                'satuan' => $d['satuan'],
                'stok' => $d['stok'],
                'label'=> $d['nama_barang']
            ];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
        \Yii::$app->response->data = $out;
        \Yii::$app->end();

      
    }

    public function actionGetDepartemenStok()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = self::getDepartemenListStok($cat_id); 
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                echo \yii\helpers\Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo \yii\helpers\Json::encode(['output'=>'', 'selected'=>'']);
        die();
    }

    private function getDepartemenListStok($id)
    {
        $query = DepartemenStok::find()->where(['departemen_id'=>$id])->all();
        // $query->joinWith(['departemen as d']);
        $result = [];
        foreach($query as $item)
        {
            $result[] = [
                'id' => $item->barang_id,
                'name' => $item->barang->nama_barang
            ];
        }

        return $result;
    }

    /**
     * Lists all PerusahaanSubStok models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DepartemenStokSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $departemens = \yii\helpers\ArrayHelper::map(\app\models\Departemen::find()->all(),'nama','nama');

        $listJenisBarang = \yii\helpers\ArrayHelper::map(\app\models\MasterJenisBarang::find()->all(),'nama','nama');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'departemens' => $departemens,
            'listJenisBarang' => $listJenisBarang
        ]);
    }

    /**
     * Displays a single PerusahaanSubStok model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PerusahaanSubStok model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DepartemenStok();
        $model->scenario = 'insert';
        $model->tanggal = date('Y-m-d');
        if ($model->load(Yii::$app->request->post())) {
            
            $m = DepartemenStok::find()->where([
                'barang_id' => $model->barang_id,
                'departemen_id' => $model->departemen_id
            ])->one();

            if (!empty($m)) {
                $model->addError('departemen_id', 'Barang ini sudah ada di unit ini');
            }

            else
            {
                $model->save();
                Yii::$app->session->setFlash('success', "Data telah ditambahkan");
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PerusahaanSubStok model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $errors = '';
        $stok_sebelum = $model->stok;
        if ($model->load(Yii::$app->request->post())) 
        {
            $transaction = \Yii::$app->db->beginTransaction();

            $stok_riil = $_POST['DepartemenStok']['qty'];
            try 
            {

                // if($model->stok != $stok_sebelum)
                // {
                $dept_id = Yii::$app->user->identity->access_role == 'kepalaCabang' ? $model->departemen_id : Yii::$app->user->identity->departemen;
                $timestamp = date('Y-m-d H:i:s');
                $ks = new KartuStok;
                $ks->barang_id = $model->barang_id;
                $ks->qty_in = $stok_riil;
                $ks->kode_transaksi = 'STOKAWAL_'.$timestamp.'_'.$model->id;
                $ks->tanggal = date('Y-m-d');
                $ks->departemen_id = $dept_id;
                
                $ks->stok_id = $model->id;
                $ks->keterangan = 'Stok Opname '.$model->barang->nama_barang.' '.$timestamp;
                $ks->created_at = date('Y-m-d H:i:s');//date('Y-m-01').' 00:00:01';
                $ks->updated_at = date('Y-m-d H:i:s');//date('Y-m-01').' 00:00:01';
                // print_r($ks->qty_in);exit;

              
                $sh = new StokHistory;
                $sh->barang_id = $ks->barang_id;
                $sh->kode_transaksi = $ks->kode_transaksi;
                $sh->departemen_id = $ks->departemen_id;
                $sh->qty_in = $ks->qty_in ?: 0;
                $sh->qty_out = $ks->qty_out ?: 0;

                // if(!empty($prevStok[1]))
                // {
                //     $sh->sisa = $prevStok->sisa - $sh->qty_out;
                // }

                $sh->sisa = $stok_riil;
                $sh->tanggal = $ks->tanggal;
                $sh->keterangan = $ks->keterangan;
                $sh->created_at = $ks->created_at;
                $sh->updated_at = $ks->updated_at;
                // $sh->sisa = $m->sisa;
                

                if($ks->validate())
                {
                    $ks->save();
                }    
                else{
                    $errors .= \app\helpers\MyHelper::logError($ks);
                    throw new \Exception;
                    
                }

                if($sh->validate())
                {
                    $sh->save();
                }    
                else{
                    $errors .= \app\helpers\MyHelper::logError($sh);
                    // throw new \Exception;
                    
                }
                // }
                $model->save();
                Yii::$app->session->setFlash('success', "Data telah diupdate");
                $transaction->commit();
                return $this->redirect(['index']);
                // return $this->redirect(['create']);
            } catch (\Exception $e) {
                 $transaction->rollBack();
                Yii::$app->session->setFlash('error', $errors.$e->getMessage());
               
                // throw $e;
            } catch (\Throwable $e) {
                 $transaction->rollBack();
                Yii::$app->session->setFlash('error', $errors.$e->getMessage());
               
                // throw $e;
            }

            return $this->redirect(['update','id'=>$id]);
            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PerusahaanSubStok model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_hapus = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionRestore($id)
    {
        $model = $this->findModel($id);
        $model->is_hapus = 0;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionAjaxDelete($id)
    {
        $this->findModel($id)->delete();

        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the PerusahaanSubStok model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PerusahaanSubStok the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DepartemenStok::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
