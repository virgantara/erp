<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_departemen_stok_history".
 *
 * @property int $id
 * @property int $dept_stok_id
 * @property double $stok_awal
 * @property double $stok
 * @property double $stok_bulan_lalu
 * @property string $tanggal
 * @property double $hb
 * @property double $hj
 * @property string $created_at
 * @property string $updated_at
 *
 * @property DepartemenStok $deptStok
 */
class DepartemenStokHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_departemen_stok_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dept_stok_id'], 'required'],
            [['dept_stok_id'], 'integer'],
            [['stok_awal', 'stok', 'hb', 'hj'], 'number'],
            [['tanggal', 'created_at', 'updated_at'], 'safe'],
            [['dept_stok_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartemenStok::className(), 'targetAttribute' => ['dept_stok_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dept_stok_id' => 'Dept Stok ID',
            'stok_awal' => 'Stok Awal',
            'stok' => 'Stok',
            // 'stok_bulan_lalu' => 'Stok Bulan Lalu',
            'tanggal' => 'Tanggal',
            'hb' => 'Hb',
            'hj' => 'Hj',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeptStok()
    {
        return $this->hasOne(DepartemenStok::className(), ['id' => 'dept_stok_id']);
    }
}
