<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_stok_history".
 *
 * @property int $id
 * @property int $barang_id
 * @property int $departemen_id
 * @property string $tanggal
 * @property double $qty_in
 * @property double $qty_out
 * @property double $sisa
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SalesMasterBarang $barang
 * @property Departemen $departemen
 */
class StokHistory extends \yii\db\ActiveRecord
{
     public $tanggal_awal;
    public $tanggal_akhir;
    public $bulan;
    public $tahun;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_stok_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['barang_id', 'departemen_id', 'tanggal'], 'required'],
            [['barang_id', 'departemen_id'], 'integer'],
            [['tanggal', 'created_at', 'updated_at'], 'safe'],
            [['qty_in', 'qty_out', 'sisa'], 'number'],
            [['keterangan'], 'string', 'max' => 255],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalesMasterBarang::className(), 'targetAttribute' => ['barang_id' => 'id_barang']],
            [['departemen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departemen::className(), 'targetAttribute' => ['departemen_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'barang_id' => 'Barang ID',
            'departemen_id' => 'Departemen ID',
            'tanggal' => 'Tanggal',
            'qty_in' => 'Qty In',
            'qty_out' => 'Qty Out',
            'sisa' => 'Sisa',
            'keterangan' => 'Keterangan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getPrevStok($params)
    {
        $query = StokHistory::find();
        $query->where([
            'barang_id' => $params['barang_id'],
            'departemen_id' => $params['departemen_id'],
        ]);
        $query->orderBy(['id'=>SORT_DESC]);
        // $query->limit(1);
        return $query->one();
    }

    public static function getLastMonthStok($barang_id, $departemen_id,$bulan)
    {
        $query = StokHistory::find()->alias('bo');
        $query->andWhere(['departemen_id'=>$departemen_id]);
        // $query->andWhere(['barang.jenis_barang_id'=>$_GET['jenis_barang_id']]);
        // $query->andWhere(['created_at'=>$tahunbulan]);
        $query->andFilterWhere(['between', 'bo.created_at', $bulan.'-01 00:00:00', $bulan.'-31 23:59:59']);
        $query->andWhere(['bo.barang_id'=>$barang_id]);
        $query->orderBy(['bo.created_at'=>SORT_DESC]);
        // $query->limit(1);
        return $query->one();
    }

    public static function hitungSisa($barang_id, $departemen_id)
    {
        $sd = date('Y-m-01 00:00:01');
        $ed = date('Y-m-d H:i:s');
        $query = StokHistory::find();
        $query->where([
            'barang_id' => $barang_id,
            'departemen_id' => $departemen_id,
        ]);
        $query->andWhere(['between','created_at',$sd, $ed]);
            
        $list = $query->all();
        $sisa = 0;
        $stok = 0;
        foreach($list as $l)
        {
            $stok += $l->qty_in;
            $stok -= $l->qty_out;
            $sisa = $stok;
            // $sum += $l->qty_out;
        }    


        return $sisa;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(SalesMasterBarang::className(), ['id_barang' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartemen()
    {
        return $this->hasOne(Departemen::className(), ['id' => 'departemen_id']);
    }
}
