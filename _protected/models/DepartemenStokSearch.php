<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DepartemenStok;

/**
 * PerusahaanSubStokSearch represents the model behind the search form of `app\models\PerusahaanSubStok`.
 */
class DepartemenStokSearch extends DepartemenStok
{

    public $namaDepartemen;
    public $namaBarang;
    public $kodeBarang;
    public $jenisBarang;
    public $hargaJual;
    public $hargaBeli;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'barang_id', 'departemen_id', 'bulan', 'tahun'], 'integer'],
            [['stok_akhir', 'stok_awal', 'stok_bulan_lalu'], 'number'],
            [['created_at', 'tanggal','namaBarang','kodeBarang','namaDepartemen','exp_date','batch_no','stok_minimal','stok_maksimal','jenisBarang','hargaBeli','hargaJual'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function searchPaket($params)
    {
        $query = DepartemenStok::find()->where(['barang.is_paket'=>1]);
        $query->joinWith(['departemen as departemen','barang as barang','barang.jenisBarang as j']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->where(['barang.is_hapus'=>0,self::tableName().'.is_hapus'=>0]);
        $query->andWhere(['barang.is_paket'=>1]);

        $dataProvider->sort->attributes['namaDepartemen'] = [
            'asc' => ['departemen.nama'=>SORT_ASC],
            'desc' => ['departemen.nama'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['jenisBarang'] = [
            'asc' => ['j.nama'=>SORT_ASC],
            'desc' => ['j.nama'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['namaBarang'] = [
            'asc' => ['barang.nama_barang'=>SORT_ASC],
            'desc' => ['barang.nama_barang'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['hargaBeli'] = [
            'asc' => ['barang.harga_beli'=>SORT_ASC],
            'desc' => ['barang.harga_beli'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['hargaJual'] = [
            'asc' => ['barang.harga_jual'=>SORT_ASC],
            'desc' => ['barang.harga_jual'=>SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'stok_akhir' => $this->stok_akhir,
            'stok_awal' => $this->stok_awal,
            'bulan' => $this->bulan,
            'tahun' => $this->tahun,
            'stok_bulan_lalu' => $this->stok_bulan_lalu,
            // 'stok' => $this->stok,
            // 'ro_item_id' => $this->ro_item_id,
        ]);

        $userLevel = Yii::$app->user->identity->access_role;    
        $where = [];
        if($userLevel == 'operatorCabang' || $userLevel == 'operatorUnit'){
            $departemen = Yii::$app->user->identity->departemen;
            $query->andFilterWhere(['departemen_id' => $departemen]);
        }


        $query->andFilterWhere(['like', 'departemen.nama', $this->namaDepartemen]);
        $query->andFilterWhere(['like', 'barang.nama_barang', $this->namaBarang]);
        $query->andFilterWhere(['like', 'j.nama', $this->jenisBarang]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DepartemenStok::find();
        $query->joinWith(['departemen as departemen','barang as barang','barang.jenisBarang as j']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->where(['barang.is_hapus'=>0]);

        $dataProvider->sort->attributes['namaDepartemen'] = [
            'asc' => ['departemen.nama'=>SORT_ASC],
            'desc' => ['departemen.nama'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['namaBarang'] = [
            'asc' => ['barang.nama_barang'=>SORT_ASC],
            'desc' => ['barang.nama_barang'=>SORT_DESC]
        ];

         $dataProvider->sort->attributes['jenisBarang'] = [
            'asc' => ['j.nama'=>SORT_ASC],
            'desc' => ['j.nama'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['kodeBarang'] = [
            'asc' => ['barang.kode_barang'=>SORT_ASC],
            'desc' => ['barang.kode_barang'=>SORT_DESC]
        ];

         $dataProvider->sort->attributes['hargaBeli'] = [
            'asc' => ['barang.harga_beli'=>SORT_ASC],
            'desc' => ['barang.harga_beli'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['hargaJual'] = [
            'asc' => ['barang.harga_jual'=>SORT_ASC],
            'desc' => ['barang.harga_jual'=>SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'departemen_id' => $this->departemen_id,
            'stok_akhir' => $this->stok_akhir,
            'stok_awal' => $this->stok_awal,
            'created_at' => $this->created_at,
            'bulan' => $this->bulan,
            'tahun' => $this->tahun,
            'tanggal' => $this->tanggal,
            'stok_bulan_lalu' => $this->stok_bulan_lalu,
            // 'stok' => $this->stok,
            // 'ro_item_id' => $this->ro_item_id,
        ]);

        $userLevel = Yii::$app->user->identity->access_role;    
        $where = [];
        if($userLevel == 'operatorCabang' || $userLevel == 'operatorUnit'){
            $departemen = Yii::$app->user->identity->departemen;
            $query->andFilterWhere(['departemen_id' => $departemen]);
            $query->andWhere([self::tableName().'.is_hapus'=>0]);
        }


        $query->andFilterWhere(['like', 'departemen.nama', $this->namaDepartemen])
            ->andFilterWhere(['like', 'barang.nama_barang', $this->namaBarang])
            ->andFilterWhere(['like', 'j.nama', $this->jenisBarang])
            ->andFilterWhere(['like', 'barang.kode_barang', $this->kodeBarang]);

        return $dataProvider;
    }
}
