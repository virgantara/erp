<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%kartu_stok}}".
 *
 * @property int $id
 * @property int $barang_id
 * @property int $departemen_id
 * @property int $stok_id
 * @property double $qty
 * @property int $status_disposisi 0 = masuk, 1 = keluar
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SalesMasterBarang $barang
 * @property Departemen $departemen
 */
class KartuStok extends \yii\db\ActiveRecord
{

    public $tanggal_awal;
    public $tanggal_akhir;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%kartu_stok}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['barang_id', 'tanggal'], 'required'],
            [['barang_id', 'departemen_id', 'stok_id'], 'integer'],
            [['qty_in','qty_out'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['keterangan'], 'string', 'max' => 255],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalesMasterBarang::className(), 'targetAttribute' => ['barang_id' => 'id_barang']],
            // [['stok_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalesStokGudang::className(), 'targetAttribute' => ['stok_id' => 'id_stok']],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'barang_id' => 'Barang',
            'kode_transaksi' => 'Kode Trx',
            'departemen_id' => 'Departemen',
            'stok_id' => 'Stok',
            'qty_in' => 'Masuk',
            'qty_out' => 'Keluar',
            'tanggal_awal' => 'Tgl Awal',
            'tanggal_akhir' => 'Tgl Akhir',
            'keterangan' => 'Keterangan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    // public function getStokBarang()
    // {
    //     return $this->hasOne(SalesStokGudang::className(), ['id_stok' => 'stok_id']);
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(SalesMasterBarang::className(), ['id_barang' => 'barang_id']);
    }

    public static function deleteKartuStok($kode_transaksi){
        $m = KartuStok::find()->where(['kode_transaksi'=>$kode_transaksi]);
        foreach($m->all() as $item)
            $item->delete();
    }

    public static function getPrevStok($params)
    {
        $query = KartuStok::find();
        $query->where([
            'barang_id' => $params['barang_id'],
            'departemen_id' => $params['departemen_id'],
        ]);
        $query->orderBy(['created_at'=>SORT_DESC]);
        $query->limit(2);
        return $query->all();
    }

 
    public static function getListPenjualanSebelumValidasi($barang_id, $departemen_id,$sd, $ed)
    {
        // $sd = date('Y-m-d 00:00:01');
        // $ed = date('Y-m-d H:i:s');
        $query = StokHistory::find();
        $query->where([
            'barang_id' => $barang_id,
            'departemen_id' => $departemen_id,
        ]);
        $query->andWhere(['between','created_at',$sd, $ed]);
            
        $list = $query->all();
            
        return $list;
    }

    public static function getPenjualanSebelumValidasi($barang_id, $departemen_id,$sd, $ed)
    {
        // $sd = date('Y-m-d 00:00:01');
        // $ed = date('Y-m-d H:i:s');
        $query = StokHistory::find();
        $query->where([
            'barang_id' => $barang_id,
            'departemen_id' => $departemen_id,
        ]);
        $query->andWhere(['between','created_at',$sd, $ed]);
            
        $list = $query->all();
        $sum = 0;
        foreach($list as $l)
        {
            $sum += $l->qty_out;
        }    
        return $sum;
    }

    public static function createKartuStok($params){
        $m = new KartuStok;
        $m->barang_id = $params['barang_id'];



        $prevStok = StokHistory::getPrevStok($params);
        // print_r($prevStok->attributes);exit;
        if($params['status'] == 1)
        {
            $m->qty_in = $params['qty'];
            if(!empty($prevStok))
            {

                $m->sisa_lalu = $prevStok->sisa;
                $m->sisa = $prevStok->sisa + $m->qty_in;
                $m->prev_id = $prevStok->id;

            }

            else
            {
                $m->sisa_lalu = 0;
                $m->sisa = $m->qty_in;
            }
        }

        else
        {
            $m->qty_out = $params['qty'];
            if(!empty($prevStok))
            {


                
                $m->sisa_lalu = $prevStok->sisa;
                $m->sisa = $prevStok->sisa - $m->qty_out;
                $m->prev_id = $prevStok->id;

            }

            else
            {
                $m->sisa_lalu = 0;
                $m->sisa = -$m->qty_out;
            }
        }



        $m->kode_transaksi = !empty($params['kode_transaksi']) ? $params['kode_transaksi'] : '-';
        $m->tanggal = $params['tanggal'];
        $m->departemen_id = $params['departemen_id'];
        $m->stok_id = $params['stok_id'];
        $m->keterangan = $params['keterangan'];

        $sh = new StokHistory;
        $sh->barang_id = $m->barang_id;
        $sh->kode_transaksi = $m->kode_transaksi;
        $sh->departemen_id = $m->departemen_id;
        $sh->qty_in = $m->qty_in ?: 0;
        $sh->qty_out = $m->qty_out ?: 0;
        $sh->tanggal = $m->tanggal;
        $sh->keterangan = $m->keterangan;
        $sh->sisa = $m->sisa;
        $sh->save();

        if($m->validate())
            $m->save();
        else{
            $errors = '';
            foreach($m->getErrors() as $attribute){
                foreach($attribute as $error){
                    $errors .= $error.' ';
                }
            }
                
            print_r($errors);exit;             
        }

        DepartemenStok::updateStok($m->barang_id, $m->departemen_id);
    }

    public static function getQty($provider, $inout=1)
    {
      $total = 0;

      foreach ($provider as $item) {
        
        switch ($inout) {
            case 1:
                $total += $item->qty_in;
                break;
            case 0:
                $total += $item->qty_out;
                break;
            
        }
        
        // $total += $subtotal;
      }


      return $total;  
    }

}
