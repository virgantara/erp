<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "perusahaan_sub_stok".
 *
 * @property int $id
 * @property int $barang_id
 * @property int $perusahaan_sub_id
 * @property double $stok_akhir
 * @property double $stok_awal
 * @property string $created
 * @property int $bulan
 * @property int $tahun
 * @property string $tanggal
 * @property double $stok_bulan_lalu
 * @property double $stok
 * @property int $ro_item_id
 *
 * @property SalesMasterBarang $barang
 * @property RequestOrderItem $roItem
 * @property PerusahaanSub $perusahaanSub
 */
class DepartemenStok extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%departemen_stok}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['barang_id', 'departemen_id', 'tanggal','stok_minimal','stok_maksimal'], 'required'],
            [['barang_id', 'departemen_id', 'bulan', 'tahun'], 'integer'],
            // [['barang_id'],'validateBarang','on'=>'insert'],
            [['stok_akhir', 'stok_awal'], 'number'],
            [['barang_id', 'departemen_id'],'unique','on'=>'insert'],
            [['created_at', 'tanggal','stok_minimal'], 'safe'],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalesMasterBarang::className(), 'targetAttribute' => ['barang_id' => 'id_barang']],
            // [['ro_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestOrderItem::className(), 'targetAttribute' => ['ro_item_id' => 'id']],
            [['departemen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departemen::className(), 'targetAttribute' => ['departemen_id' => 'id']],
        ];
    }

    

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'barang_id' => 'Barang ID',
            'departemen_id' => 'Departemen',
            'stok_akhir' => 'Stok Akhir',
            'stok_awal' => 'Stok Awal',
            'created_at' => 'Created',
            'bulan' => 'Bulan',
            'tahun' => 'Tahun',
            'tanggal' => 'Tanggal',
            'stok_bulan_lalu' => 'Stok Bulan Lalu',
            'stok_lama' => 'Qty',
            'stok_minimal' => 'Stok Minimal',
            // 'ro_item_id' => 'Ro Item ID',
            'exp_date' => 'Exp Date',
            'batch_no' => 'Batch No.',
            'hb' => 'HB',
            'hj' => 'HJ'
        ];
    }

    public static function updateStok($barang_id, $departemen_id)
    {
        $command = Yii::$app->db->createCommand("SELECT sisa FROM erp_stok_history WHERE departemen_id = ".$departemen_id." AND barang_id = ".$barang_id." ORDER BY id DESC limit 1;");
        $sum = $command->queryScalar();

        $model = DepartemenStok::find()->where(['barang_id'=>$barang_id,'departemen_id'=>$departemen_id])->one();
        if(empty($model)){
            $model = new DepartemenStok;
            $model->barang_id = $barang_id;
            $model->departemen_id = $departemen_id;
            $model->qty = $sum;
            $model->save(false,['qty','barang_id','departemen_id']);
        }

        else
        {
            $model->qty = $sum;
            $model->save(false,['qty']);    
        }
        
    } 

    public function getStok()
    {
        $command = Yii::$app->db->createCommand("SELECT sisa FROM erp_stok_history WHERE departemen_id = ".$this->departemen_id." AND barang_id = ".$this->barang_id." ORDER BY id DESC limit 1;");
        $sum = $command->queryScalar();
        return $sum;
    }

    // public function validateBarang($attribute, $params, $validator)
    // {
    //     $model = DepartemenStok::find()->where([
    //         'barang_id' => $this->barang_id,
    //         'departemen_id' => $this->departemen_id
    //     ])->one();

    //     if (!empty($model)) {
    //         $this->addError($attribute, 'Barang ini sudah ada di unit ini');
    //     }
    // }

    public static function minusStok($barang_id, $departemen_id, $qty)
    {
        $query = DepartemenStok::find()->where([
            'barang_id' => $barang_id,
            'departemen_id' => $departemen_id
        ]);

        $ds = $query->one();



        if(!empty($ds))
        {
            $ds->qty -= $qty;
            if(!$ds->save())
            {
                print_r($ds->getErrors());exit;
            }

            // print_r($ds->stok);exit;

        }

        else
        {
            $ds = new DepartemenStok;
            $ds->scenario = 'insert';
            $ds->barang_id = $barang_id;
            $ds->departemen_id = $departemen_id;
            $ds->qty = $qty;
            $ds->tanggal = date('Y-m-d');
            $ds->bulan = date('m');
            $ds->tahun = date('Y');
            $ds->save();
        }
    }

    public static function addStok($barang_id, $departemen_id, $qty)
    {
        $query = DepartemenStok::find()->where([
            'barang_id' => $barang_id,
            'departemen_id' => $departemen_id
        ]);

        $ds = $query->one();

        if(!empty($ds))
        {
            $ds->qty += $qty;
            // print_r($ds->attributes);exit;
            if(!$ds->save(false,['stok']))
            {
                print_r($ds->getErrors());exit;
            }

            
        }

        else
        {
            $ds = new DepartemenStok;
            $ds->scenario = 'insert';
            $ds->barang_id = $barang_id;
            $ds->departemen_id = $departemen_id;
            $ds->qty = $qty;
            $ds->tanggal = date('Y-m-d');
            $ds->bulan = date('m');
            $ds->tahun = date('Y');
            $ds->save();
        }
    }

    public static function getListStokDepartemen()
    {

        $userPt = '';
            
        $where = [];    
        $userLevel = Yii::$app->user->identity->access_role;    
            
        $list_user = [
            'operatorCabang'
        ];

        if(in_array($userLevel, $list_user)){
            $userPt = Yii::$app->user->identity->id;
            $where = array_merge($where,['d.user_id' => $userPt]);
        }

        $query = DepartemenStok::find();
        $query->joinWith(['departemen as d']);
        $query->andFilterWhere($where);

        $list = $query->all();

        $listDataGudang=ArrayHelper::map($list,'id','d.nama');
        return $listDataGudang;
    }

    public function getKodeBarang()
    {
        return $this->barang->kode_barang;
    }

    public function getNamaBarang()
    {
        return $this->barang->nama_barang;
    }

    public function getNamaDepartemen()
    {
        return $this->departemen->nama;   
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(SalesMasterBarang::className(), ['id_barang' => 'barang_id']);
    }

    public function getJenisBarang()
    {
        return $this->barang->jenisBarang->nama;
    }

    public function getHargaBeli()
    {
        return $this->barang->harga_beli;

    }

    public function getHargaJual()
    {
        return $this->barang->harga_jual;
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getRoItem()
    // {
    //     return $this->hasOne(RequestOrderItem::className(), ['id' => 'ro_item_id']);
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartemen()
    {
        return $this->hasOne(Departemen::className(), ['id' => 'departemen_id']);
    }
}
