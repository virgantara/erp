<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReturBarangCustomer;

/**
 * ReturBarangCustomerSearch represents the model behind the search form of `app\models\ReturBarangCustomer`.
 */
class ReturBarangCustomerSearch extends ReturBarangCustomer
{

    public $namaPasien;
    public $kodePenjualan;
    public $unitPenerima;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'penjualan_id', 'departemen_id', 'departemen_penerima_id', 'customer_id'], 'integer'],
            [['tanggal', 'created_at', 'updated_at','namaPasien','kodePenjualan','unitPenerima'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReturBarangCustomer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['penjualan as p','penjualan.penjualanResep as pr','departemenPenerima as dp']);

        $dataProvider->sort->attributes['namaPasien'] = [
            'asc' => ['pr.pasien_nama'=>SORT_ASC],
            'desc' => ['pr.pasien_nama'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['kodePenjualan'] = [
            'asc' => ['p.kode_penjualan'=>SORT_ASC],
            'desc' => ['p.kode_penjualan'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['unitPenerima'] = [
            'asc' => ['dp.nama'=>SORT_ASC],
            'desc' => ['dp.nama'=>SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
            'p.customer_id' => $this->customer_id,
            // 'tanggal' => $this->tanggal,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'pr.pasien_nama', $this->namaPasien])
            ->andFilterWhere(['like', 'dp.nama', $this->unitPenerima])
            ->andFilterWhere(['like', 'p.kode_penjualan', $this->kodePenjualan]);
            // ->andFilterWhere(['like', 'f.no_so', $this->noSo])
            // ->andFilterWhere(['like', 'p.nama', $this->namaPerusahaan]);

        return $dataProvider;
    }
}
