<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DepartemenStokHistory;

/**
 * DepartemenStokHistorySearch represents the model behind the search form of `app\models\DepartemenStokHistory`.
 */
class DepartemenStokHistorySearch extends DepartemenStokHistory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dept_stok_id'], 'integer'],
            [['stok_awal', 'stok', 'hb', 'hj'], 'number'],
            [['tanggal', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DepartemenStokHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dept_stok_id' => $this->dept_stok_id,
            'stok_awal' => $this->stok_awal,
            'stok' => $this->stok,
            'tanggal' => $this->tanggal,
            'hb' => $this->hb,
            'hj' => $this->hj,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
