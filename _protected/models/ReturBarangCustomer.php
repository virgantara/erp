<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_retur_barang_customer".
 *
 * @property int $id
 * @property int $penjualan_id
 * @property int $departemen_id
 * @property int $departemen_penerima_id
 * @property int $customer_id
 * @property string $tanggal
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Penjualan $penjualan
 * @property Departemen $departemen
 * @property Departemen $departemenPenerima
 * @property ReturBarangCustomerItem[] $returBarangCustomerItems
 */
class ReturBarangCustomer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_retur_barang_customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['penjualan_id', 'departemen_id', 'departemen_penerima_id', 'customer_id'], 'integer'],
            [['departemen_penerima_id', 'tanggal'], 'required'],
            [['tanggal', 'created_at', 'updated_at'], 'safe'],
            [['penjualan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Penjualan::className(), 'targetAttribute' => ['penjualan_id' => 'id']],
            [['departemen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departemen::className(), 'targetAttribute' => ['departemen_id' => 'id']],
            [['departemen_penerima_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departemen::className(), 'targetAttribute' => ['departemen_penerima_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'penjualan_id' => 'Penjualan ID',
            'departemen_id' => 'Departemen ID',
            'departemen_penerima_id' => 'Departemen Penerima ID',
            'customer_id' => 'Customer ID',
            'tanggal' => 'Tanggal',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getKodePenjualan()
    {
        return $this->penjualan->kode_penjualan;
    }

    public function getNamaPasien()
    {
        return $this->penjualan->penjualanResep->pasien_nama;
    }

    public function getUnitPenerima()
    {
        return $this->departemenPenerima->nama;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenjualan()
    {
        return $this->hasOne(Penjualan::className(), ['id' => 'penjualan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartemen()
    {
        return $this->hasOne(Departemen::className(), ['id' => 'departemen_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartemenPenerima()
    {
        return $this->hasOne(Departemen::className(), ['id' => 'departemen_penerima_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturBarangCustomerItems()
    {
        return $this->hasMany(ReturBarangCustomerItem::className(), ['retur_id' => 'id']);
    }
}
