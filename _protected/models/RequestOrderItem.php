<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_request_order_item".
 *
 * @property int $id
 * @property int $ro_id
 * @property int $barang_id
 * @property int $departemen_id
 * @property int $stok_id
 * @property double $jumlah_minta
 * @property double $jumlah_beri
 * @property string $satuan
 * @property string $keterangan
 * @property string $created
 * @property int $item_id
 *
 * @property RequestOrder $ro
 * @property SalesMasterBarang $barang
 * @property Departemen $departemen
 * @property SalesMasterBarang $item
 */
class RequestOrderItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_request_order_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ro_id', 'barang_id','departemen_id', 'keterangan', 'item_id'], 'required'],
            [['ro_id', 'barang_id', 'departemen_id', 'item_id'], 'integer'],
            [['jumlah_minta', 'jumlah_beri'], 'number'],
            [['created'], 'safe'],
            [['satuan'], 'string', 'max' => 50],
            [['keterangan'], 'string', 'max' => 255],
            [['ro_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestOrder::className(), 'targetAttribute' => ['ro_id' => 'id']],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalesMasterBarang::className(), 'targetAttribute' => ['barang_id' => 'id_barang']],
            [['departemen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departemen::className(), 'targetAttribute' => ['departemen_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalesMasterBarang::className(), 'targetAttribute' => ['item_id' => 'id_barang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ro_id' => 'Ro ID',
            'barang_id' => 'Barang ID',
            'departemen_id' => 'Departemen ID',
            'stok_id' => 'Stok ID',
            'jumlah_minta' => 'Jumlah Minta',
            'jumlah_beri' => 'Jumlah Beri',
            'satuan' => 'Satuan',
            'keterangan' => 'Keterangan',
            'created' => 'Created',
            'item_id' => 'Item ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRo()
    {
        return $this->hasOne(RequestOrder::className(), ['id' => 'ro_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(SalesMasterBarang::className(), ['id_barang' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartemen()
    {
        return $this->hasOne(Departemen::className(), ['id' => 'departemen_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(SalesMasterBarang::className(), ['id_barang' => 'item_id']);
    }
}
