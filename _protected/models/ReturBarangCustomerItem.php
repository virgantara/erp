<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_retur_barang_customer_item".
 *
 * @property int $id
 * @property int $retur_id
 * @property int $barang_id
 * @property double $qty
 * @property double $hb
 * @property double $hj
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ReturBarangCustomer $retur
 * @property SalesMasterBarang $barang
 */
class ReturBarangCustomerItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_retur_barang_customer_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['barang_id'], 'required'],
            [['id', 'retur_id', 'barang_id'], 'integer'],
            [['qty', 'hb', 'hj'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['retur_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReturBarangCustomer::className(), 'targetAttribute' => ['retur_id' => 'id']],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalesMasterBarang::className(), 'targetAttribute' => ['barang_id' => 'id_barang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'retur_id' => 'Retur ID',
            'barang_id' => 'Barang ID',
            'qty' => 'Qty',
            'hb' => 'Hb',
            'hj' => 'Hj',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getNamaBarang()
    {
        return $this->barang->nama_barang;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRetur()
    {
        return $this->hasOne(ReturBarangCustomer::className(), ['id' => 'retur_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(SalesMasterBarang::className(), ['id_barang' => 'barang_id']);
    }
}
