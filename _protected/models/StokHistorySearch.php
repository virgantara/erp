<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StokHistory;

/**
 * StokHistorySearch represents the model behind the search form of `app\models\StokHistory`.
 */
class StokHistorySearch extends StokHistory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'barang_id', 'departemen_id'], 'integer'],
            [['tanggal', 'keterangan', 'created_at', 'updated_at'], 'safe'],
            [['qty_in', 'qty_out', 'sisa'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StokHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        // $this->load($params);

        $this->tanggal_awal = date('Y-m-d',strtotime($params['StokHistory']['tanggal_awal']));
        $this->tanggal_akhir = date('Y-m-d',strtotime($params['StokHistory']['tanggal_akhir']));
        // print_r($this->tanggal_awal);
        // print_r($this->tanggal_akhir);
        if(!empty($params))
        {
            $query->where(['departemen_id'=>Yii::$app->user->identity->departemen]);
            $query->andWhere(['barang_id'=>$this->barang_id]);
            $query->andFilterWhere(['between', 'tanggal', $this->tanggal_awal, $this->tanggal_akhir]);
            $query->orderBy(['created_at'=>SORT_ASC]);
            // print_r($params);exit;
        }

        else{
            $query->where(['barang_id'=>'a']);
        }

        return $dataProvider;
    }
}
