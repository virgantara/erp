<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DepartemenStokHistory */

$this->title = Yii::t('app', 'Create Departemen Stok History');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departemen Stok Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departemen-stok-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
