<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DepartemenStokHistorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="departemen-stok-history-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'dept_stok_id') ?>

    <?= $form->field($model, 'stok_awal') ?>

    <?= $form->field($model, 'stok') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?php // echo $form->field($model, 'hb') ?>

    <?php // echo $form->field($model, 'hj') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
