<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReturBarangCustomerItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Retur Barang Customer Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retur-barang-customer-item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Retur Barang Customer Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'retur_id',
            'barang_id',
            'qty',
            'hb',
            //'hj',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
