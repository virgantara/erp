<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReturBarangCustomerItem */

$this->title = 'Update Retur Barang Customer Item: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Retur Barang Customer Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="retur-barang-customer-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
