<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReturBarangCustomerItem */

$this->title = 'Create Retur Barang Customer Item';
$this->params['breadcrumbs'][] = ['label' => 'Retur Barang Customer Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retur-barang-customer-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
