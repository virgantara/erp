<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReturBarangCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Retur Barang Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retur-barang-customer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Retur Barang Customer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'penjualan_id',
            'departemen_id',
            'departemen_penerima_id',
            'customer_id',
            //'tanggal',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
