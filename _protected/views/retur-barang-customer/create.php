<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReturBarangCustomer */

$this->title = 'Create Retur Barang Customer';
$this->params['breadcrumbs'][] = ['label' => 'Retur Barang Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retur-barang-customer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
